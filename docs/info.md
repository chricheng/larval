# 描述

-   多商户，插件式社区中台

## 接口地址

-  生产环境

    ```json
    http://community.didiyd.vip/
    ```

-  测试环境

    ```json
    // IP： 192.168.2.198 需解析
    http://www.cmm.com/
    ```


- 二层楼测试key ：co181773fb158fed2

- 裙子社区测试key co4a361a79789bdds

    

## telescope调试工具

- 地址
    ```
    http://www.cmm.com/telescope/
    ```
- 开发时所有的请求都会反馈到这个url中

## 社区中台架构

- php 版本 > 7.1.3
- 框架 laravel > 5.8
- 搜索 elasticsearch
