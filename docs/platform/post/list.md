# 帖子列表

#### 接口地址

-   platform/post/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "limit":"20", // 帖子每页数量
        "page":"1", // 帖子页
        "query[start_time]":"2019-08-21 00:00:00",// 时间范围开始时间
        "query[end_time]":"2019-11-22 00:00:00", // 时间范围结束时间
        "query[title]":"不是测试行么？", // 标题
        "query[state]":"sdff",// 帖子状态 1 正常， 2 隐藏，3已违规
        "query[post_types]":"1,2", // 帖子类型，多个逗号隔开  TODO
        "query[resource_types]":"2", // 资源类型，多个逗号隔开 TODO
        "query[is_public]":"2", // 可见范围 1公开 2私有
        "query[content]":"哈哈", // 正文
        "query[resources]":"1", // 暂不好使
        "query[token]":"bec67443d6b1721589dac3f5d3ccb5be", // 用户token
        "query[clique_id]":"1", // 圈子id TODO
        "query[essence_types]":"repo,snapshot", // 传空为全部，普通帖子：normal repo帖子：repo， 私影帖子：snapshot， 多个逗号隔开
        // 排序
        "order[created_at]":"desc",// created_at时间 comment_num评论数 share_num转发数量，praise_num点赞数量, favorite_num收藏数量
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                "id": 89, // 帖子id
                "post_type": 2, // 帖子类型 1普通帖子，2图文文章(富文本)
                "created_at": "2019-08-23 03:43:13", // 创建时间
                "updated_at": "2019-08-23 03:43:13", // 修改时间
                "comment_num": 0, // 评论数量
                "praise_num": 0, // 点赞数量
                "favorite_num": 0, // 收藏数量
                "share_num": 0, // 分享数量
                "user": {
                    "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户标识
                    "follow": 1  // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
                },
                "favorite": 0, // 1已收藏，0未收藏
                "title": "不是测试行么？", // 标题（权限联动）
                "praise": 0, // 1已赞，0未赞
                "content": "<a href='#'>#今天s天气好###我很帅3的呢#####</a>", // 内容（权限联动）
                "state":"1", // 帖子状态 1 正常， 2 隐藏，3已违规
                "is_public":"1", // 1 公开，2私有
                 "topic_list": [  // 话题
                    {
                        "id": 77, // 话题id
                        "name": "我是画家" // 话题名字
                    }
                ],
                "resources": [ // 资源（权限联动）,
                    {
                        "type": 2, // 1图片，2视频
                        "src": "视频地址",
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196, // 图片高度px
                            "cover_url": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/image/dynamic/bf7c937bef2349d98d142a629443be15.gif", // gif图片 type=2可用
                            "water_src": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/4f1fd9198279177ed3a4b9c196959257-fd.mp4" // 水印视频 type=2可用
                        },
                        "ext":"扩展字段" // string
                    },
                    {
                        "type": 1, // 1图片，2视频
                        "src": "图片地址",
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196 // 图片高度px
                        },
                        "ext":"扩展字段" // string
                    }
                ],
                "clique": {
                    "title": "万年小朋友", // 圈子名称
                    "id": 6 // 圈子id
                }
            ],
            "current_page": 1,
            "per_page": 1,
            "total": 39,
            "last_page": 39
        }
    }
```

#### [首页](../../readme.md)
