# 修改帖子状态

#### 接口地址

-   platform/post/update/state

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "post_id":"12", // 帖子id
        "state":"1" // 帖子状态 1 正常， 2 隐藏，3已违规
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
