# 删除帖子

#### 接口地址

-   platform/post/delete

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "post_id":"1", // 帖子id，必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
