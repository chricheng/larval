# 创建圈子

#### 接口地址

-   platform/clique/update

#### 请求方式

-   post

#### Content-Type

- multipart/form-data

#### 请求参数（请带上全局请求参数）

```json
    {
        "id":"圈子id", // 圈子id
        "title":"咋了", // 圈子title
        "description":"123", // 圈子描述
        "pron":"人", // 参与的代称， 人，歌迷，影迷
        "background":"文件", // 背景图
        "background_two":"文件" // 第二个背景图，非必填
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "操作成功",
        "data": []
    }
```

#### [首页](../../readme.md)
