# 评论+回复 混合列表 （慎用， 使用了mysql的union all 影响性能）

#### 接口地址

-   platform/comment_reply/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "limit":"20", // 每页数量 默认20
        "query[token]":"用户token", // 用户token
        "query[start_time]":"2012-10-12", // 时间 Y-m-d H:i:s
        "query[end_time]":"2012-10-12", // 时间 Y-m-d H:i:s
        "query[type]":"" // 评论类型 默认全部，comment评论， reply回复
    }
```

#### 返回参数

```json
    {
    "status": 200,
    "info": "success",
    "data": {
        "list": [
            {
                "id": 496,
                "content": "傻了吧三级@1345005 8句步步错",
                "resource_type": 1,
                "post_id": 2402,
                "praise_num": 0,
                "created_at": "2020-03-03 15:03:31",
                "updated_at": "2020-03-03 15:03:31",
                "type": "reply",
                "post": {
                    "id": 2402,
                    "post_type": 1,
                    "content": "5部不夸拒绝不咯摸摸肯能@1345005 卡特教练"
                }
            }
        ],
        "current_page": 1,
        "per_page": 20,
        "total": 717,
        "last_page": 36
    }
```

#### [首页](../../readme.md)
