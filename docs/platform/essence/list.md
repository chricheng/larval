# 加精申请列表

#### 接口地址

-   platform/essence/index

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "limit":"20", // 每页数量 默认20
        "page":1, // 页码
        "state":"" // 空 全部 ，0 申请中，1成功，2驳回
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "id": 336, // 申请id
                    "post_id": 878, // 帖子id
                    "state": 0,  // 申请状态 0 申请中，1成功，2驳回
                    "type": "repo", // 类型 Repo repo ， 私影 snapshot
                    "created_at": "2019-11-07 10:12:14", // 申请时间
                    "updated_at": "2019-11-07 10:13:01", // 修改时间，代表通过、驳回时间
                    "post": { // 帖子详情
                        "id": 89, // 帖子id
                        "post_type": 2, // 帖子类型 1普通帖子，2图文文章(富文本)
                        "state": 1, // 帖子状态 1 正常， 2 隐藏，3已违规
                        "is_public": 2, // 1 公开，2私有
                        "created_at": "2019-08-23 03:43:13", // 创建时间
                        "updated_at": "2019-08-23  03:43:13", // 修改时间
                        "user": {
                            "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户标识
                        },
                        "repo_id":1, //  repo id
                        "essence_type":"repo", // 类型 Repo repo ， 私影 snapshot
                        "comment_num": 0, // 评论数量
                        "praise_num": 0, // 点赞数量
                        "favorite_num": 0, // 收藏数量
                        "share_num": 0, // 分享数量
                        "favorite": 0, // 1已收藏，0未收藏
                        "title": "不是测试行么？", // 标题（权限联动）
                        "praise": 0, // 1已赞，0未赞
                        "content": "<a href='#'>#今天s天气好###我很帅3的呢#####</a>", // 内容（权限联动）
                        "resources": [ // 资源（权限联动）,
                            {
                                "type": 2, // 1图片，2视频
                                "src": "视频地址",
                                "config": {
                                    "width": 250, // 图片宽度px
                                    "height": 196, // 图片高度px
                                    "cover_url": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/image/dynamic/bf7c937bef2349d98d142a629443be15.gif", // gif图片 type=2可用
                                    "water_src": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/4f1fd9198279177ed3a4b9c196959257-fd.mp4" // 水印视频 type=2可用
                                },
                                "ext":"扩展字段" // string
                            },
                            {
                                "type": 1, // 1图片，2视频
                                "src": "图片地址",
                                "config": {
                                    "width": 250, // 图片宽度px
                                    "height": 196 // 图片高度px
                                },
                                "ext":"扩展字段" // string
                            }
                        ]
                    },
                    "topic_list": [ // 列表
                        {
                            "name": "还逗我"
                        }
                    ],
                },
            ],
            "current_page": 1,
            "per_page": 20,
            "total": 3,
            "last_page": 1
        }
    }
```

#### [首页](../../readme.md)
