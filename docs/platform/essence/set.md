# 帖子设置加精

#### 接口地址

-   platform/essence/set

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "post_id":"609", // 加精列表id
        "type":"repo" Repo repo, 私影 snapshot  
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "操作成功",
        "data": []
    }
```

#### [首页](../../readme.md)
