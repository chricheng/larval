# 话题绑定，解绑

#### 接口地址

-   platform/topic/relation

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```json
    {
        "topic_id":"话题id", // 必填
        "post_id":"帖子id", // 必填
        "action_type":"1", // 1绑定，2解绑
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "处理成功",
        "data": []
    }
```

```json
    {
        "status": 500,
        "info": "失败原因",
        "data": []
    }
```

#### [首页](../../readme.md)
