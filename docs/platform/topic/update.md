# 话题列表

#### 接口地址

-   platform/topic/update

#### 请求方式

-   post

#### header

-   Content-Type: multipart/form-data

#### 请求参数（请带上全局请求参数） form_data

```
    {
        "id":"1",  // 必填
        "ext":"扩展字段", // 扩展字段
        "image":"文件", // 文件
        "description":"描述", // 扩展字段
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "修改话题成功",
        "data": []
    }
```

#### [首页](../../readme.md)
