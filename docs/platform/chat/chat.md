# 私信 聊天内容列表

#### 接口地址

-   platform/chat/list

#### 请求方式

-   get

#### 请求参数（带上全局请求参数）

page 页数,可选
user_token 要查的用户token 可选

#### 返回参数

```json

  "status": 200,
  "info": "ok",
  "data": {
    "current_page": 1,
    "last_page": 1,
    "per_page": 50,
    "total": 32
    "data": [
      {
        "id": 1,
        "from_user_id": 22,
        "to_user_id": 135,
        "type": 0,
        "content": "文字传递真情.",
        "is_read": 0,
        "risk_content": null,
        "created_at": "2020-04-02 18:04:26",
        "updated_at": "2020-04-02 18:04:26",
        "from_user": {
          "id": 22,
          "token": "803f348027fecd9fba3981efeec69553"
        },
        "to_user": {
          "id": 135,
          "token": "54ffb86f89ba709ad7b1929324b2e6bb"
        }
      },
```

#### [首页](../../readme.md)
