# 查询用户封禁状态,多个token

#### 接口地址

-   platform/user/ban/multiple

#### 请求方式

-   get


#### 请求参数（请带上全局请求参数） 

```json
    {
        "user_tokens":"46acef73caca3f05cf21fd7c3d57e195", // 用户token多个逗号隔开
    }
```

#### 返回参数

```json
    {
    "status": 200,
    "info": "success",
    "data": [ // 多个
        {
            "token": "46acef73caca3f05cf21fd7c3d57e195", // 当前用户token
            "list": [
                {
                    "type": "post", // 封禁类型 post comment
                    "info": "测试封禁", // 封禁理由
                    "shielded_at": "2020-02-18 14:00:00" // 封禁截止时间
                },
                {
                    "type": "comment",// 封禁类型 post comment
                    "info": "测试封禁",
                    "shielded_at": "2020-02-18 14:00:00"
                }
            ]
        }
    ]
}
```

#### [首页](../../readme.md)
