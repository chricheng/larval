# 用户设置限制

#### 接口地址

-   platform/user/ban/add

#### 请求方式

-   post


#### 请求参数（请带上全局请求参数） 

```json
    {
        "user_token":"46acef73caca3f05cf21fd7c3d57e195", // 需要封禁的用户token
        "type":"comment",  // 需要封禁的行为, comment:评论,post:发帖
        "shielded_at":"2020-02-18 14:00:00", // 封禁的截止时间
        "info":"测试封禁", // 封禁期限, 行为提示
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
