# 用户解除限制

#### 接口地址

-   platform/user/ban/cancel

#### 请求方式

-   post


#### 请求参数（请带上全局请求参数）

```json
    {
        "user_token":"46acef73caca3f05cf21fd7c3d57e195", // 需要封禁的用户token
        "type":"comment",  // 需要解除的行为, comment:评论,post:发帖
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
