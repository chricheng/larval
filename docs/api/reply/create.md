# 评论回复

#### 接口地址

-   api/reply/create

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```json
    {
        "token":"token", //用户token
        "comment_id":"1",// 评论id，必填
        "reply_id":"1",// 回复的id 若回复评论则为0
        "content":"*#06#", // 回复内容
        "resource_type":0, // 资源类型 0 默认无资源，1 图片  2 视频    //TODO
        "resources":[  // 资源id 多个传数组, 详细请看上传接口 //选填
            "资源key，key请查看上传接口"
        ],
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "id": 18,       // 回复id
            "post_id": "66" // 帖子id
        }
    }
```

#### [首页](../../readme.md)
