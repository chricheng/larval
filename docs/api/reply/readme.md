# 回复列表

#### 接口地址

-   api/reply/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "comment_id":"1" // 评论id，必填
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "id": 15, // 回复id
                    "content": "哈哈哈 #咋弄啊# #这好说哦#", // 回复内容
                    "post_id": 66, // 帖子id
                    "comment_id": 14, // 评论id
                    "reply_id": 0, // 回复上一级id
                    "praise_num":0, // 点赞数量
                    "praise": 0, // 点赞状态 0未点赞，1已点赞
                    "created_at": "2019-08-23 07:53:11", // 回复时间
                    "updated_at": "2019-08-23 07:53:11", // 回复修改时间
                    "user": {
                        "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户token
                        "follow": 0 // 关注状态
                    },
                    "to_user": {
                        "token": "bec67443d6b1721589dac3f5d3ccb5be", // 上一个内容的用户token
                        "follow": 0 // 关注状态
                    },
                    "resource_type":1, // 资源类型 0 默认无资源，1 图片  2 视频    //TODO
                    "resources": [ // 资源（权限联动）,
                        {
                            "type": 1, // 1图片，2视频
                            "src": "图片地址",
                            "config": {
                                "width": 250, // 图片宽度px
                                "height": 196 // 图片高度px
                            },
                            "ext":"扩展字段" // string
                        }
                    ],
                }
            ],
            "current_page": 1,
            "per_page": 10,
            "total": 13,
            "last_page": 2
        }
    }
```

#### [首页](../../readme.md)
