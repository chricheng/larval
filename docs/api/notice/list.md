# 通知列表

#### 接口地址

-   api/notice/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "type":"1, 2, 3", // 多个逗号隔开  1系统, 2关注 3评论 4点赞 5艾特@ 6转发
        "page":"1",   // 页码
        "limit":"20"  // 每页显示数量
    }
```

#### 返回参数

```
    {
    "status": 200,
    "info": "success",
    "data": {
        "list": [
            {
                "id": 22,
                "type": 1,  // 类型，1系统, 2关注 3评论 4点赞 5艾特@ 6转发
                "path_type": 0, // 跳转类型 1 帖子详情 2 评论详情 3 用户中心
                "content": { // 推送内容json
                    "msg": "赞了你的帖子", // 推送内容
                    "post_id": "92",
                    "comment_id": "评论id",// 评论id
                    "post": {
                        "id": "92",      // 帖子id
                        "resources": [   // 帖子资源
                            {
                                "type": 1, // 资源类型
                                "content": "ssss", // 帖子内容
                                "state": 1, // 帖子状态  1 正常，2 隐藏，3已违规
                                "is_public": 1, // 隐私状态  1 公开，2 私有
                                "src": "http://192.168.2.21:8899/storage/resource/images/20190823/ewz9t0yz3nnxBs8iVBzxGEo2zs0hwadhxic5rn1q.png", // 资源
                                "config": {
                                    "width": 250,  // 宽度
                                    "height": 196  // 高度
                                },
                                "ext": "" // 扩展字段
                            }
                        ]
                    }
                },
                "ext": "", // 扩展字段
                "user": {
                    "token": "bec67443d6b1721589dac3f5d3ccb5be" // token
                }
            }
        ],
        "current_page": 1,
        "per_page": 100,
        "total": 2,
        "last_page": 1
    }
}
```

#### [首页](../../readme.md)
