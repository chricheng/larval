# 通知列表

#### 接口地址

-   api/notice/number

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"1"  // 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "praise": 20, // 新的赞数量
            "comment": 0, // 新的评论数量
            "at": 0,      // 新的at数量
            "follow": 0   // 新的关注数量
        }
    }
```

#### [首页](../../readme.md)
