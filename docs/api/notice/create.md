# 通知

#### 接口地址

-   api/notice/create

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```

    // 系统通知
    {
        "token":"2", // 用户token 必填
        "to_token":"3", // 用户token 多个逗号隔开，最多1千个非必填
        "type":"1",
        "path_type":"1", // 3帖子详情
        "content": {'msg':'通知的内容', 'post_id':'帖子id'}, // 内容必填
        "ext": "" // 扩展字段
    }

    // 关注通知
    {
        "token":"2", // 用户token 必填
        "to_token":"3", // 用户token 多个逗号隔开，最多1千个非必填
        "type":"2",
        "path_type":"3", // 3用户详情
        "content": {'msg':'通知内容'}, // 内容必填
        "ext": "" // 扩展字段
    }

    // 评论通知
    {
        "token":"1", // 用户token 必填
        "to_token":"2", // 用户token 多个逗号隔开，最多1千个非必填
        "type":"3", // 评论
        "path_type":"1", // 1 帖子详情 2 评论详情
        "content": {'msg':'评论了你的帖', 'post_id':'帖子id', 'comment_id':'评论id'}, // 内容必填
        "ext": "" // 扩展字段
    }

    // 点赞
    {
        "token":"1", // 用户token 必填
        "to_token":"2", // 用户token 多个逗号隔开，最多1千个非必填
        "type":"4", // 点赞
        "path_type":"1", // 1 帖子 2 评论
        "content": {'msg':'点赞了你的帖', 'post_id':'帖子id', 'comment_id':'评论id'}, // 内容必填
        "ext": "" // 扩展字段
    }

    // @艾特通知
    {
        "token":"1", // 用户token 必填
        "to_token":"2", // 用户token 多个逗号隔开，最多1千个非必填
        "type":"5", // 艾特
        "path_type":"3", // 3 用户详情
        "content": {'msg':'艾特了你', 'post_id':'帖子id'}, // 内容必填
        "ext": "" // 扩展字段
    }

```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {}
    }
```

#### [首页](../../readme.md)
