# 用户推荐（未关注） 随机

#### 接口地址

-   api/user/rand

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "num":"6", // 随机数量，选填 默认6条
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": [
            {
                "token": "5b89fceff287b9ee6d087384a4d150c5"
            },
            {
                "token": "5ee63f34202ca1d93aa526eeb33cd375"
            },
            {
                "token": "11a680c358affb197c241da4ef34df1c"
            },
            {
                "token": "09bf7b358c4522291ad9ee81bd3b0c77"
            },
            {
                "token": "a24dde5869c300c32fbc8cf512cbbac6"
            }
        ]
    }
```

#### [首页](../../readme.md)
