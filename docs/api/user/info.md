# 用户推荐（未关注） 随机

#### 接口地址

-   api/user

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "user_token":"123123", // 用户token 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "token": "bec67443d6b1721589dac3f5d3ccb5be",
            "follow": 0,  // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
            "fans_num": 0, // 粉丝数量
            "follow_num": 1  // 关注数量
        }
    }
```

#### [首页](../../readme.md)
