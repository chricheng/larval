# 授权登录获取 token

#### 接口地址

-   api/login

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "unique_key":"222" //平台唯一标识，方便二次调用
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "token": "11fd485e14fa80cf9afa6f7850104ae1" //用户token
        }
    }
```

#### [首页](../../readme.md)
