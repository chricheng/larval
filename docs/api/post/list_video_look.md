# 帖子列表

#### 接口地址

-   api/post/video/look

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```json
    {
        "token":"123123", // token必填
        "id":1231, // 视频帖子id
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "设置阅读成功",
        "data": []
    }
```

#### [首页](../../readme.md)
