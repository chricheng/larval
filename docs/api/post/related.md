# 帖子相关推荐列表

#### 接口地址

-   api/post/related

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "id":"886", // 帖子id
        "limit":"10", // 推荐数量， 默认10个
        "post_type":"1" // TODO 新增帖子类型筛选字段 默认1 普通帖子1， 富文本2, 公告3
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": [
            {
                "id": 89, // 帖子id
                "post_type": 2, // 帖子类型 1普通帖子，2图文文章(富文本)
                "state": 1, // 帖子状态 1 正常， 2 隐藏，3已违规
                "is_public": 2, // 1 公开，2私有
                "created_at": "2019-08-23 03:43:13", // 创建时间
                "updated_at": "2019-08-23 03:43:13", // 修改时间
                "user": {
                    "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户标识
                    "follow": 1  // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
                },
                "comment_num": 0, // 评论数量
                "praise_num": 0, // 点赞数量
                "favorite_num": 0, // 收藏数量
                "share_num": 0, // 分享数量
                "resource_type":1, // 资源类型 0 默认无资源，1 图片  2 视频    //TODO
                "favorite": 0, // 1已收藏，0未收藏
                "title": "不是测试行么？", // 标题（权限联动）
                "praise": 0, // 1已赞，0未赞
                "content": "<a href='#'>#今天s天气好###我很帅3的呢#####</a>", // 内容（权限联动）
                "resources": [ // 资源（权限联动）,
                    {
                        "type": 2, // 1图片，2视频
                        "src": "视频地址",
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196, // 图片高度px
                            "cover_url": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/image/dynamic/bf7c937bef2349d98d142a629443be15.gif", // gif图片 type=2可用
                            "cover_url_static": "http://vcdn.community.didiyd.vip/sv/18d9d534-1715324cd46/18d9d534-1715324cd46.mp4?x-oss-process=video/snapshot,t_5000", // 静态封面 type=2可用
                            "water_src": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/4f1fd9198279177ed3a4b9c196959257-fd.mp4" // 水印视频 type=2可用
                        },
                        "ext":"扩展字段" // string
                    },
                    {
                        "type": 1, // 1图片，2视频
                        "src": "图片地址",
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196 // 图片高度px
                        },
                        "ext":"扩展字段" // string
                    }
                ],
                "clique": { // 圈子
                   "title": "quanquan", // 圈子名字
                    "id": 6  // 圈子id
                }
            }
        ]
    }
```

#### [首页](../../readme.md)
