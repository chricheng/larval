# 创建帖子

#### 接口地址

-   api/post/update

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    //  只有 resource_type = 0 的内容支持修改
    {
        "token":"lsdjfljl1j23lj12l308", // 用户token 必填
        "post_id":"帖子id",
        "title":"标题", // 标题 选填
        "content":"内容", // 内容 选填 【请看下方注意事项】
        "ext":"扩展字段", // 扩展字段 字符串 // 选填
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "post_id": 166  // 帖子id
        }
    }
```

#### [首页](../../readme.md)
