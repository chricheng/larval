# 用户中心收藏 (没卵用好像)

#### 接口地址

-   api/favorite/user

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "limit":"20", // 帖子每页数量
        "page":"1" // 页
        "user_token":"1" // 用户token 必填
    }
```

#### 返回参数

```json

    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "post_id": 89,
                    "content": "这里是帖子内容",
                    "resources": {
                        "type": 2, // 1图片，2视频
                        "src": "http://www.cmm.com/file/resource/video/20190823/kxHhbsMwCrBAarxcFWNaFusJnaxdkYbxaZ1rYdew.mp4", // 地址
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196, // 图片高度px
                            "cover_url": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/image/dynamic/bf7c937bef2349d98d142a629443be15.gif", // gif图片 type=2可用
                            "cover_url_static": "http://vcdn.community.didiyd.vip/sv/18d9d534-1715324cd46/18d9d534-1715324cd46.mp4?x-oss-process=video/snapshot,t_5000", // 静态封面 type=2可用
                            "water_src": "http://vcdn.community.didiyd.vip/fa25b714261344a78a54e684b8cd12dc/4f1fd9198279177ed3a4b9c196959257-fd.mp4" // 水印视频 type=2可用
                        },
                        "ext": ""
                    }
                }
            ],
            "current_page": 1,
            "per_page": 1,
            "total": 61,
            "last_page": 61
        }
    }
```

#### [首页](../../readme.md)
