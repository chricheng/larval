# 删除帖子

#### 接口地址

-   api/post/delete

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"lsdjfljl1j23lj12l308", // 必填
        "post_id":"1", // 帖子id，必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
