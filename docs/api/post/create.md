# 创建帖子

#### 接口地址

-   api/post/create

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```json
    {
        "token":"lsdjfljl1j23lj12l308", // 用户token 必填
        "title":"标题", // 标题 选填
        "content":"内容", // 内容 选填 【请看下方注意事项】
        "post_type":"帖子类型", // 帖子类型 1普通帖子，2图文文章(富文本)，3公告
        "is_public":1, // 是否隐私 1 公开，2私有
        "resource_type":0, // 资源类型 0 无资源，1图片  2视频  3图片+视频   //TODO
        "resources":[  // 资源id 多个传数组, 详细请看上传接口 //选填
            "e75093f18c9bfaf4a55f6127cb8f269d",
            "e30416f903cb88aeb989041f6ff8f1db"
        ],
        "ext":"扩展字段", // 扩展字段 字符串 // 选填
        "map_name": "测试", // 具体位置 // 选填
        "map_address": "我的定位", // 位置详细地址 // 选填
        "map_location": "116.470143,39.985959", // 位置坐标 // 选填

        "clique_id" :"6" // 圈子id 选填 TODO新增
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "post_id": 166  // 帖子id
        }
    }
```

#### [首页](../../readme.md)
