# 更新帖子分享数量

#### 接口地址

-   api/post/share/num

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "post_id":"66" // 必填 帖子
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
