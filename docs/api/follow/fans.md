# 用户粉丝列表

#### 接口地址

-   api/fans/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "user_token":"用户token", 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "created_at": "2019-08-26 03:30:45",
                    "updated_at": "2019-08-26 03:30:45",
                    "user": {
                        "token": "11fd485e14fa80cf9afa6f7850104ae1", // 用户token
                        "follow": 1 // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
                    }
                }
            ],
            "current_page": 1,
            "per_page": 10,
            "total": 1,
            "last_page": 1
        }
    }
```

#### [首页](../../readme.md)
