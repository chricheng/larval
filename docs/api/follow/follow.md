# 添加关注

#### 接口地址

-   api/follow

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"用户token", 必填
        "to_token":"关注的用户token", 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "following the success",
        "data": {
            "follow": 3 // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
        }
    }
```

#### [首页](../../readme.md)
