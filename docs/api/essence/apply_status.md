# 申请加精

#### 接口地址

-   api/essence/status

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"token", 用户token
        "post_id":"1" // 帖子id
    }
```

#### 返回参数

```
    // 成功
    {
        "status": 200,
        "info": "success",
        "data": []
    }

    // 失败 申请存在
    {
        "status": 500,
        "info": "the apply already exists",
        "data": []
    }
```

#### [首页](../../readme.md)
