# 私影列表

#### 接口地址

-   api/snapshot/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "limit":"20", // 帖子每页数量
        "page":"1" // 帖子页
        "snapshot_id":"1" // 最后一个snapshot_id 防止“上拉加载”的时候数据重复， 注意：“首次请求” 和 "下拉刷新" 不用携带
        "comment_num":0 // 跟随评论数量  eg：2 则会跟随两条评论
        "comment_type":1 // 跟随评论类型，默认最新评论 ，1 点赞大于5条切最高的
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                "id": 89, // 帖子id
                "post_type": 2, // 帖子类型 1普通帖子，2图文文章(富文本)
                "state": 1, // 帖子状态 1 正常， 2 隐藏，3已违规
                "is_public": 2, // 1 公开，2私有
                "created_at": "2019-08-23 03:43:13", // 创建时间
                "updated_at": "2019-08-23 03:43:13", // 修改时间
                "user": {
                    "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户标识
                    "follow": 1  // 关注状态 0 自己，1 未关注 2 已关注 3 已互关
                },
                "snapshot_id":1, //  私影id
                "comment_num": 0, // 评论数量
                "praise_num": 0, // 点赞数量
                "favorite_num": 0, // 收藏数量
                "share_num": 0, // 分享数量
                "favorite": 0, // 1已收藏，0未收藏
                "title": "不是测试行么？", // 标题（权限联动）
                "praise": 0, // 1已赞，0未赞
                "content": "<a href='#'>#今天s天气好###我很帅3的呢#####</a>", // 内容（权限联动）
                "resources": [ // 资源（权限联动）,
                    {
                        "type": 2, // 1图片，2视频
                        "src": "视频地址",
                        "config": {},
                        "ext":"扩展字段" // string
                    },
                    {
                        "type": 1, // 1图片，2视频
                        "src": "图片地址",
                        "config": {
                            "width": 250, // 图片宽度px
                            "height": 196 // 图片高度px
                        },
                        "ext":"扩展字段" // string
                    }
                ],
                "comment_list": [
                    {
                        "id": 34, // 评论id
                        "content": "哈哈哈 #咋弄啊# #这好说哦#", // 评论内容
                        "user": {
                            "token": "bec67443d6b1721589dac3f5d3ccb5be", // 评论用户
                            "follow": 0 // 关注状态
                        }
                    }
                ]
            ]
        }
    }
```

#### [首页](../../readme.md)
