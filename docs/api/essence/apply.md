# 申请加精

#### 接口地址

-   api/essence/apply

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"token", 用户token
        "type":"repo" // 加精品类型 repo | snapshot
        "post_id":"1" // 帖子id
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": []
    }
```

#### [首页](../../readme.md)
