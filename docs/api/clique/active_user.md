# 圈子活跃用户列表

#### 接口地址

-   api/clique/users

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "clique_id":"圈子id"
    }
```

#### 返回参数

```json
   {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "id": 13, // 榜单id
                    "clique_id": 6, // 圈子id
                    "active_value": 3, // 活跃值
                    "created_at": "2019-11-08 10:13:06", // 加入时间
                    "updated_at": "2019-11-08 10:13:06", // 修改时间
                    "user": {
                        "token": "0c975375dd76103e74207229db225d6e", // 用户token
                        "follow": 1 // 我与此用户的关注状态
                    }
                }
            ],
            "current_page": 1,
            "per_page": 20,
            "total": 1,
            "last_page": 1
        }
    }
```

#### [首页](../../readme.md)
