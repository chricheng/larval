# 关注、加入圈子

#### 接口地址

-   api/clique/action

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```json
    {
        "token":"用户token", 必填
        "id":"圈子id"
    }
```

#### 返回参数

```json
   {
        "status": 200,
        "info": "success to join clique",
        "data": {
            "type": 1 // 1关注状态，0取消状态
        }
    }
```

#### [首页](../../readme.md)
