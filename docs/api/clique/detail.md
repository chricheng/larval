# 圈子详情

#### 接口地址

-   api/clique/detail

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "limit":"20", // 帖子每页数量
        "page":"1" // 帖子页
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
            "id": 6,  // 圈子id
            "title": "123", // 圈子名称
            "description": "123", // 圈子描述
            "background": "http://www.cmm.com/file/clique/6/background/20191107/Hkfbwdr2xIiYMjAQWThOmR7ONaKt5ILcZL512gXC.png", // 圈子图片
            "background_two": "http://www.cmm.com/file/clique/6/background/20191107/Hkfbwdr2xIiYMjAQWThOmR7ONaKt5ILcZL512gXC.png", // 圈子第二个图片
            "pv": 12, // 浏览量
            "pron": "人", // 参与人 代称
            "user_num": 1, // 参与人数
            "post_num": 3, // 总帖子数
            "today_post_num": 3, // 今日帖子数量
            "created_at": "2019-11-07 16:28:01", // 创建时间
            "updated_at": "2019-11-08 11:16:47", // 修改时间
            "follow": 1, // 是否加入圈子 0未加入，1已加入
            "posts": [ // 公告帖
                {
                    "post_id": 920, // 帖子id
                    "title": null  // 帖子标题
                }
            ]
        }
    }
```

#### [首页](../../readme.md)
