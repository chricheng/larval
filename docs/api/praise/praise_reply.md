# 回复设置、取消点赞

#### 接口地址

-   api/praise/reply

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"用户token", 必填
        "reply_id":"回复id", 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success to cancel praise",
        "data": {
            "type": 0 // 0取消点赞，1设置点赞
        }
    }
```

#### [首页](../../readme.md)
