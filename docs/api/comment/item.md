# 评论详情

#### 接口地址

-   api/comment/item

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```json
    {
        "comment_id":"1", // 评论id
        "reply_num":"0", // 跟随回复数量 选填 默认0
        "reply_type":"1", // 跟随回复类型 选填  0：时间降序  1：点赞最多切大5的  3点赞降序排列，4点赞大于0降序排列
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "success",
        "data": {
           "id": 3, // 评论id
            "content": "哈哈哈", // 评论内容
            "post_id": 66,// 帖子id
            "reply_num": 0, // 回复数量
            "praise_num":0, // 点赞数量
            "created_at": "2019-08-23 06:43:02", //创建时间
            "updated_at": "2019-08-23 06:43:02",// 修改时间
            "user": {
                "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户token
                "follow": 0 // 关注状态
            }
            "reply_list": [
                {
                    "id": 23, // 回复id
                    "content": "哈哈哈 #咋弄啊# #这好说哦#", // 回复内容
                    "user": { //
                        "token": "bec67443d6b1721589dac3f5d3ccb5be", // 用户token
                        "follow": 0 // 关注状态
                    },
                    "resource_type":1, // 资源类型 0 默认无资源，1 图片  2 视频    //TODO
                    "resources": [ // 资源（权限联动）,
                        {
                            "type": 1, // 1图片，2视频
                            "src": "图片地址",
                            "config": {
                                "width": 250, // 图片宽度px
                                "height": 196 // 图片高度px
                            },
                            "ext":"扩展字段" // string
                        }
                    ],
                }
            ],
            "resource_type":1, // 资源类型 0 默认无资源，1 图片  2 视频    //TODO
            "resources": [ // 资源（权限联动）,
                {
                    "type": 1, // 1图片，2视频
                    "src": "图片地址",
                    "config": {
                        "width": 250, // 图片宽度px
                        "height": 196 // 图片高度px
                    },
                    "ext":"扩展字段" // string
                }
            ],
        }
    }
```

#### [首页](../../readme.md)
