# 某个话题的几个有图的火帖子的第一个图片

#### 接口地址

-   api/topic/post/hot

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "topicids":"13,14", // 话题id，多个用id隔开
        "limit":"3" // 帖子数量
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "id": 20,   // 话题id
            "name": "阿道夫", // 话题名字
            "post_num": 0, // 关联帖子数量
            "comment_num": 0, // 关联评论数量
            "reply_num": 0, // 关联评论数量
            "ext": "ext", // 业务方扩展字段 string
            "created_at": "2019-09-11 17:21:43",
            "updated_at": "2019-09-11 17:29:50"
            "post": [
                {
                    "id": 369, // 帖子id
                    "resources": {
                        "type": 1, // 资源类型
                        "src": "http://0.0.0.0:8899/storage/resource/images/20190823/ewz9t0yz3nnxBs8iVBzxGEo2zs0hwadhxic5rn1q.png", // 图片
                        "config": {
                            "width": 250, // 图片宽
                            "height": 196 // 图片高
                        },
                        "ext": "" // 扩展
                    }
                }
            ]
        }
    }
```

#### [首页](../../readme.md)
