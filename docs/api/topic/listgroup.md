# 话题列表（根据多个 id 查询）

#### 接口地址

-   api/topic/group

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "ids":"1,2" // 话题id 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            {
                "id": 1,   // 话题id
                "name": "阿道夫", // 话题名字
                "description": "123", // 描述
                "image": "http://192.168.2.21:8899/storage/topic/images/20190925/8YZRc7pClUAFP86sfK9imSOjEAaaq5q7QgSFMeX3.png", // 图片
                "post_num": 0, // 关联帖子数量
                "comment_num": 0, // 关联评论数量
                "reply_num": 0, // 关联评论数量
                "ext": "ext", // 业务方扩展字段 string
                "created_at": "2019-09-11 17:21:43",
                "updated_at": "2019-09-11 17:29:50"
            }
        }
    }
```

#### [首页](../../readme.md)
