# 获取用户全部未读消息列表
接口可以重复拉取未读.
如果失败需要重试,来保证用户一定能成功拉取新消息.
只有当标记已读后才不会拉取到
需要客户端自己处理去重问题.

消息类型:  0 文字 1 图片
图片的时候content为json
图片为单独的一条消息.消息类型可能扩展,后期可能增加卡片类型,客户端需要提前预处理"不支持的消息类型"

#### 接口地址

-  api/chat/unread 

#### 请求方式

-   get

#### 请求参数（带上全局请求参数）


#### 返回参数

```json
{
    "status": 200,
    "info": "ok",
    "data":[
    {
        "id": 1, //消息id
        "from_user_token": 'bba34ac20034d28c9e3d2da690a81beb',
        "type": 0,
        "content": "文字传递真情.", //文字 | json数组
        "created_at": "2020-04-02 18:04:26"
    },
    {
        "id": 17,
        "type": 1,
        "content": "{\"src\":\"chat\\/6\\/135\\/KpI0zb1SE2w2lJyrqe7vI5iSjTIUdobsdsRqHtI7.jpeg\",\"config\":{\"width\":1298,\"height\":348}}",
        "created_at": "2020-04-03 19:01:01",
        "from_user_token": "54ffb86f89ba709ad7b1929324b2e6bb"
    }
    ]
} 
```
