#  标记已读

标记某个对话的全部消息为已读 

#### 接口地址

-  api/chat/markread

#### 请求方式

-   post

#### 请求参数（带上全局请求参数）
from_user_token  必填 string  发消息的用户token  

max_message_id   必填 int     客户端已经收取到的最大消息id

#### 返回参数

```json
{
    "status": 200,
    "info": "ok",
    "data":[ ]
} 
```
