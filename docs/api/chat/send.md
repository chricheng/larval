#  发私信

消息类型:  0 文字 1 图片
发文字的时候 content为string
发图片的时候 content为空,file字段为图片

图片为单独的一条消息.消息类型可能扩展,后期可能增加卡片类型

#### 接口地址

-  api/chat/send

#### 请求方式

-   post

#### 请求参数（带上全局请求参数）
to_user_token  必填 string  接收用户token  
type 必填 int  消息类型 0文字 1图片  
content  string  消息内容,文字的时候必填,不得超过500字;图片的时候为空  
file  resource  图片内容,type为1的时候必填,不得超过10MB  

#### 返回参数

```json
{
    "status": 200,
    "info": "ok",
    "data":{ "img_url":"http://xxoo.png" }
}

如果消息为图片,data里有img_url字段为图片url 

status 201为报错
可能的错误:
type err 类型错误
content max size exceed 消息内容太大
image too large 图片太大 
send chat message failed 发送失败


status 202为报错
触发了风险内容

```
