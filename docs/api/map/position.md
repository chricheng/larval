# 定位搜索周边

#### 接口地址

-   api/map/position

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "limit":"50", // 帖子每页数量 默认50
        "page":"1", // 帖子页
        "location:"116.473168,39.993015" // 中心点坐标 必填
        "keywords":"天通苑" // 	查询关键词 选填
        "driver":"gao", //驱动， gao/baidu baidu还未开发 选填
        "radius":"1000" //  查询半径 取值范围:0-50000。规则：大于50000按默认值，单位：米  选填
        "types":"011100" // 查询POI类型 选填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "id": 12,
                    "name": "这好说哦",
                    "post_num": 1, // 关联帖子数量
                    "comment_num": 2, // 关联评论数量
                    "reply_num": 3, // 关联回复数量
                    "created_at": "2019-08-23 06:53:33",
                    "updated_at": "2019-08-23 06:53:33"
                }
            ],
            "current_page": 1,
            "per_page": 1,
            "total": 10,
            "last_page": 10
        }
    }
```

#### [首页](../../readme.md)
