# 评论视频上传

#### 接口地址

-   api/upload/comment/video

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）form_data

```json
    {
        "video":"文件体", // 文件体
        "ext":"扩展字段" // string
    }
```

- video 支持类型

```json
    支持类型 mimes .flv  对应的 mimetypes   video/x-flv
    支持类型 mimes .mp4  对应的 mimetypes   video/mp4
    支持类型 mimes .m3u8 对应的 mimetypes   application/x-mpegURL
    支持类型 mimes .ts   对应的 mimetypes   video/MP2T
    支持类型 mimes .3gp  对应的 mimetypes   video/3gpp
    支持类型 mimes .mov  对应的 mimetypes   video/quicktime
    支持类型 mimes .avi  对应的 mimetypes   video/x-msvideo
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "视频上传成功",
        "data": {
            "key": "ab8073f651389453597177b17ab7e860", // 文件标识
            "resource": {
                "type": "2", // 1图片，2视频
                "src": "视频地址， 原图",,
                "ext": "", // 扩展字段
                "config": {}
            }
        }
    }
```

#### [首页](../../readme.md)
