# 评论图片上传

#### 接口地址

-   api/upload/comment/image

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）form_data

```json
    {
        "image":"文件体", // 文件体
        "ext":"扩展字段", // string
        // 水印配置 //不传不加水印
        "water_config":{
            "water_str" : "http://dsmall.yangdev.admin.com/loapi/post/makeWaterImg?userid=7", //水印图片
            "water_x" : 20, //相对于postion 的x
            "water_y" : 20, //相对于postion 的y
            "water_position": "bottom-left" // 默认 'bottom-left','left-right','top','top-right','left','center','right','bottom','bottom-right'
        }
    }
```
- image 支持类型

```json
    支持类型 mimes .jpeg  
    支持类型 mimes .png 
    支持类型 mimes .gif   
    支持类型 mimes .bmp  
    支持类型 mimes .svg  
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "图片上传成功",
        "data": {
            "key": "ab8073f651389453597177b17ab7e860", // 文件标识
            "resource": {
                "type": 1, // 1图片，2视频
                "src": "图片地址， 原图",
                "config": {
                    "width": 250, // 图片宽度px
                    "height": 196 // 图片高度px
                },
                "ext":"扩展字段" // string
            }
        }
    }
```

#### [首页](../../readme.md)
