# 帖子阿里云视频上传

#### 接口地址

-   api/upload/videoal

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）form_data

```json
    // 上传视频接口 60分钟最多30次
    {
        "token":"必填",
        "video":"文件体", // 文件体
        "ext":"扩展字段", // string
        "gif_conf":{ // gifconfig
            "start_time":3, // 开始时间
            "end_time":6 // 结束时间
        }
    }
```

#### 返回参数

```json
    {
        {
            "status": 200,
            "info": "视频上传成功",
            "data": {
                "key": "a430c7c1e02a915363ae966b18817187"
            }
        }
    }
```

#### [首页](../../readme.md)
