# 帖子阿里云视频获取上传凭证

#### 接口地址

-   api/upload/videoal/create

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）form_data

```json
    {
        "token":"必填",
        "title":"文件名字", // 没有的话随机给一个
        "file_name":"文件扩展名", // 需要给正确的要不然无法处理 eg:12313.mp4
        "gif_conf":{ // gifconfig
            "start_time":3,
            "end_time":6
        },
        "ext":"扩展字段", // string
    }
```

#### 返回参数

```json
    {
        "status": 200,
        "info": "获取上传凭证成功",
        "data": {
            "key": "24320c834d44c7bf48f46242eba19730" // 资源key
            "video_id": "4b6f09f91d0c4f26b2cee4f2a1f34db2",
            "upload_address": "eyJFbmRwb2ludCI6Imh0dHBzOi8vb3NzLWNuLXNoYW5naGFpLmFsaXl1bmNzLmNvbSIsIkJ1Y2tldCI6Im91dGluLTAxM2U4MDliNzMyYzExZWFiZWJkMDAxNjNlMWM5NGE0IiwiRmlsZU5hbWUiOiJvcmlnaW5hbC93b3JrZmxvdy8zMGExOWJjZC0xNzEzZTEyNzdjOC0wMDA0LTY2NTMtYjRmLTIxMDE2Lm1wNCJ9",
            "request_id": "A4F0415F-4609-4318-B416-79A1B02C65D7",
            "upload_auth": "eyJTZWN1cml0eVRva2VuIjoiQ0FJUzFnUjFxNkZ0NUIyeWZTaklyNWJSSHRqQjM1dERnbyt1YUdUQ3IwczBXZHNidGFUcHNqejJJSDVFZW5OcUF1d2F2Lzh5bEd0VDZQZ1psclVxRWNRVkdSU1ZNNUlvdjg0THFWNzlKcGZadjh1ODRZQURpNUNqUWVrM3hvUUFtcDI4V2Y3d2FmK0FVQS9HQ1RtZDVNMFlvOWJUY1RHbFFDWnVXLy90b0pWN2I5TVJjeENsWkQ1ZGZybC9MUmRqcjhsbzF4R3pVUEcyS1V6U24zYjNCa2hsc1JZZTcyUms4dmFIeGRhQXpSRGNnVmJtcUpjU3ZKK2pDNEM4WXM5Z0c1MTlYdHlwdm9weGJiR1Q4Q05aNXo5QTlxcDlrTTQ5L2l6YzdQNlFIMzViNFJpTkw4L1o3dFFOWHdoaWZmb2JIYTlZcmZIZ21OaGx2dkRTajQzdDF5dFZPZVpjWDBha1E1dTdrdTdaSFArb0x0OGphWXZqUDNQRTNyTHBNWUx1NFQ0OFpYVVNPRHREWWNaRFVIaHJFazRSVWpYZEk2T2Y4VXJXU1FDN1dzcjIxN290ZzdGeXlrM3M4TWFIQWtXTFg3U0IyRHdFQjRjNGFFb2tWVzRSeG5lelc2VUJhUkJwYmxkN0JxNmNWNWxPZEJSWm9LK0t6UXJKVFg5RXoycExtdUQ2ZS9MT3M3b0RWSjM3V1p0S3l1aDRZNDlkNFU4clZFalBRcWl5a1QwbkZncGZUSzFSemJQbU5MS205YmFCMjUvelcrUGREZTBkc1Znb0psS0VwaUdXRzNSTE5uK3p0Sjl4YmtlRStzS1VsUFdYL3NNNFFRRnh2WXBVVkZpSWVOeGdwVkErdS9Mc3RCbksrNzYvV3l6dCtYQTU5ZGplOW8wSXEya2NKNnY2MmJMTTVHQ0Q0eWJJUHZaa3dQMjZBak0vSFU2RkhGVmkyKzJYaTM0OW9CUU1ybnE1SVI1MzZTN0tpemJ1SnBkQWpxRGFuUzRVV2Z3Tng3L0NOeDZrLzNSOUQreU83NDBDVVBoWllQdDBWZkt4emErS09pZ1pBYm5hR29BQkExN2EwQXFQZWhDaEh4ckRvd3Z4ZGhtSk5tcU4wL2l5amorb052a0JubWkxaTF2NWw4YURqanRMWmhONDFrOUxYWGxyUTZUUEtoeldtMGxqN3VrdkU4eVMvdW50OXFVZk5iMVlRRXdleDA4M3dnMk91MEpWbi9CT0d0Z2xVYXRJWWtSQWNFbEZudDNHWjE0citGNy9BRlVRdUJwcnlHM2VZUXlvRktQdGE1MD0iLCJBY2Nlc3NLZXlJZCI6IlNUUy5OVWRVYnUyRGI1TUVqQnNLS2FVVDdaYUJQIiwiRXhwaXJlVVRDVGltZSI6IjIwMjAtMDQtMDNUMDQ6MjU6MjlaIiwiQWNjZXNzS2V5U2VjcmV0IjoiNmpTM2pFSFNuZDJ6MkczNFNZdU01NVNlVUZ3NEZlY0dWOEFuVDRQVlplUngiLCJFeHBpcmF0aW9uIjoiMzYwMCIsIlJlZ2lvbiI6ImNuLXNoYW5naGFpIn0=",
        }
    }
```

#### [首页](../../readme.md)
