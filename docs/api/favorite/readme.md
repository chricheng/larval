# 我的收藏列表

#### 接口地址

-   api/favorite/list

#### 请求方式

-   get

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"用户token", 必填
        "page":20, 页数
        "limit":20, 每页数量
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success",
        "data": {
            "list": [
                {
                    "id": 6,
                    "post_id": 66,
                    "created_at": "2019-08-26 07:47:44",
                    "updated_at": "2019-08-26 07:47:44",
                    "post": { // 帖子详情，详细请看帖子详情
                        "id": 66,
                        "post_type": 1,
                        "created_at": "2019-08-22 07:48:42",
                        "updated_at": "2019-08-22 07:48:42",
                        "favorite": 1,
                        "title": "不是测试行么？",
                        "praise": 1,
                        "content": "#今天天气好###我很帅的呢#####",
                        "comment_num": 0, // 评论数量
                        "praise_num": 0, // 点赞数量
                        "favorite_num": 0, // 收藏数量
                        "share_num": 0, // 分享数量
                        "topic_list": [
                            {
                                "name": "今天天气好"
                            },
                            {
                                "name": "我很帅的呢"
                            }
                        ],
                        "resources": []
                    }
                }
            ],
            "current_page": 1,
            "per_page": 10,
            "total": 1,
            "last_page": 1
        }
    }
```

#### [首页](../../readme.md)
