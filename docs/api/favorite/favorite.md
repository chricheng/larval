# 添加、删除收藏

#### 接口地址

-   api/favorite

#### 请求方式

-   post

#### 请求参数（请带上全局请求参数）

```
    {
        "token":"用户token", 必填
        "post_id":"帖子id", 必填
    }
```

#### 返回参数

```
    {
        "status": 200,
        "info": "success to add favorite",
        "data": {
            "type": 0 // 0取消收藏，1添加收藏
        }
    }
```

#### [首页](../../readme.md)
