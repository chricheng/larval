## API 接口文档

-   开始

    -   [描述](info.md)
    -   [全局参数](global.md)

## 平台后台

-   帖子相关
    -   [帖子列表](platform/post/list.md)
    -   [修改帖子状态](platform/post/update_state.md)
    -   [删除帖子](platform/post/delete.md)
-   话题相关
    -   [话题列表](platform/topic/readme.md)
    -   [话题详情](platform/topic/item.md)
    -   [修改话题](platform/topic/update.md)
    -   [话题绑定，解绑](platform/topic/relation.md)

-  帖子加精
    -   [加精申请列表](platform/essence/list.md)
    -   [加精通过](platform/essence/pass.md)
    -   [加精驳回](platform/essence/cancel.md)

    -   [现有帖子设置加精](platform/essence/set.md)
    -   [现有帖子取消加精](platform/essence/del.md)
    
-  圈子管理
    -   [圈子列表](platform/clique/list.md)
    -   [圈子详情](platform/clique/detail.md)
    -   [创建圈子](platform/clique/create.md)
    -   [修改圈子](platform/clique/update.md)
- 用户封禁/解封
    -   [用户封禁](platform/user/ban_add.md)
    -   [用户解封](platform/user/ban_cancel.md)
    -   [用户封禁状态](platform/user/ban_multiple.md)

- 数据
   -   [根据用户token查询帖子日数据](platform/stat/stat_user_post.md)


## 平台前台 API


-   帖子相关（重）
    -   [我的关注帖子列表（timeline）](api/post/timeline.md)
    -   [推荐列表 千人一面（recommend）](api/post/recommend.md)
    -   [搜索](api/post/search.md)
-   帖子相关

    -   **[帖子列表](api/post/list.md)**
    -   **[多个话题的帖子列表](api/topic/multiple_topic_post.md)**
    -   [用户中心帖子列表](api/post/userlist.md)
    -   [用户中心帖子图片列表](api/post/resourcelist.md)
    -   [帖子详情](api/post/readme.md)
    -   [帖子相关推荐](api/post/related.md)
    -   [帖子列表（多个帖子id）](api/post/multiple.md)
    -   [创建帖子](api/post/create.md)
    -   [修改帖子](api/post/update.md)
    -   [修改帖子公开状态](api/post/change_public.md)
    -   [删除帖子](api/post/delete.md)
    -   [分享数量增加(新)](api/post/update_share_num.md)

-   视频相关
    -   [帖子视频列表](api/post/list_video.md)
    -   [视频浏览打点](api/post/list_video_look.md)
    
-   资源
    -   [帖子上传普通视频](api/resources/video.md)
    -   [帖子上传图片](api/resources/image.md)

    -   [帖子阿里云视频上传](api/resources/aliyun/upload_video.md)
    -   [帖子阿里云视频获取上传凭证](api/resources/aliyun/create_upload_video.md)

    -   [评论上传视频](api/resources/comment_video.md)
    -   [评论上传图片](api/resources/comment_image.md)
    
-   话题

    -   [话题列表](api/topic/readme.md)
    -   [话题列表（根据多个 id 查询）](api/topic/listgroup.md)
    -   [话题详情](api/topic/item.md)
    -   [某个话题的几个有图的火帖子的第一个图片](api/topic/guiyi.md)

-   用户

    -   [个人中心](api/user/info.md)
    -   [多个 token 的用户信息](api/user/batch.md)
    -   [授权登录获取 token](api/user/login.md)
    -   [用户推荐（未关注） 随机](api/user/rand.md)

-   评论

    -   [评论列表](api/comment/readme.md)
    -   [评论详情](api/comment/item.md)
    -   [评论创建](api/comment/create.md)

-   回复

    -   [回复列表](api/reply/readme.md)
    -   [回复评论](api/reply/create.md)

-   关注

    -   [添加关注](api/follow/follow.md)
    -   [取消关注](api/follow/unfollow.md)
    -   [用户关注列表](api/follow/readme.md)
    -   [用户粉丝列表](api/follow/fans.md)

-   点赞/喜欢

    -   [我的点赞列表](api/praise/readme.md)
    -   [帖子点赞](api/praise/praise.md)
    -   [评论点赞](api/praise/praise_comment.md)
    -   [回复点赞](api/praise/praise_reply.md)
    -   [用户中心点赞/喜欢](api/post/praise.md)


-   收藏

    -   [我的收藏列表](api/favorite/readme.md)
    -   [收藏](api/favorite/favorite.md)
    -   [用户中心收藏](api/post/favorite.md)

-   其他

    -   [地图](api/map/position.md)
    -   [通知列表](api/notice/list.md)
    -   [通知数量](api/notice/number.md)
    -   [创建通知](api/notice/create.md)

-  加精
    -   [查看申请加精状态](api/essence/apply_status.md)
    -   [申请加精](api/essence/apply.md)
    -   [加精-repo列表](api/essence/repo.md)
    -   [加精-私影列表](api/essence/snapshot.md)

-  圈子
    -   [所有的圈子列表](api/clique/list.md)
    -   [我关注的圈子列表](api/clique/my.md)
    -   [圈子列表(多个id)](api/clique/multiple.md)
    -   [圈子详情](api/clique/detail.md)
    -   [关注、加入圈子](api/clique/follow.md)
    -   [圈子-新帖列表](api/clique/new_list.md)
    -   [圈子-热帖列表](api/clique/hot_list.md)
    -   [圈子-新评论帖列表](api/clique/comment_list.md)
    -   [圈子活跃用户列表](api/clique/active_user.md)
    
- 私信 
    -  [设计&流程](api/chat/index.md)
    -  [获取未读消息列表](api/chat/unread.md)
    -  [发私信](api/chat/send.md)
    -  [标记已读](api/chat/markread.md)

