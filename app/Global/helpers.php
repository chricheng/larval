<?php

use Illuminate\Http\JsonResponse;


/**
 * @param $oriArr
 * @param $fieldArr
 *  ['oriKey'[,'defaultVal'[,'customKey']]]
 * @return array
 */
function getArrayCustomKey($oriArr, $fieldArr)
{
    $resArr = [];
    foreach ($fieldArr as $item) {
        if (is_array($item)) {

            $oriKey     = Arr::get($item, 0);
            $defaultVal = Arr::get($item, 1);
            $customKey  = Arr::get($item, 2, $oriKey);

            $resArr[$customKey] = Arr::get($oriArr, $oriKey, $defaultVal);
        } else {
            $resArr[$item] = Arr::get($oriArr, $item);
        }
    }

    return $resArr;
}


/**
 * 成功的json响应
 * @param  array  $data
 * @param  string  $message
 * @param  int  $code
 * @return JsonResponse
 */

if (!function_exists('ajaxReturn')) {
    function ajaxReturn($code, $data = null, $msg = null)
    {
        $data            = App\Services\Tools\Common\ErrorSvc::getError($code, $data, $msg);
        $data['success'] = isset($data['errno']) && ($data['errno'] == 0) ? true : false;
        return response()->json($data);
    }
}
/**
 * 成功的json响应
 * @param  array  $data
 * @param  string  $message
 * @param  int  $code
 * @return JsonResponse
 */
function successJsonResponse($data = [], $message = "OK", $code = 0)
{
    $resInfo = [
        'errno'  => $code,
        'errmsg' => $message
    ];

    if (!empty($data)) {
        $resInfo['data'] = $data;
    }

    return response()->json($resInfo);
}

/**
 * 失败的json响应
 *
 * @param $code
 * @param $message
 * @return JsonResponse
 */
function failJsonResponse($code, $message = '')
{
    $resInfo = [
        'errno'  => $code,
        'errmsg' => $message
    ];

    return response()->json($resInfo);
}

function myJsonResCache($data = [], $maxAge = 10)
{
    $resInfo = [
        "errno"  => 0,
        "errmsg" => "OK",
    ];

    if (!empty($data)) {
        $resInfo['data'] = $data;
    }

    $cacheOptions = [
        'private' => true,
        'max_age' => $maxAge,
    ];

    return response()->json($resInfo)->setCache($cacheOptions);
}


/**
 * 头像待优化
 * @param  null  $url
 * @return string|string[]
 */
function formatAvatarUrl($url = null)
{
    if (empty($url) || trim($url) == '/0') {
        return 'https://img1.jihai8.com/default1.png';
    } else {
        if (strpos($url, "http:") !== false) {
            $url = str_replace("http:", 'https:', $url);
        }
        if (strpos($url, "jhios.oss-cn-beijing.aliyuncs.com") !== false) {
            $url = str_replace("jhios.oss-cn-beijing.aliyuncs.com", 'img3.jihai8.com', $url);
            if (strpos($url, "x-oss-process=") === false) {
                if (strpos($url, "?") === false) {
                    $url = $url.'?x-oss-process=style/s&';
                } else {
                    $url = $url.'&x-oss-process=style/s&';
                }
            }
        }
        return $url;
    }
}

/**
 * 是否IOS设备
 * @return int
 */
function isIosDevice()
{
    return (false !== strpos(\Request::get('src'), 'AppStore')) ? 1 : 0;
}

/**
 * 数组转字符串
 * @param $arr
 * @return string
 */
function getKeyByArr($arr)
{
    return implode('_', $arr);
}

/**
 * 原生跳转信息
 * @param  string  $code
 * @param  array  $params
 * @param  string  $scroll
 * @return array
 */
function appJumpInfo($code, $params = [], $scroll = '')
{
    if (!empty($params)) {
        $params = http_build_query($params);
    } else {
        $params = '';
    }

    if (!empty($scroll)) {
        $params .= '#'.$scroll;
    }

    return [
        'code'     => $code,
        'url'      => $code,
        'params'   => $params,
        'jumptype' => 1,
    ];
}

/**
 * H5跳转信息
 * @param  string  $path  路径
 * @param  array  $params  参数
 * @param  string  $scroll  锚点
 * @param  null  $host  路由
 * @return array
 */
function h5JumpInfo($path, $params = [], $scroll = '', $host = null)
{
    if (empty($host)) {
        $url = getNewMobileUrl($path);
    } else {
        $url = $host.$path;
    }

    if (!empty($params)) {
        $params = '?'.http_build_query($params);
    } else {
        $params = '';
    }

    if (!empty($scroll)) {
        $params .= '#'.$scroll;
    }

    return [
        'code'     => 0,
        'url'      => $url.$params,
        'params'   => '',
        'jumptype' => 2,
    ];
}


function formatHiddenName($name, $leftLen = 2)
{
    if (empty($name)) {
        return '';
    }
    if (strlen($name) > $leftLen * 2) {
        return mb_substr($name, 0, $leftLen, "utf8")."**";
    } else {
        return $name."**";
    }
}
