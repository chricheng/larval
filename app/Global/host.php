<?php

/**
 * 旧版本H5地址
 * @param $path
 * @param  array  $params
 * @return string
 */
function getOldMobileUrl($path, $params = [])
{
    return env('OLD_MOBILE_DOMAIN').$path.'?'.http_build_query($params);
}

/**
 * 新版本H5地址
 * @param $path
 * @param  array  $params
 * @return string
 */
function getNewMobileUrl($path, $params = [])
{
    if (empty($params)) {
        return  env('NEW_MOBILE_DOMAIN').$path;
    }
    return env('NEW_MOBILE_DOMAIN').$path.'?'.http_build_query($params);
}

/**
 * img图片链接
 * @param  string  $path  路径
 * @param  array  $params  参数
 * @return string
 */
function getImgOssUrl($path, $params = [])
{
    return env('OSS_IMG_DOMAIN').$path.'?'.http_build_query($params);
}
