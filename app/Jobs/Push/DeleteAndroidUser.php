<?php

namespace App\Jobs\push;

use App\Services\Push\AndDeviceUserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteAndroidUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $uidArr;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($uidArr)
    {
        //
        $this->uidArr = $uidArr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       app(AndDeviceUserService::class)
            ->setUidArr($this->uidArr)
            ->main();

    }
}
