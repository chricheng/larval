<?php

namespace App\Jobs\Cms;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertKindTagInitService;
use App\Services\News\Expert\ExpertLeagueTagInitService;
use App\Services\News\Expert\ExpertTagInitService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExpertTagJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    /**
     * @var NewsModel
     */
    public $model;


    /**
     * ExpertTagJob constructor.
     * @param $model
     */
    public function __construct($model)
    {
        //
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        \Log::info('news_predict_id_'.$this->model->id);

        //全部标签
        app(ExpertTagInitService::class)
            ->setSportId(0)
            ->setUid($this->model->authorid)
            ->main();

        //足球标签
        app(ExpertTagInitService::class)
            ->setSportId($this->model->sport_id)
            ->setUid($this->model->authorid)
            ->main();

        //联赛标签
        app(ExpertLeagueTagInitService::class)
            ->setSportId($this->model->sport_id)
            ->setUid($this->model->authorid)
            ->setLeagueId($this->model->league_id)
            ->main();

        //玩法标签

        if (in_array($this->model->betKind, self::JC_BET_KIND)) {
            app(ExpertKindTagInitService::class)
                ->setSportId($this->model->sport_id)
                ->setUid($this->model->authorid)
                ->setKindArr([$this->model->betKind])
                ->setIsJc(1)
                ->main();
        } else {
            app(ExpertKindTagInitService::class)
                ->setSportId($this->model->sport_id)
                ->setUid($this->model->authorid)
                ->setKindArr([$this->model->betKind])
                ->setIsJc(0)
                ->main();
        }
    }
}
