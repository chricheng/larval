<?php

namespace App\Exceptions;

class UnauthorizedException extends ApiException
{
    //
    protected $code    = -1;
    protected $message = '未登录';
}

