<?php

namespace App\Exceptions;


class ForbiddenException extends ApiException
{
    //
    protected $code    = 403;
    protected $message = '禁止访问';
}
