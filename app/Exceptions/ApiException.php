<?php

namespace App\Exceptions;

use Exception;

class ApiException extends Exception
{
    //
    protected $code    = 500;
    protected $message = '系统异常';
}
