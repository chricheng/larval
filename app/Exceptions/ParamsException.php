<?php

namespace App\Exceptions;


class ParamsException extends ApiException
{
    //
    protected $code    = 422;
    protected $message = '参数错误';
}
