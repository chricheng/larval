<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        //专家信息同步
        $schedule->command('cms-expert:info-sync')
            ->everyMinute()
            ->withoutOverlapping()
            ->runInBackground();


        //把赛事信息刷进news表
        $schedule->command('cms-news:sync-news-match-info')
            ->everyMinute()
            ->withoutOverlapping()
            ->runInBackground();

        //更新方案预测状态
        $schedule->command('cms-news:sync-news-predict-status')
            ->everyMinute()
            ->withoutOverlapping()
            ->runInBackground();

        //更新方案销售数
        $schedule->command('cms-news:sync-news-sale-info')
            ->everyFiveMinutes()
            ->withoutOverlapping()
            ->runInBackground();

        //统计近期订单销量
        $schedule->command('cms-expert:recent-order')
            ->dailyAt('4:01')
            ->withoutOverlapping()
            ->runInBackground();

        //专家近期盈利天数
        $schedule->command('cms-expert:recent-profit')
            ->dailyAt('4:11')
            ->withoutOverlapping()
            ->runInBackground();

        //================================================

        //新发得帖子 需要完成 增量
        $schedule->command('bbs:new-discussion')
            ->everyMinute()
            ->withoutOverlapping()
            ->runInBackground();

        //帖子点击量每十分钟完成增量
        $schedule->command('bbs:increase-clicks')
            ->everyTenMinutes()
            ->withoutOverlapping()
            ->runInBackground();

        // 同步CMS库news信息到Passport库pay-news 供后台付费阅读列表筛选
        $schedule->command('cms-news:sync-pay-news-info')
            ->everyTenMinutes()
            ->withoutOverlapping()
            ->runInBackground();

        // 删除一个月不活跃的用户推送token
        $schedule->command('push:unactive-android-user')
            ->monthlyOn(15, '01:00')
            ->withoutOverlapping()
            ->runInBackground();

        // 用户订单购买行为记录脚本
        $schedule->command('user:sync-order-record')
            ->everyMinute()
            ->withoutOverlapping()
            ->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
