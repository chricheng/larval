<?php

namespace App\Console\Commands\push;

use App\Jobs\push\DeleteAndroidUser;
use App\Services\Passport\User\UserUnActiveService;
use Illuminate\Console\Command;

class PushUnActiveAndroidUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:unactive-android-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '删除一个月不活跃的用户推送token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        app(UserUnActiveService::class)->main();
        return 0;
    }
}
