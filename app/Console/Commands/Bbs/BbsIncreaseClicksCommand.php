<?php


namespace App\Console\Commands\Bbs;

use App\Models\Bbs\DiscussionsClicksModel;
use App\Models\Bbs\DiscussionsModel;
use Illuminate\Console\Command;

class BbsIncreaseClicksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbs:increase-clicks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每十分钟完成增量';

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {
        $syncWork  = false;
        $waitPosts =  $this->findSyncPosts();

        if(!empty( $waitPosts)) {
            $syncWork = $this->handleDiscussNum($waitPosts);
        }

        return  $syncWork;

    }

    /**
     * 获取需要同步得数据结果集
     * @return array
     */
    private function findSyncPosts()
    {
        return DiscussionsClicksModel::query()
            ->where('expect_num', '>', 0)
            ->where('plan_num', '<', 18)
            ->whereBetween('first_time',[
                now()->startOfDay(),
                now()->endOfDay()
             ])
            ->get()
            ->toArray();
    }


    private function handleDiscussNum($discussionsClicksList)
    {
        $this->output->progressStart(count($discussionsClicksList));

        $num = 0;
        foreach ($discussionsClicksList as $row) {
            $posts  =   DiscussionsClicksModel::query()->find($row['discussions_id']);
            if(empty($posts)){
                continue;
            }

            $data = [
                'actual_num' =>    $posts->actual_num + $posts->increase_num,
                'plan_num' =>   $posts->plan_num + 1
            ];

            #维护更新增长表记录
            DiscussionsClicksModel::query()
                ->where('discussions_id',  $row['discussions_id'])
                ->update($data);

            #维护更新增长表记录
            DiscussionsModel::query()
                ->where('id', $row['discussions_id'])
                ->increment('clicks',$posts->increase_num);

            $num++;
        }

        $this->info("总共更新:".$num);
        $this->output->progressFinish();
        return true;
    }

}
