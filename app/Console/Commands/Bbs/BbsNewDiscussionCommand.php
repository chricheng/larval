<?php


namespace App\Console\Commands\Bbs;

use App\Models\Bbs\DiscussionsClicksModel;
use App\Models\Bbs\DiscussionsModel;
use Illuminate\Console\Command;

class BbsNewDiscussionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbs:new-discussion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步初始化得资讯id';

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {
       $syncWork  = false;
       $publishPosts =  $this->findNewPosts();

       if(!empty($publishPosts)) {
           $syncWork = $this->handleDissList($publishPosts);
       }

       return  $syncWork;

    }

    /**
     * 获取最新队列发布得帖子信息
     * @return array
     */
    private function findNewPosts()
    {
        return DiscussionsModel::query()
            ->where('c_time', '>', now()->subMinutes(5*60))
            ->where('status', '=', 1)
            ->where('is_approved', '=', 1)
            ->where('is_private', '=', 1)
            ->get()
            ->toArray();
    }


    private function handleDissList($discussionsList)
    {
        $this->output->progressStart(count($discussionsList));
        $num = 0;
        foreach ($discussionsList as $row) {
            $startClickVal =  $this->getMtRand();
            if(!empty(DiscussionsClicksModel::query()->find($row['id']))){
                continue;
            }

            $data = [
                'discussions_id' => $row['id'],
                'expect_num'  =>     $startClickVal['expect_num'],
                'increase_num' =>   $startClickVal['increase_num'],
                'actual_num' =>   $startClickVal['increase_num'],
                'plan_num' =>   1
            ];

            #维护更新增长表记录
            DiscussionsClicksModel::query()
                ->create($data);

            #维护更新增长表记录
            DiscussionsModel::query()
                ->where('id', $row['id'])
                ->increment('clicks',$startClickVal['increase_num']);

            $num++;
        }
        $this->info("总共更新:".$num);
        $this->output->progressFinish();
        return true;
    }

    /**
     * 帖子&快讯阅读数规则优化
     * @return array
     */
    public function  getMtRand(){
        $expect_num  =   mt_rand(500, 1000);
        $increase_num  =   intval(ceil($expect_num / 18) );

        return [
            'expect_num'  => $expect_num,
            'increase_num' => $increase_num
        ];
    }

}
