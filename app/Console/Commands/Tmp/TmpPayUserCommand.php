<?php


namespace App\Console\Commands\Tmp;

use App\Models\Passport\Pay\PayDataModel;
use App\Models\Passport\Pay\PayGroupModel;
use App\Models\Passport\Pay\PayNewsModel;
use App\Models\User;
use Illuminate\Console\Command;

class TmpPayUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmp:pay-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '即嗨APP付费用户数据筛选';

    const FOOTBALL_TYPE   = [2, 9];
    const BASKETBALL_TYPE = [11, 12];

    private $firstMonthArr  = [];
    private $secondMonthArr = [];
    private $thirdMonthArr  = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->initParams();

        $uidArr = $this->getUidArr();

        $this->output->progressStart(count($uidArr));

        foreach ($uidArr as $uid) {
            $this->output->progressAdvance();

            $this->handleEachUid($uid);
        }


        $this->output->progressFinish();

        return 0;
    }

    private function initParams()
    {
        $this->firstMonthArr = [
            now()->subMonths(2)->startOfMonth()->toDateTimeString(),
            now()->subMonths(2)->endOfMonth()->toDateTimeString(),
        ];


        $this->secondMonthArr = [
            now()->subMonths(1)->startOfMonth()->toDateTimeString(),
            now()->subMonths(1)->endOfMonth()->toDateTimeString(),
        ];

        $this->thirdMonthArr = [
            now()->subMonths(0)->startOfMonth()->toDateTimeString(),
            now()->subMonths(0)->endOfMonth()->toDateTimeString(),
        ];

    }

    private function handleEachUid($uid)
    {
        $arr = [];

        //
        $arr[] = $this->getNickName($uid);
        $arr[] = $uid;

        //付费阅读
        $arr[] = $this->getPayNewsAmountByUid($uid, 1, $this->firstMonthArr);
        $arr[] = $this->getPayNewsAmountByUid($uid, 2, $this->firstMonthArr);
        $arr[] = $this->getPayNewsAmountByUid($uid, 1, $this->secondMonthArr);
        $arr[] = $this->getPayNewsAmountByUid($uid, 2, $this->secondMonthArr);
        $arr[] = $this->getPayNewsAmountByUid($uid, 1, $this->thirdMonthArr);
        $arr[] = $this->getPayNewsAmountByUid($uid, 2, $this->thirdMonthArr);

        //最后购买时间
        $arr[] = $this->getLastPayTime($uid);

        //直播间
        $arr[] = $this->getPayGroupByUid($uid, $this->firstMonthArr);

        $arr[] = $this->getPayGroupByUid($uid, $this->secondMonthArr);

        $arr[] = $this->getPayGroupByUid($uid, $this->thirdMonthArr);

        //大数据
        $arr[] = $this->getPayDataByUid($uid, $this->firstMonthArr);

        $arr[] = $this->getPayDataByUid($uid, $this->secondMonthArr);

        $arr[] = $this->getPayDataByUid($uid, $this->thirdMonthArr);

        $res = implode(',', $arr);

        $path = public_path('storage');

        if (!is_dir($path)) {
            mkdir($path, 0777);
        }

        \File::append(public_path('storage/1.csv'), $res.PHP_EOL);
    }

    private function getPayDataByUid($uid, $payTimeArr)
    {
        $builder = PayDataModel::query()
            ->where('uid', $uid)
            ->where('status', 3);

        return $builder->whereBetween('pay_time', $payTimeArr)
            ->sum('money');
    }


    private function getNickName($uid)
    {
        $model = User::query()
            ->where('uid', $uid)
            ->first();

        if (empty($model)) {
            return '';
        }

        return $model->nickname == $model->uid ? $model->tnickname : $model->nickname;
    }

    private function getPayGroupByUid($uid, $payTimeArr)
    {
        $builder = PayGroupModel::query()
            ->where('uid', $uid)
            ->where('status', 4);

        return $builder->whereBetween('pay_time', $payTimeArr)
            ->sum('money');
    }

    private function getPayNewsAmountByUid($uid, $sportId, $payTimeArr)
    {
        $builder = PayNewsModel::query()
            ->where('uid', $uid)
            ->where('status', 4);

        if ($sportId == 1) {
            $builder->whereIn('type', self::FOOTBALL_TYPE);
        } else {
            $builder->whereIn('type', self::BASKETBALL_TYPE);
        }

        return $builder->whereBetween('pay_time', $payTimeArr)
            ->sum('money');
    }


    private function getPayNewsAmount($uidArr, $payTimeArr)
    {
        return PayNewsModel::query()
            ->whereIn('uid', $uidArr)
            ->whereIn('status', [3, 4, 5])
            ->whereBetween('pay_time', $payTimeArr)
            ->selectRaw('uid,sum(money) total')
            ->groupBy('uid')
            ->pluck('total', 'uid')
            ->toArray();
    }

    private function getLastPayTime($uid)
    {
        $model = PayNewsModel::query()
            ->where('uid', $uid)
            ->whereIn('status', [3, 4, 5])
            ->orderByDesc('pay_time')
            ->first();

        if (empty($model)) {
            return '';
        } else {
            return $model->pay_time;
        }
    }

    private function getUidArr()
    {
        //3 支付成功  4已结算  5 已退款
        return PayNewsModel::query()
            ->where('status', 4)
            ->whereBetween('pay_time', [
                '2021-12-01 00:00:00',
                '2022-01-31 23:59:59',
            ])->distinct('uid')
            ->pluck('uid')
            ->toArray();
    }
}
