<?php


namespace App\Console\Commands\Tmp;;

use App\Models\Passport\Pay\PayDataModel;
use Illuminate\Console\Command;
use App\Models\User;

class TmpModelDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmp:model-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '即嗨APP数据模型历史购买用户付费数据';


    const DATA_TYPE = [
            18  => '冷门分析',
            19  => '情报速递', 
            21  => '泊松分布', 
            23  => 'margin模型'
    ]; //冷门分析 情报速递，泊松分布，margin 模型

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {

       foreach (self::DATA_TYPE as $key => $name) {

            $outData =  $this->summaryDataType($key);
            $this->output->progressStart(count($outData));
            $title = ['用户名称','ID','购买次数','起始时间','结束时间','数据类型'];
            $title = implode(',', $title);

            \File::append(public_path($key."_".$name.'.csv'), $title.PHP_EOL);
            
            foreach ($outData as  $model) {
                 $outCsvData = $this->handleEachModelData($model);
                 $outCsvData = implode(',', $outCsvData);
                 \File::append(public_path($key."_".$name.'.csv'), $outCsvData.PHP_EOL);
            }

            $this->output->progressAdvance();
       }
       $this->output->progressFinish();
       return  true;

    }

    private function handleEachModelData($model)
    {
    
        $arr[] = $this->getNickName($model['uid']);
        $arr[] = $model['uid'];
        $arr[] = $model['total'];
        $arr[] = $model['start_time'];
        $arr[] = $model['end_time'];
        $arr[] = self::DATA_TYPE[$model['data_type']];

        return  $arr;
    }

    /**
     * 情报速递、泊松分布、冷门分析和margin
     * @return array
     */
    private function summaryDataType($dataType)
    {
        if( empty($dataType) ){
            return [];
        }
        return PayDataModel::query()
            ->where('data_type', '=', $dataType)
            ->where('status', '=', 3)
            ->selectRaw('count(1) as total,uid,min(start_time) as start_time, max(end_time) as end_time,data_type')
            ->groupBy('uid')
            ->orderByDesc('end_time')
            ->get()
            ->toArray();
    }

    /**
     * 处理用户昵称
     * @param [type] $uid
     * @return string
     */
    private function getNickName($uid)
    {
        $model = User::query()
            ->where('uid', $uid)
            ->first();

        if (empty($model)) {
            return '';
        }

        return $model->nickname == $model->uid ? $model->tnickname : $model->nickname;
    }
}
