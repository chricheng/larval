<?php

namespace App\Console\Commands\User;

use App\Models\Passport\Pay\PayNewsModel;
use App\Services\Passport\User\UserOrderStaticService;
use Illuminate\Console\Command;

class UserSyncOrdersRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:sync-order-record {isFirst=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '用户订单购买行为记录脚本';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");
        \Log::info('user:sync-order');

        $isFirst = $this->argument('isFirst');

        $this->getUserPayNewModel($isFirst); //第一次初始化


    }

    #运营 只需要统计购买资讯的 记录
    #init 处理的 所有历史记录
    private function getUserPayNewModel($isFirst = 0)
    {
        $builder = PayNewsModel::query()
            ->with(['user'])
            ->from('pay_news as pay_news')
            ->leftJoin('user', 'pay_news.uid', '=', 'user.uid')
            ->whereIn('pay_news.status', [3, 4, 5])
            ->whereNotNull('user.regtime');

        if (!$isFirst) {
            $builder->where(function ($builder) {
                $builder->where('news_order.update_time', '>', now()->subMinutes(5));
            });
        }

        return $builder->selectRaw('pay_news.uids,user.regtime,min(pay_time) as first_pay_time, max(pay_time) as last_pay_time')
            ->groupBy('pay_news.uid')
            ->orderByDesc('pay_news.uid')
            ->chunk(1000, function ($itemsTmp) {
                $payRecordUidArr = $itemsTmp->toArray();
                $this->output->progressStart(count($payRecordUidArr));
                foreach ($payRecordUidArr as $k => $model) {
                    app(UserOrderStaticService::class)
                        ->setRecodeArr($model)
                        ->main();
                }
                $this->output->progressFinish();
            });
    }
}

