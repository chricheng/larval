<?php

namespace App\Console\Commands\Odds;

use App\Models\QtData\OddsRunningHalfModel;
use App\Models\QtData\OddsRunningModel;
use Illuminate\Console\Command;

class DeleteOddsRunCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'odds:delete-odds-run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '删除全场、半场滚球数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->handleData();

        return 0;
    }

    private function handleData()
    {
        OddsRunningModel::query()
            ->where('odds_time', '<', now()->subDays(1))
            ->delete();

        OddsRunningHalfModel::query()
            ->where('odds_time', '<', now()->subDays(1))
            ->delete();
    }
}
