<?php

namespace App\Console\Commands\Cms\News;

use App\Models\Cms\NewsModel;
use App\Models\QtBasketball\BasketballModel;
use App\Models\QtData\FootballModel;
use Illuminate\Console\Command;

class SyncNewsMatchInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-news:sync-news-match-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '把赛事信息刷进news表';

    const FOOTBALL_TYPE   = [2, 9];
    const BASKETBALL_TYPE = [11, 12];

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info('sync-news-match-info');

        ini_set("memory_limit", "1G");

        $this->handleFootballNews();
        $this->handleBasketballNews();

        return 1;
    }

    private function handleFootballNews()
    {
        $newsModels = NewsModel::query()
            ->where('firsttime', '>', now()->subMinutes(30))
            ->where('status', 1)
            ->whereIn('type', self::FOOTBALL_TYPE)
            ->where('league_id', 0)
            ->get();

        $this->output->progressStart(count($newsModels));

        foreach ($newsModels as $model) {

            $this->output->progressAdvance();

            $footballModel = FootballModel::query()
                ->where('schedule_id', $model->matchid)
                ->first();

            $model->update([
                'sport_id'  => 1,
                'league_id' => data_get($footballModel, 'sclass_id', 0)
            ]);
        }

        $this->output->progressFinish();

    }

    private function handleBasketballNews()
    {
        $newsModels = NewsModel::query()
            ->where('firsttime', '>', now()->subMinutes(30))
            ->where('status', 1)
            ->whereIn('type', self::BASKETBALL_TYPE)
            ->where('league_id', 0)
            ->get();


        $this->output->progressStart(count($newsModels));

        foreach ($newsModels as $model) {

            $this->output->progressAdvance();

            $basketballModel = BasketballModel::query()
                ->where('event_id', $model->matchid)
                ->first();

            $model->update([
                'sport_id'  => 2,
                'league_id' => data_get($basketballModel, 'league_id', 0)
            ]);
        }

        $this->output->progressFinish();

    }

}
