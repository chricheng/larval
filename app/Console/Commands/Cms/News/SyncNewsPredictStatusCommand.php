<?php

namespace App\Console\Commands\Cms\News;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertKindTagInitService;
use App\Services\News\Expert\ExpertLeagueTagInitService;
use App\Services\News\Expert\ExpertTagInitService;
use Illuminate\Console\Command;

class SyncNewsPredictStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-news:sync-news-predict-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新方案预测状态';


    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $newsModels = $this->getNewsModels();

        $this->output->progressStart(count($newsModels));

        foreach ($newsModels as $model) {

            $this->output->progressAdvance();

            $model->update([
                'predict_status' => 1,
                'predict_time'   => now(),
            ]);

            try {
                $this->handleEachModel($model);

            } catch (\Exception $exception) {
                \Log::error('sync_news_predict_status', [
                    'id'  => $model->id,
                    'msg' => $exception->getMessage()
                ]);
            }


        }

        $this->output->progressFinish();

        //===============================================

//        $newsModels = $this->getPredictedModels();
//
//        $this->output->progressStart(count($newsModels));
//
//        foreach ($newsModels as $model) {
//
//            $this->output->progressAdvance();
//
//            try {
//
//                $this->handleEachModel($model);
//
//            } catch (\Exception $exception) {
//                \Log::error('sync_news_predict_status', [
//                    'id'  => $model->id,
//                    'msg' => $exception->getMessage()
//                ]);
//            }
//        }
//
//        $this->output->progressFinish();


        return 1;
    }

    private function handleEachModel(NewsModel $model)
    {
        \Log::info('news_predict_id_'.$model->id, $this->getRecentArr($model->authorid));

        //全部标签
        app(ExpertTagInitService::class)
            ->setSportId(0)
            ->setUid($model->authorid)
            ->main();

        //足球标签
        app(ExpertTagInitService::class)
            ->setSportId($model->sport_id)
            ->setUid($model->authorid)
            ->main();

        //联赛标签
        app(ExpertLeagueTagInitService::class)
            ->setSportId($model->sport_id)
            ->setUid($model->authorid)
            ->setLeagueId($model->league_id)
            ->main();

        //玩法标签

        if (in_array($model->betKind, self::JC_BET_KIND)) {
            app(ExpertKindTagInitService::class)
                ->setSportId($model->sport_id)
                ->setUid($model->authorid)
                ->setKindArr([$model->betKind])
                ->setIsJc(1)
                ->main();
        } else {
            app(ExpertKindTagInitService::class)
                ->setSportId($model->sport_id)
                ->setUid($model->authorid)
                ->setKindArr([$model->betKind])
                ->setIsJc(0)
                ->main();
        }
    }

    private function getPredictedModels()
    {
        $builder = NewsModel::query()
            ->whereIn('sport_id', [1, 2])
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1)
            ->whereBetween('predict_time', [
                now()->subMinutes(10),
                now()->subMinutes(5),
            ]);

        return $builder->select([
            'id',
            'league_id',
            'sport_id',
            'betKind',
            'predict_status',
            'authorid',
        ])->orderBy('id')
            ->limit(2000)
            ->get();
    }

    /**
     * 获取满足条件的用户UID
     * @return NewsModel[]
     */
    private function getNewsModels()
    {
        $builder = NewsModel::query()
            ->whereIn('sport_id', [1, 2])
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 0);

        return $builder->select([
            'id',
            'league_id',
            'sport_id',
            'betKind',
            'predict_status',
            'authorid',
        ])->orderBy('id')
            ->limit(2000)
            ->get();
    }

    private function getRecentArr($uid)
    {
        return NewsModel::query()
            ->where('authorid', $uid)
            ->whereIn('isWin', [1, 2, 3])
            ->whereIn('sport_id', [1, 2])
            ->where('status', 1)
            ->where('predict_status', 1)
            ->pluck('isWin', 'id')
            ->toArray();
    }
}
