<?php

namespace App\Console\Commands\Cms\News;

use App\Models\Cms\NewsModel;
use App\Models\Passport\Pay\PayNewsModel;
use Illuminate\Console\Command;

class SyncPayNewsInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-news:sync-pay-news-info {initDate=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步CMS-news信息到Passport库的pay-news表中';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        # 更新数据量
        $count = 0;
        # $initDate => 20220223
        $initDate = $this->argument("initDate");
        if(!empty($initDate)){
            $MinNewId = $initDate . "00000000000";
            $info = PayNewsModel::select("news_id")
                ->where("news_id",">=",$MinNewId)
                ->where("isWin",0)
                ->distinct()
                ->get()
                ->toArray();
            foreach ($info as $row){
                $newsId = $row["news_id"];
                $newsInfo = NewsModel::select("isWin","jc","betKind")
                    ->where("id",$newsId)
                    ->first();
                if(!empty($newsInfo)){
                    $params = $newsInfo->toArray();
                    $num = PayNewsModel::where("news_id",$newsId)
                        ->update($params);
                    $count += $num;
                }
            }
        }else{
            $updateTime = now()->addMinute(-11)->toDateTimeString();
            $mid = date('Ymd',strtotime("-48 hours")) . "00000000000";
            $info = NewsModel::select("id","isWin","jc","betKind")
                ->where("id",">",$mid)
                ->where("update_time",">",$updateTime)
                ->whereIn("isWin",[1,2,3])
                ->get();
            if(empty($info)){
                return $count;
            }
            foreach ($info as $item){
                /**@var $item NewsModel**/
                $params = [
                    "isWin"     => $item->isWin,
                    "jc"        => $item->jc,
                    "betKind"   => $item->betKind,
                ];
                $num = PayNewsModel::where("news_id",$item->id)
                    ->update($params);
                $count += $num;
            }
        }
        return $count;
    }
}
