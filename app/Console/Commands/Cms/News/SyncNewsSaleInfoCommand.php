<?php

namespace App\Console\Commands\Cms\News;

use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Cms\NewsModel;
use Illuminate\Console\Command;

class SyncNewsSaleInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-news:sync-news-sale-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新方案销售数';

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info('sync-news-sale-info');

        $this->handleEachSportId(0);
        $this->handleEachSportId(1);
        $this->handleEachSportId(2);


//        $uidArr = array_unique(array_merge($this->getBeginMatchUidArr(), $this->getNewArticleUidArr()));
//
//        $this->info('总共用户UID:' . count($uidArr) . '个');
//
//        $this->handleExpertTag($uidArr, 0);
//        $this->handleExpertTag($uidArr, 1);
//        $this->handleExpertTag($uidArr, 2);

        return 1;
    }

    private function handleEachSportId($sportId = 0)
    {
        $saleArr = $this->getSaleArr($sportId);

        foreach ($saleArr as $uid => $num) {

            $tagModel = ExpertTagModel::query()
                ->where('uid', $uid)
                ->where('sport_id', $sportId)
                ->first();

            if (empty($tagModel)) {
                continue;
            }

            $tagModel->update([
                'can_buy'   => 1,
                'buy_count' => $num
            ]);

        }

        ExpertTagModel::query()
            ->whereNotIn('uid', array_keys($saleArr))
            ->where('sport_id', $sportId)
            ->update([
                'can_buy'   => 0,
                'buy_count' => 0,
            ]);
    }

    private function getSaleArr($sportId = 0)
    {
        $builder = NewsModel::query()
            ->where('firsttime', '>', now()->subDays(5));


        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        return $builder
            ->where('for_sale', 1)
            ->selectRaw('authorid,count(1) num')
            ->groupBy('authorid')
            ->pluck('num', 'authorid')
            ->toArray();
    }

    private function handleExpertTag($uidArr, $sportId = 0)
    {
        $builder = NewsModel::query()
            ->whereIn('authorid', $uidArr);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        $arr = $builder
            ->where('isWin', 0)
            ->where('status', 1)
            ->where('for_sale', 1)
            ->orderByDesc('id')
            ->selectRaw('authorid,count(1) num,firsttime')
            ->groupBy('authorid')
            ->get()
            ->toArray();


        $tmpUid  = array_column($arr, 'authorid');
        $diffUid = array_diff($uidArr, $tmpUid);

        foreach ($arr as $item) {

            $model = ExpertTagModel::query()
                ->where('uid', $item['authorid'])
                ->where('sport_id', $sportId)
                ->first();

            //无统计值的不处理
            if (empty($model)) {
                continue;
            }

            $model->update([
                'buy_count'         => $item['num'],
                'can_buy'           => 1,
                'last_article_time' => $item['firsttime'],
            ]);
        }

        ExpertTagModel::query()
            ->whereIn('uid', $diffUid)
            ->where('sport_id', $sportId)
            ->update([
                'can_buy'   => 0,
                'buy_count' => 0,
            ]);
    }

    /**
     * 近期发文UID
     * @return array
     */
    private function getNewArticleUidArr()
    {
        $builder = NewsModel::query()
            ->where('firsttime', '>', now()->subDays(3))
            ->whereIn('sport_id', [1, 2])
            ->where('isWin', 0)
            ->where('status', 1)
            ->where('predict_status', 0);

        return $builder
            ->distinct('authorid')
            ->pluck('authorid')
            ->toArray();
    }

    /**
     * 近期开赛的UID
     * @return array
     */
    private function getBeginMatchUidArr()
    {
        $builder = NewsModel::query()
            ->whereBetween('match_time', [
                now()->subMinutes(5),
                now()
            ])
            ->whereIn('sport_id', [1, 2])
            ->where('isWin', 0)
            ->where('status', 1)
            ->where('predict_status', 0);

        return $builder
            ->distinct('authorid')
            ->pluck('authorid')
            ->toArray();
    }
}
