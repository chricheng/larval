<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertTagInitService;
use Illuminate\Console\Command;

class ExpertTagInitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:tag-init {isFirst=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '专家标签初始化';


    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");

        $isFirst = $this->argument('isFirst');

        $this->handleBySportId(0, $isFirst);
        $this->handleBySportId(1, $isFirst);
        $this->handleBySportId(2, $isFirst);

        return 0;
    }

    private function handleBySportId($sportId, $isFirst)
    {
        $uidArr = $this->getAllAuthorUid($sportId, $isFirst);

        $this->output->progressStart(count($uidArr));

        foreach ($uidArr as $uid) {

            $this->output->progressAdvance();

            try {
                app(ExpertTagInitService::class)
                    ->setIsFirst($isFirst)
                    ->setSportId($sportId)
                    ->setUid($uid)
                    ->main();
            } catch (\Exception $exception) {
                \Log::error('expert_tag_init_error', [
                    'uid' => $uid,
                    'msg' => $exception->getMessage()
                ]);
            }

        }

        $this->output->progressFinish();
    }

    /**
     * 获取满足条件的用户UID
     * @param $sportId
     * @param $isFirst
     * @return mixed
     */
    private function getAllAuthorUid($sportId, $isFirst)
    {
        $builder = NewsModel::query()
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        //是否首次
        if (!$isFirst) {
            //近30天连红需要
            $builder->where('firsttime', '>', now()->subDays(5));
        }

        return $builder->orderBy('authorid')
            ->distinct('authorid')
            ->pluck('authorid')
            ->toArray();
    }
}
