<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertKindTagInitService;
use Illuminate\Console\Command;

class ExpertKindTagInitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:kind-tag-init {isFirst=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '专家玩法维度标签初始化';

    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");


        $isFirst = $this->argument('isFirst');


        $this->info("start_football:".now()->toDateTimeString());

        $this->handleBySportId(1, $isFirst, 0);
        $this->handleBySportId(1, $isFirst, 1);

        $this->info("start_basketball:".now()->toDateTimeString());

        $this->handleBySportId(2, $isFirst, 0);
        $this->handleBySportId(2, $isFirst, 1);

        $this->info("end:".now()->toDateTimeString());

        return 0;
    }

    private function handleBySportId($sportId, $isFirst, $jc)
    {
        $uidArr = $this->getAllAuthorUid($sportId, $isFirst, $jc);

        $this->output->progressStart(count($uidArr));

        foreach ($uidArr as $uid => $item) {

            $betKindArr = array_column($item, 'betKind');


            $this->output->progressAdvance();

            try {
                app(ExpertKindTagInitService::class)
                    ->setIsFirst($isFirst)
                    ->setSportId($sportId)
                    ->setUid($uid)
                    ->setKindArr($betKindArr)
                    ->setIsJc($jc)
                    ->main();

            } catch (\Exception $exception) {
                \Log::error('expert_tag_init_error', [
                    'uid'  => $uid,
                    'kind' => $betKindArr,
                    'jc'   => $jc,
                    'msg'  => $exception->getMessage()
                ]);
            }

        }

        $this->output->progressFinish();
    }

    /**
     * 获取满足条件的用户UID
     * @param $sportId
     * @param $isFirst
     * @param $jc
     * @return array
     */
    protected function getAllAuthorUid($sportId, $isFirst, $jc)
    {
        $builder = NewsModel::query()
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        //JC判断
        if ($jc) {
            $builder->whereIn('betKind', self::JC_BET_KIND);
        } else {
            $builder->whereNotIn('betKind', self::JC_BET_KIND);
        }


        //是否首次
        if (!$isFirst) {
            //近30天连红需要
            $builder->where('firsttime', '>', now()->subDays(5));
        }


        return $builder->orderBy('authorid')
            ->groupBy(['authorid', 'betKind'])
            ->select(['authorid', 'betKind'])
            ->get()
            ->groupBy('authorid')
            ->toArray();
    }
}
