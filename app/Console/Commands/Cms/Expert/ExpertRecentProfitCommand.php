<?php


namespace App\Console\Commands\Cms\Expert;


use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Cms\NewsModel;
use Illuminate\Console\Command;

class ExpertRecentProfitCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:recent-profit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '专家近期盈利天数';


    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //近3天
        $this->handleData(0, 3, 'profit_num_3_days');
        $this->handleData(1, 3, 'profit_num_3_days');
        $this->handleData(2, 3, 'profit_num_3_days');

        //近7天
        $this->handleData(0, 7, 'profit_num_7_days');
        $this->handleData(1, 7, 'profit_num_7_days');
        $this->handleData(2, 7, 'profit_num_7_days');

        //近20天
        $this->handleData(0, 20, 'profit_num_20_days');
        $this->handleData(1, 20, 'profit_num_20_days');
        $this->handleData(2, 20, 'profit_num_20_days');

        return 1;
    }

    private function handleData($sportId = 0, $days = 3, $field = 'profit_num_3_days')
    {
        $builder = NewsModel::query()
            ->whereBetween('firsttime', [
                now()->subDays($days)->startOfDay(),
                now()->subDays(1)->endOfDay(),
            ])->where('status', 1)
            ->where('predict_status', 1);

        if (!empty($sportId)) {
            $builder->where('sport_id', $sportId);
        }

        $arr = $builder->selectRaw('authorid uid,substring(firsttime, 1, 10) date,sum(winFee-betFee) fee')
            ->groupByRaw('authorid,substring(firsttime, 1, 10)')
            ->having('fee', '>', 0)
            ->get()
            ->groupBy('uid')
            ->toArray();

        foreach ($arr as $uid => $item) {

            ExpertTagModel::query()
                ->updateOrCreate([
                    'uid'      => $uid,
                    'sport_id' => $sportId
                ], [
                    $field => count($item)
                ]);
        }

        ExpertTagModel::query()
            ->whereNotIn('uid', array_keys($arr))
            ->where('sport_id', $sportId)
            ->update([
                $field => 0
            ]);
    }
}

