<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\Expert\ExpertModel;
use App\Models\Passport\Expert\AuthorListModel;
use App\Models\Passport\User\UserFriendModel;
use Illuminate\Console\Command;

class ExpertInfoSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:info-sync {isFirst=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '专家信息同步';

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");

        \Log::info('cms-expert:info-sync');

        //同步专家信息
        $isFirst = $this->argument('isFirst');

        $authorListModels = $this->getAuthorModel($isFirst);
        $this->handleAuthorList($authorListModels);

        //近期有关注的专家信息
        $authorListModels = $this->getFollowAuthor();
        $this->handleAuthorList($authorListModels);

        return 1;
    }

    private function handleAuthorList($authorListModels)
    {
        $uidArr = $authorListModels->pluck('uid')->toArray();

        $followerArr = $this->getFollowerArr($uidArr);
        $followedArr = $this->getFollowedArr($uidArr);

        $this->output->progressStart(count($authorListModels));

        foreach ($authorListModels as $listModel) {

            $this->output->progressAdvance();

            $userModel = $listModel->user;

            if (empty($userModel)) {
                continue;
            }

            $data = [
                'nickname'        => $userModel->nickname,
                'avatar'          => $userModel->avatar,
                'intro'           => !empty($userModel->intro) ? $userModel->intro : '',
                'user_type'       => $userModel->usertype,
                'exp'             => $userModel->exp,
                'reg_time'        => $userModel->regtime,
                'points'          => $listModel->points,
                'buy_news_num'    => $listModel->buy_news_num,
                'final_news_time' => $listModel->final_time,
                'audit_flag'      => $listModel->audit_flag,
                'pass_flag'       => $listModel->pass_flag,
                'anchor_status'   => $listModel->anchor_status,
                'status'          => $listModel->user->status,
                'level'           => $listModel->user->level,
                'close_type'      => $listModel->close_type,
                'is_active'       => $this->getActiveInfo($listModel),
                'follower_num'    => data_get($followerArr, $userModel->uid, 0),
                'followed_num'    => data_get($followedArr, $userModel->uid, 0),
            ];

            ExpertModel::query()
                ->updateOrCreate(['uid' => $userModel->uid], $data);
        }

        $this->output->progressFinish();

    }

    /**
     * 获取关注数
     * @param $uidArr
     * @return array
     */
    private function getFollowedArr($uidArr)
    {
        if (empty($uidArr)) {
            return [];
        }
        return UserFriendModel::query()
            ->whereIn('uid', $uidArr)
            ->where('status', 1)
            ->selectRaw('uid,count(1) num')
            ->groupBy('uid')
            ->pluck('num', 'uid')
            ->toArray();
    }

    /**
     * 获取粉丝数信息
     * @param $uidArr
     * @return array
     */
    private function getFollowerArr($uidArr)
    {
        if (empty($uidArr)) {
            return [];
        }

        return UserFriendModel::query()
            ->whereIn('followuid', $uidArr)
            ->where('status', 1)
            ->selectRaw('followuid,count(1) num')
            ->groupBy('followuid')
            ->pluck('num', 'followuid')
            ->toArray();
    }

    private function getActiveInfo(AuthorListModel $listModel)
    {
        //usertype   用户类型1正常，2虚拟 3 小黑屋  最牛逼的权限
        //
        //
        //audit_flag  资料状态，不能发文，不能开播，正常显示方案  审核状态0待审核,1通过,2驳回,3取消
        //pass_flag  专家身份信息，不能发文，不显示方案 审核状态0待审核,1通过,2驳回,3取消
        //anchor_status  主播身份信息，  审核状态-1 未申请 0待审核,1通过,2驳回,3取消’
        //close_type    可以废弃，先废弃他。

        if ($listModel->user->usertype == 3) {
            return 0;
        }

        if ($listModel->close_type > 0) {
            return 0;
        }

        if ($listModel->pass_flag == 1) {
            return 1;
        }

        return 0;
    }

    private function getAuthorModel($isFirst = 0)
    {
        $builder = AuthorListModel::query()
            ->with(['user'])
            ->from('author_list as expert')
            ->leftJoin('user', 'expert.uid', '=', 'user.uid')
            ->where('expert.news_nun', '>', 0);

        if (!$isFirst) {

            $builder->where(function ($builder) {
                $builder->where('expert.update_time', '>', now()->subMinutes(5))
                    ->orWhere('user.update_time', '>', now()->subMinutes(5));
            });
        }

        return $builder->select(['expert.*'])
            ->get();
    }

    private function getFollowAuthor()
    {
        $uidArr = UserFriendModel::query()
            ->where('update_time', '>', now()->subMinutes(5))
            ->where('followuid_type', 1)
            ->orderByDesc('update_time')
            ->pluck('uid')
            ->toArray();

        $builder = AuthorListModel::query()
            ->with(['user'])
            ->from('author_list as expert')
            ->leftJoin('user', 'expert.uid', '=', 'user.uid')
            ->whereIn('expert.uid', $uidArr);

        return $builder->select(['expert.*'])
            ->get();
    }
}
