<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertLeagueTagInitService;
use Illuminate\Console\Command;

class ExpertLeagueTagInitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:league-tag-init {isFirst=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '专家联赛维度标签初始化';

    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");


        $isFirst = $this->argument('isFirst');


        $this->info("start_football:".now()->toDateTimeString());

        $this->handleBySportId(1, $isFirst);

        $this->info("start_basketball:".now()->toDateTimeString());

        $this->handleBySportId(2, $isFirst);

        $this->info("end:".now()->toDateTimeString());

        return 0;
    }

    private function handleBySportId($sportId, $isFirst)
    {
        $uidArr = $this->getAllAuthorUid($sportId, $isFirst);

        $this->output->progressStart(count($uidArr));

        foreach ($uidArr as $item) {

            $this->output->progressAdvance();

            try {
                app(ExpertLeagueTagInitService::class)
                    ->setIsFirst($isFirst)
                    ->setSportId($sportId)
                    ->setUid($item['authorid'])
                    ->setLeagueId($item['league_id'])
                    ->main();

            } catch (\Exception $exception) {
                \Log::error('expert_tag_init_error', [
                    'uid'       => $item['authorid'],
                    'league_id' => $item['league_id'],
                    'msg'       => $exception->getMessage()
                ]);
            }

        }

        $this->output->progressFinish();
    }

    /**
     * 获取满足条件的用户UID
     * @param $sportId
     * @param $isFirst
     * @return mixed
     */
    private function getAllAuthorUid($sportId, $isFirst)
    {
        $builder = NewsModel::query()
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        //是否首次
        if (!$isFirst) {
            $builder->where('firsttime', '>', now()->subDays(5));
        }

        return $builder->orderBy('authorid')
            ->groupBy(['authorid', 'league_id'])
            ->select(['authorid', 'league_id'])
            ->get()
            ->toArray();
    }
}
