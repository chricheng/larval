<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\NewsModel;
use App\Services\News\Expert\ExpertKindTagInitService;
use App\Services\News\Expert\ExpertLeagueTagInitService;
use App\Services\News\Expert\ExpertTagInitService;
use Illuminate\Console\Command;

class ExpertRepairTagCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:repair-tag {uid} {first=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '修复专家标签';

    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    protected $isFirst = 0;


    /**
     * Create a new command instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", "1G");

        $uid           = $this->argument('uid');
        $this->isFirst = $this->argument('first');

        $this->handleTag($uid);
        $this->handleLeagueTag($uid, 1);
        $this->handleLeagueTag($uid, 2);

        $this->handleKindTag($uid, 1, 1);
        $this->handleKindTag($uid, 1, 0);
        $this->handleKindTag($uid, 2, 1);
        $this->handleKindTag($uid, 2, 0);

        return 0;
    }

    private function handleKindTag($uid, $sportId, $jc)
    {
        $kindArr = $this->getKindArr($uid, $sportId, $this->isFirst, $jc);

        app(ExpertKindTagInitService::class)
            ->setIsFirst($this->isFirst)
            ->setSportId($sportId)
            ->setUid($uid)
            ->setKindArr($kindArr)
            ->setIsJc($jc)
            ->main();
    }

    /**
     * 获取满足条件的用户UID
     * @param $uid
     * @param $sportId
     * @param $isFirst
     * @param $jc
     * @return array
     */
    protected function getKindArr($uid, $sportId, $isFirst, $jc)
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid)
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        //JC判断
        if ($jc) {
            $builder->whereIn('betKind', self::JC_BET_KIND);
        } else {
            $builder->whereNotIn('betKind', self::JC_BET_KIND);
        }


        //是否首次
        if (!$isFirst) {
            //近30天连红需要
            $builder->where('firsttime', '>', now()->subDays(40));
        }


        return $builder
            ->select(['betKind'])
            ->distinct()
            ->pluck('betKind')
            ->toArray();
    }


    private function handleLeagueTag($uid, $sportId)
    {
        $leagueIdArr = $this->getLeagueIdArr($uid, $sportId, $this->isFirst);

        foreach ($leagueIdArr as $leagueId) {

            app(ExpertLeagueTagInitService::class)
                ->setIsFirst($this->isFirst)
                ->setSportId($sportId)
                ->setUid($uid)
                ->setLeagueId($leagueId)
                ->main();

        }
    }


    /**
     * 获取满足条件的用户UID
     * @param $uid
     * @param $sportId
     * @param $isFirst
     * @return mixed
     */
    private function getLeagueIdArr($uid, $sportId, $isFirst)
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid)
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1);

        if (empty($sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $sportId);
        }

        //是否首次
        if (!$isFirst) {
            $builder->where('firsttime', '>', now()->subDays(5));
        }

        return $builder->select(['league_id'])
            ->distinct()
            ->pluck('league_id')
            ->toArray();
    }

    private function handleTag($uid)
    {
        app(ExpertTagInitService::class)
            ->setIsFirst($this->isFirst)
            ->setSportId(0)
            ->setUid($uid)
            ->main();

        app(ExpertTagInitService::class)
            ->setIsFirst($this->isFirst)
            ->setSportId(1)
            ->setUid($uid)
            ->main();

        app(ExpertTagInitService::class)
            ->setIsFirst($this->isFirst)
            ->setSportId(2)
            ->setUid($uid)
            ->main();
    }
}
