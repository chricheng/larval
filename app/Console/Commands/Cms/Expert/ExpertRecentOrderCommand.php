<?php

namespace App\Console\Commands\Cms\Expert;

use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Passport\Pay\PayNewsModel;
use Illuminate\Console\Command;

class ExpertRecentOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms-expert:recent-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '统计近期订单销量';


    const FOOTBALL_TYPE   = [2, 9];
    const BASKETBALL_TYPE = [11, 12];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //近三天
        $this->handleData(3, 'order_num_3_days', 0);
        $this->handleData(3, 'order_num_3_days', 1);
        $this->handleData(3, 'order_num_3_days', 2);

        //近七天
        $this->handleData(7, 'order_num_7_days', 0);
        $this->handleData(7, 'order_num_7_days', 1);
        $this->handleData(7, 'order_num_7_days', 2);

        //近三十天
        $this->handleData(30, 'order_num_30_days', 0);
        $this->handleData(30, 'order_num_30_days', 1);
        $this->handleData(30, 'order_num_30_days', 2);

        return 0;
    }

    private function handleData($days = 3, $field = 'order_num_3_days', $sportId = 0)
    {
        $builder = PayNewsModel::query()
            ->whereBetween('pay_time', [
                now()->subDays($days)->startOfDay(),
                now()->subDays(1)->endOfDay(),
            ])
            ->whereIn('status', [3, 4, 5]);

        if ($sportId == 1) {
            $builder->whereIn('type', self::FOOTBALL_TYPE);
        } elseif ($sportId == 2) {
            $builder->whereIn('type', self::BASKETBALL_TYPE);
        } else {
            $builder->whereIn('type', array_merge(self::FOOTBALL_TYPE, self::BASKETBALL_TYPE));
        }

        $arr = $builder
            ->selectRaw('author_uid,count(1) as num')
            ->groupBy('author_uid')
            ->orderByDesc('num')
            ->limit(30)
            ->pluck('num', 'author_uid')
            ->toArray();

        foreach ($arr as $uid => $num) {

            ExpertTagModel::query()
                ->updateOrCreate([
                    'uid'      => $uid,
                    'sport_id' => $sportId
                ], [
                    $field => $num
                ]);
        }

        ExpertTagModel::query()
            ->whereNotIn('uid', array_keys($arr))
            ->where('sport_id', $sportId)
            ->update([
                $field => 0
            ]);
    }
}
