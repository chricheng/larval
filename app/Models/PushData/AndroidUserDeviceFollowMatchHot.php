<?php

namespace App\Models\PushData;

use App\Models\BaseModel;

/**
 * App\Models\PushData\AndroidUserDeviceFollowMatchHot
 *
 * @property int $ID
 * @property string|null $DeviceNo
 * @property string|null $DeviceToken
 * @property string|null $Remark
 * @property string|null $UpdateTime
 * @property string|null $src
 * @property string|null $channel
 * @property string|null $v
 * @property string|null $client_ip
 * @property int|null $badge 未读消息数
 * @property int|null $badge_at @我的消息数
 * @property int|null $badge_remark 评论消息数
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot query()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereBadge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereBadgeAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereBadgeRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereClientIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereDeviceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereDeviceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatchHot whereV($value)
 * @mixin \Eloquent
 */
class AndroidUserDeviceFollowMatchHot extends BaseModel
{
    protected $connection = 'push_data';
    protected $table      = 'AndroidDeviceToken';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
