<?php

namespace App\Models\PushData;

use App\Models\BaseModel;

/**
 * App\Models\PushData\AndroidDeviceToken
 *
 * @property int $ID
 * @property string|null $DeviceNo
 * @property string|null $DeviceToken
 * @property string|null $Remark
 * @property string|null $UpdateTime
 * @property string|null $src
 * @property string|null $channel
 * @property string|null $v
 * @property string|null $client_ip
 * @property int|null $badge 未读消息数
 * @property int|null $badge_at @我的消息数
 * @property int|null $badge_remark 评论消息数
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken query()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereBadge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereBadgeAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereBadgeRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereClientIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereDeviceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereDeviceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidDeviceToken whereV($value)
 * @mixin \Eloquent
 */
class AndroidDeviceToken extends BaseModel
{
    protected $connection = 'push_data';
    protected $table      = 'AndroidDeviceToken';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
