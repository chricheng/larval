<?php

namespace App\Models\PushData;

use App\Models\BaseModel;

/**
 * App\Models\PushData\AndDeviceUserModel
 *
 * @property int $ID
 * @property string|null $DeviceNo
 * @property string|null $deviceSign 设备第一次注册时的标识
 * @property int $uid 用户ID
 * @property string|null $Remark
 * @property string|null $UpdateTime
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereDeviceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereDeviceSign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndDeviceUserModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class AndDeviceUserModel extends BaseModel
{
    protected $connection = 'push_data';
    protected $table      = 'AndDeviceUser';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
