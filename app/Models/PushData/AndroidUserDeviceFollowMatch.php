<?php

namespace App\Models\PushData;

use App\Models\BaseModel;

/**
 * App\Models\PushData\AndroidUserDeviceFollowMatch
 *
 * @property int $ID
 * @property string|null $DeviceNo
 * @property int|null $MatchID
 * @property int|null $Status
 * @property string|null $UpdateTime
 * @property string|null $Remark
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch query()
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereDeviceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereMatchID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AndroidUserDeviceFollowMatch whereUpdateTime($value)
 * @mixin \Eloquent
 */
class AndroidUserDeviceFollowMatch extends BaseModel
{
    protected $connection = 'push_data';
    protected $table      = 'AndroidUserDeviceFollowMatch';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
