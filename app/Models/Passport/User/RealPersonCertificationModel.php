<?php


namespace App\Models\Passport\User;


use App\Models\BaseModel;

/**
 * App\Models\Passport\User\RealPersonCertificationModel
 *
 * @property int $id
 * @property int|null $uid 用户uid
 * @property int|null $status 状态：0待审核1已通过2驳回
 * @property string|null $real_name 真实姓名
 * @property string|null $real_id 真实身份证号
 * @property string|null $memo 备注
 * @property string|null $create_time 创建时间
 * @property string|null $update_time
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereRealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RealPersonCertificationModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class RealPersonCertificationModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'real_person_certification';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
