<?php


namespace App\Models\Passport\User;


use App\Models\BaseModel;

/**
 * App\Models\Passport\User\UserFriendModel
 *
 * @property int $id
 * @property int|null $uid 用户id
 * @property int|null $followuid 关注人id
 * @property string|null $followtime 关注事件
 * @property int|null $status 关注状态1关注2取消关注
 * @property string|null $modtime 修改时间
 * @property int|null $uid_type 是否是作家0不是，1是
 * @property int|null $followuid_type 是否是作家0不是，1是
 * @property string|null $update_time
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereFollowtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereFollowuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereFollowuidType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereModtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereUidType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserFriendModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class UserFriendModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'user_friend';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
