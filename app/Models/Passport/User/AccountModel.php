<?php


namespace App\Models\Passport\User;


use App\Models\BaseModel;

/**
 * App\Models\Passport\User\AccountModel
 *
 * @property int $uid 用户ID
 * @property int|null $balance 账户余额:分
 * @property string|null $first_pay_time 首次充值时间
 * @property string|null $last_pay_time 最后充值时间
 * @property int|null $pay_times 充值次数
 * @property int|null $pay_money 充值总额度:分
 * @property string|null $first_buy_time 首次消费时间
 * @property string|null $last_buy_time 最后消费时间
 * @property int|null $buy_times 消费次数
 * @property int|null $buy_money 消费总额度:分
 * @property string|null $update_time
 * @property string|null $ext 扩展预留
 * @property int|null $win_money 已中奖总金额
 * @property int|null $fast_win_money 快频已中奖总金额
 * @property int|null $exch_money 已兑换总金额
 * @property int|null $news_money 资讯收到付费阅读总金额
 * @property int $third_news_money 第三方分成金额：分
 * @property int $exch_third_news_money 第三方提现金额：分
 * @property int|null $exch_news_money 已兑换付费阅读金额
 * @property int|null $reward_money 资讯收到打赏总金额
 * @property int|null $exch_reward_money 已兑换打赏金额
 * @property int|null $lock_money 锁定充值额度
 * @property int|null $points 礼物点卷
 * @property int|null $chat_group_money 直播间金额
 * @property int|null $exch_chat_group_money 已兑换直播间金额
 * @property int|null $reward_group_money 直播间打赏金额
 * @property int|null $exch_reward_group_money 已提取直播间打赏金额
 * @property int|null $present_money 直播间金额
 * @property int|null $exch_present_money 直播间金额
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereBuyMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereBuyTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereChatGroupMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchChatGroupMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchNewsMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchPresentMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchRewardGroupMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchRewardMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExchThirdNewsMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereFastWinMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereFirstBuyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereFirstPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereLastBuyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereLastPayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereLockMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereNewsMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel wherePayMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel wherePayTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel wherePresentMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereRewardGroupMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereRewardMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereThirdNewsMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountModel whereWinMoney($value)
 * @mixin \Eloquent
 */
class AccountModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'account';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
