<?php


namespace App\Models\Passport\User;


use App\Models\BaseModel;

/**
 * App\Models\Passport\User\BindUserModel
 *
 * @property int $uid 用户id
 * @property string $binduserid 第三方ID
 * @property string $bindappsrc 第三方平台
 * @property string $bindtime 绑定时间
 * @property string $modtime 更新时间
 * @property string $nickname 第三方平台昵称
 * @property string $avatar 第三方平台头像
 * @property string $refreshtoken 身份验证码
 * @property string $ext 扩展预留
 * @property string|null $src 来源
 * @property string|null $ip IP
 * @property string|null $openid 开放平台ID
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereBindappsrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereBindtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereBinduserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereModtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereRefreshtoken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BindUserModel whereUid($value)
 * @mixin \Eloquent
 */
class BindUserModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'bind_user';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

}
