<?php

namespace App\Models\Passport\User;

use App\Models\BaseModel;


/**
 * App\Models\Passport\User\AccountBill
 *
 * @property int $id
 * @property int $uid 用户uid
 * @property int $balance 当前余额
 * @property int|null $status 1:正常，2已退款
 * @property string|null $trade_no 交易流水号
 * @property int|null $acc_type 账号交易类型，1:充值,2:消费,3:赠送,4:收获
 * @property string $create_time 创建时间
 * @property string|null $update_time 更新时间
 * @property int $in_money 充值额度
 * @property int $out_money 支出额度
 * @property string|null $client_ip 下单时ip
 * @property string|null $memo 备注
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill query()
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereAccType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereClientIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereInMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereOutMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AccountBill whereUpdateTime($value)
 * @mixin \Eloquent
 */
class AccountBill extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'account_bill';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
