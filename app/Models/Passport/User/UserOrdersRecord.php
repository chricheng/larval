<?php

namespace App\Models\Passport\User;

use App\Models\BaseModel;


/**
 * App\Models\Passport\User\UserOrdersRecord
 *
 * @property int $id
 * @property int $uid uid
 * @property string|null $reg_time 注册时间
 * @property string|null $frist_order_time 第一次下单时间
 * @property string|null $last_order_time 最后一次下单时间
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereFristOrderTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereLastOrderTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereRegTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserOrdersRecord whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserOrdersRecord extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'user_orders_record';
    protected $guarded    = [];


}
