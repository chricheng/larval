<?php


namespace App\Models\Passport\Expert;


use App\Models\BaseModel;
use App\Models\User;

/**
 * App\Models\Passport\Expert\AuthorNewsModel
 *
 * @property int $id
 * @property int $uid 用户ID
 * @property int $auditor 审核用户ID
 * @property int $status 申请状态：1待审核,2驳回,3通过
 * @property string|null $content 申请内容
 * @property string|null $old_content 驳回内容
 * @property string|null $create_time 申请时间
 * @property int|null $type 0:0天驳回后可申请,7:7天后可申请
 * @property string|null $time_start 开始时间
 * @property string|null $reason 原因
 * @property string|null $audit_time 操作时间
 * @property string|null $update_time
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereAuditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereAuditor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereOldContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorNewsModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class AuthorNewsModel extends BaseModel
{

    protected $connection = 'passport';
    protected $table      = 'author_news';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'uid', 'uid');
    }
}
