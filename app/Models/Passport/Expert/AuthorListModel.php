<?php


namespace App\Models\Passport\Expert;


use App\Models\BaseModel;
use App\Models\User;

/**
 * App\Models\Passport\Expert\AuthorListModel
 *
 * @property int $id
 * @property int $uid 用户id
 * @property string|null $create_time 申请时间
 * @property string|null $pass_time 审批通过时间
 * @property string|null $first_time 首次发文时间
 * @property string|null $final_time 最后发文时间
 * @property string|null $final_title 最后发文标题
 * @property string|null $final_news_id 最后发布文章ID
 * @property int|null $news_nun 发布文章总数量
 * @property string|null $real_name 真实姓名
 * @property string|null $real_id 真实身份证号
 * @property string|null $real_phone 真实手机号
 * @property string|null $real_img 真实身份证URL
 * @property int|null $pass_flag 审核状态0待审核,1通过,2驳回,3取消
 * @property string|null $nickname 待审用户昵称
 * @property string|null $avatar 待审用户头像
 * @property string|null $intro 待审核个人简介
 * @property string|null $commit_time 提交时间
 * @property int|null $audit_flag 审核状态0待审核,1通过,2驳回,3取消
 * @property int|null $err_code 错误提示
 * @property int|null $check_phone 0未验证手机号，已验证手机号
 * @property string|null $bank_account_name 银行账户名
 * @property string|null $bank_account_number 银行账户账号
 * @property string|null $bank_name 开户行名称
 * @property int|null $points 作者积分
 * @property int|null $close_time 封停时间
 * @property int|null $close_type 0正常，1封停，2审核中
 * @property string $front_idcard 身份证正面照
 * @property string $opposite_idcard 身份证反面照
 * @property int|null $real_id_type 身份证件类型: 0 身份证 1其他
 * @property int|null $bank_flag 支付宝审核状态0待审核,1通过,2驳回
 * @property int|null $id_flag 身份证照片审核状态0待审核,1通过,2驳回
 * @property string $bank_reason 支付宝审核失败原因
 * @property string $id_reason 身份证照片审核失败原因
 * @property string $author_source 作者来源标示
 * @property string|null $update_time
 * @property int|null $subject_change 0未操作1已同意
 * @property string|null $final_group_chat_id 聊天室ID
 * @property string|null $final_group_chat_time 开播时间
 * @property int|null $final_group_chat_status 直播间状态
 * @property string|null $final_dynamic_time 最后发生行为动态时间
 * @property int|null $buy_news_num 可购买文章数
 * @property string|null $end_apply_time 最后申请的时间
 * @property int|null $anchor_status 审核状态-1 未申请 0待审核,1通过,2驳回,3取消
 * @property string $anchor_reason 主播原因
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereAnchorReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereAnchorStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereAuditFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereAuthorSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBankAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBankFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBankReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereBuyNewsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereCheckPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereCloseTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereCloseType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereCommitTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereEndApplyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereErrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalDynamicTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalGroupChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalGroupChatStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalGroupChatTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFinalTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFirstTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereFrontIdcard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereIdFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereIdReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereNewsNun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereOppositeIdcard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel wherePassFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel wherePassTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereRealId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereRealIdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereRealImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereRealPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereSubjectChange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AuthorListModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class AuthorListModel extends BaseModel
{

    protected $connection = 'passport';
    protected $table      = 'author_list';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'uid', 'uid');
    }
}
