<?php

namespace App\Models\Passport\Pay;

use App\Models\BaseModel;

/**
 * App\Models\Passport\Pay\PayCategoryModel
 *
 * @property int $id
 * @property int $money 金额:分
 * @property string $type 时间格式: day  month  year
 * @property int $number
 * @property int $data_type 数据类型 1:大热 2:悬殊 3:同赔 4:极限 5:庄家态度 10足球赛事数据
 * @property int|null $sort 排序
 * @property int $old_money 原价金额：分
 * @property int|null $cooperate_type 1 即嗨 2 体育疯 3 章鱼
 * @property int|null $renewal_money 续费价格
 * @property int|null $is_auto 是否自动续费： 1是   0否
 * @property int|null $status 是否在后台输出：0 输出【签名包】  1不输出【商城包】
 * @property int|null $is_default 是否默认价格控制前台显示
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereCooperateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereDataType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereIsAuto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereOldMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereRenewalMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayCategoryModel whereType($value)
 * @mixin \Eloquent
 */
class PayCategoryModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'pay_category';

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
