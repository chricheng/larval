<?php


namespace App\Models\Passport\Pay;


use App\Models\BaseModel;

/**
 * App\Models\Passport\Pay\PayGroupModel
 *
 * @property int $id
 * @property int|null $uid 用户id
 * @property int|null $author_uid 作者用户ID
 * @property string|null $group_id 直播间ID
 * @property int $money 付费金额：分
 * @property string|null $pay_time 购买时间
 * @property string|null $trade_no 交易流水号
 * @property int|null $status 1下单成功，2余额不足等待支付，3购买成功，4已结算，5已退款
 * @property int|null $coupon_fee 优惠卷支付额度
 * @property int|null $pay_fee 实际支付额度
 * @property int|null $coupon_id 使用的优惠卷ID
 * @property string $src 渠道来源
 * @property string|null $end_time 结算时间
 * @property string|null $update_time
 * @property int|null $scene
 * @property string|null $memo 备注
 * @property int|null $coupon_info_id 红包id
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereAuthorUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereCouponFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereCouponInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel wherePayFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereScene($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayGroupModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class PayGroupModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'pay_group';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

}
