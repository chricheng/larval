<?php

namespace App\Models\Passport\Pay;

use App\Models\BaseModel;

/**
 * App\Models\Passport\Pay\PayDataModel
 *
 * @property int $id
 * @property int|null $uid 用户id
 * @property int|null $data_type 数据类型 1:大热 2:悬殊 3:同赔 4:极限 5:庄家态度 10足球赛事数据
 * @property int $money 付费金额：分
 * @property int|null $coupon_fee 优惠卷支付额度
 * @property int|null $pay_fee 实际支付额度
 * @property int|null $coupon_id 使用的优惠卷ID
 * @property int|null $coupon_info_id 创建红包id
 * @property string|null $pay_time 下单时间
 * @property string|null $trade_no 交易流水号
 * @property int|null $status 1下单成功，2余额不足等待支付，3购买成功, 4已退款
 * @property string $src 渠道来源
 * @property string|null $start_time 有效期开始时间
 * @property string|null $end_time 有效期结束时间
 * @property string|null $ext
 * @property int|null $match_id 赛事id
 * @property int|null $trade_no_status 菠菜汪第三方支付成功状态:  0 未通知  1已通知
 * @property string|null $third_trade_no 菠菜汪订单号
 * @property int|null $is_system 标示：系统还是用户产生的记录  0用户 1 系统
 * @property int|null $is_auto 该条数据是否是自动续费的数据： 1 是  0 否
 * @property int|null $is_bkb 是否是ai智篮分析购买   1是  0否
 * @property int|null $period_id 购买期次id 唯一标识
 * @property int|null $subscribe_num 订阅期数 1 或者 10
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereCouponFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereCouponInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereDataType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereIsAuto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereIsBkb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereIsSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereMatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel wherePayFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel wherePeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereSubscribeNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereThirdTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereTradeNoStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereUid($value)
 * @mixin \Eloquent
 * @property int $is_experience 是否体验会员 1是 0否
 * @property int $expect_num 权益预计发放次数
 * @property int $actual_num 权益实际发放次数
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereActualNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereExpectNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereIsExperience($value)
 * @property int|null $claim_time 领取的年月
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereClaimTime($value)
 * @property string|null $update_time
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataModel whereUpdateTime($value)
 */
class PayDataModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'pay_data';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
