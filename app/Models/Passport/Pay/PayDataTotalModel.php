<?php

namespace App\Models\Passport\Pay;

use App\Models\BaseModel;

/**
 * App\Models\Passport\Pay\PayDataTotalModel
 *
 * @property int $id
 * @property int|null $uid 用户id
 * @property int|null $data_type 数据类型 1:大热 2:悬殊 3:同赔 4:极限 5:庄家态度 10足球赛事数据
 * @property string|null $pay_start_time 购买行为有效期开始时间
 * @property string|null $pay_end_time 购买行为的有效期结束时间
 * @property string|null $start_time 实际开始时间
 * @property string|null $end_time 实际失效时间
 * @property int|null $status 1有效，2无效
 * @property string $src 渠道来源
 * @property string|null $ext
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereDataType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel wherePayEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel wherePayStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayDataTotalModel whereUid($value)
 * @mixin \Eloquent
 */
class PayDataTotalModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'pay_data_total';

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
