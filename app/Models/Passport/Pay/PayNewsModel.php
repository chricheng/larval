<?php


namespace App\Models\Passport\Pay;


use App\Models\BaseModel;
use App\Models\User;

/**
 * App\Models\Passport\Pay\PayNewsModel
 *
 * @property int $id
 * @property int|null $uid 用户id
 * @property int|null $author_uid 作者用户ID
 * @property string|null $news_id 资讯ID
 * @property int $money 付费金额：分
 * @property string|null $pay_time 打赏时间
 * @property string|null $trade_no 交易流水号
 * @property int|null $status 1下单成功，2余额不足等待支付，3支付成功
 * @property string $src 渠道来源
 * @property string|null $end_time 结算时间
 * @property int|null $buy_type 1不中返款，0不中不返
 * @property int|null $coupon_fee 优惠卷支付额度
 * @property int|null $pay_fee 实际支付额度
 * @property int|null $coupon_id 使用的优惠卷ID
 * @property string|null $update_time
 * @property int|null $editorid 编辑ID，0为外部作者
 * @property string|null $memo 结算期强制退款操作
 * @property int|null $vip_level VIP等级
 * @property float|null $discount 折扣率
 * @property int|null $coupon_info_id 红包id
 * @property int $type 资讯类型，同cms下news表
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereAuthorUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereBuyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereCouponFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereCouponInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereEditorid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereMemo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel wherePayFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel wherePayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayNewsModel whereVipLevel($value)
 * @mixin \Eloquent
 */
class PayNewsModel extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'pay_news';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'uid', 'uid');
    }
}
