<?php


namespace App\Models\QtBasketball;


use App\Models\BaseModel;

/**
 * App\Models\QtBasketball\BasketballLeagueModel
 *
 * @property int $league_id 联赛ID
 * @property string|null $color 联赛颜色值
 * @property string|null $name_s 联赛名称简称
 * @property string|null $name_j 联赛名称简体
 * @property string|null $name_f 联赛名称繁体
 * @property string|null $name_e 联赛名称英文
 * @property int|null $kind 联赛类型1联赛，2杯赛
 * @property int|null $part_num 比赛分几节
 * @property string|null $curr_match_season 当前赛季
 * @property string|null $country_id 国家ID
 * @property string|null $country 国家名称
 * @property string|null $part_time 1节打几分钟
 * @property string|null $logo 联赛的标志
 * @property string|null $rule 赛制，所订的规则
 * @property string $update_time 最后更新时间
 * @property string|null $begin_season 开始赛季
 * @property int|null $level 联赛级别
 * @property int|null $sort 客户端排序
 * @property int|null $curr_year 当前年份
 * @property int|null $curr_month 当前月份
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereBeginSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereCurrMatchSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereCurrMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereCurrYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereLeagueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereNameE($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereNameF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereNameS($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel wherePartNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel wherePartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballLeagueModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class BasketballLeagueModel extends BaseModel
{
    protected $connection = 'qt_basketball';
    protected $table      = 'league';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'league_id';

    /**
     * 指明模型的 ID 不是自增。
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
