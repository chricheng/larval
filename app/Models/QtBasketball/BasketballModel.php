<?php


namespace App\Models\QtBasketball;


use App\Models\BaseModel;

/**
 * App\Models\QtBasketball\BasketballModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel query()
 * @mixin \Eloquent
 * @property int $event_id 赛事id
 * @property int|null $league_id 联赛|杯赛ID
 * @property int|null $kind 赛事类型联赛，1联赛，2杯赛
 * @property string|null $league_name_j 简体名
 * @property string|null $league_name_f 繁体名
 * @property int|null $event_type 分几节进行，2:上下半场，4：分4小节
 * @property string|null $color 色值
 * @property string|null $match_time 比赛的时间
 * @property string|null $last_time 小节剩余时间
 * @property int|null $match_state 比赛状态
 * @property int|null $home_id 主队编号
 * @property int|null $away_id 客队编号
 * @property string|null $home_name_j 主队名称简体
 * @property string|null $home_name_f 主队名称繁体
 * @property string|null $away_name_j 客队名称简体
 * @property string|null $away_name_f 客队名称繁体
 * @property string|null $home_order 主队排名
 * @property string|null $away_order 客队排名
 * @property int|null $home_score 主队得分
 * @property int|null $away_score 客队得分
 * @property int|null $home_1st_score 第一节|上半场主队得分
 * @property int|null $away_1st_score 第一节|上半场客队得分
 * @property int|null $home_2nd_score 第二节场主队得分
 * @property int|null $away_2nd_score 第二节场客队得分
 * @property int|null $home_3rd_score 第三节|下半场主队得分
 * @property int|null $away_3rd_score 第三节|下半场客队得分
 * @property int|null $home_4th_score 第四节场主队得分
 * @property int|null $away_4th_score 第四节场客队得分
 * @property int|null $ot_times 加时次数
 * @property int|null $home_1ot_score 1ot主队得分
 * @property int|null $away_1ot_score 1ot客队得分
 * @property int|null $home_2ot_score 2ot主队得分
 * @property int|null $away_2ot_score 2ot客队得分
 * @property int|null $home_3ot_score 3ot主队得分
 * @property int|null $away_3ot_score 3ot客队得分
 * @property int|null $isanaly 是否有技术统计
 * @property int|null $neutral 1=中立场
 * @property int|null $tv 电视直播
 * @property string|null $update_time 最后更新时间
 * @property int|null $is_cp 是否彩票赛事
 * @property string|null $season 赛季
 * @property string|null $season_kind 赛季类型
 * @property string|null $location 比赛场地
 * @property string|null $grouping 分组例如第一圈，只有杯赛或季后赛才有数据
 * @property string|null $cup_grouping 比赛子分类，例如A组，只有杯赛才有数据
 * @property int|null $pcFlag pc直播
 * @property int|null $is_bet_game 是否开启投注游戏
 * @property int|null $is_lock 停止更新0否1是
 * @property int|null $live_data 数据动画直播
 * @property string|null $time_compute 新时间计算方式
 * @property int|null $update_timestamp 数据最后更新时间
 * @property int|null $followers_num 赛事关注人数
 * @property string|null $total_score 篮球总比分：如季后赛3-2
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway1otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway1stScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway2ndScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway2otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway3otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway3rdScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAway4thScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAwayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAwayNameF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAwayNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAwayOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereAwayScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereCupGrouping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereFollowersNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereGrouping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome1otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome1stScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome2ndScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome2otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome3otScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome3rdScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHome4thScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHomeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHomeNameF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHomeNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHomeOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereHomeScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereIsBetGame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereIsCp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereIsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereIsanaly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLastTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLeagueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLeagueNameF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLeagueNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLiveData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereMatchState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereMatchTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereNeutral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereOtTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel wherePcFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereSeasonKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereTimeCompute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereTotalScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereTv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereUpdateTimestamp($value)
 * @property string|null $create_time 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|BasketballModel whereCreateTime($value)
 */
class BasketballModel extends BaseModel
{
    protected $connection = 'qt_basketball';
    protected $table      = 'event_list';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'event_id';

    /**
     * 指明模型的 ID 不是自增。
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
