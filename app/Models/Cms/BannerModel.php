<?php

namespace App\Models\Cms;

use App\Models\BaseModel;

/**
 * App\Models\Cms\BannerModel
 *
 * @property int $id
 * @property int $ad_type 广告类型：1、老版本一张图样式 2、老版本三张图样式 3、新版本纯图片样式
 * @property string $title 标题
 * @property string $imgurl 图片地址
 * @property string $url 跳转地址
 * @property string $brief 客户端预留描述
 * @property int $sort 排序
 * @property int $type 类型 1轮播2开机3精读
 * @property int|null $newsid 精读对应资讯ID
 * @property int $status 状态 1上线2下线
 * @property int $code
 * @property int|null $version 版本
 * @property string|null $params 参数
 * @property int $device 终端1H5 2ios 3and
 * @property string $channel 渠道 1 历史版本渠道
 * @property int $jumptype 跳转类型 1app内部2内嵌h53浏览器打开
 * @property string $starttime 开始时间
 * @property string $endtime 开始时间
 * @property string $createtime 添加时间
 * @property int $updateperson 修改人ID
 * @property string $updatetime 修改时间
 * @property string $ad_source 广告来源
 * @property int $is_show 是否显示广告标识
 * @property int $sign_in 是否需要登录1是0否
 * @property string $avatar 用户头像
 * @property string $nickname 用户名称
 * @property string|null $uids 用户UID，按逗号分隔
 * @property string|null $mtitle 主标题
 * @property string|null $stitle 副标题
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereAdSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereAdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereBrief($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereCreatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereImgurl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereIsShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereJumptype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereMtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereNewsid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereSignIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereStitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereUids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereUpdateperson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereUpdatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerModel whereVersion($value)
 * @mixin \Eloquent
 */
class BannerModel extends BaseModel
{
    protected $connection = 'cms';
    protected $table      = 'banner';
    protected $guarded    = [];
    protected $hidden     = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * 获取用户的姓名
     *
     * @param  string  $value
     * @return string
     */
    public function getUidsAttribute($value)
    {
        return array_unique(explode(",",$value));
    }


}
