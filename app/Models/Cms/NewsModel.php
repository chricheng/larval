<?php


namespace App\Models\Cms;


use App\Models\BaseModel;


/**
 * App\Models\Cms\NewsModel
 *
 * @property int $id
 * @property string|null $title
 * @property string $brief 简介
 * @property int $type 资讯展示页面类型，对应板块view_type
 * @property int $sales 购买人数
 * @property int $status 资讯状态0已保存,1已上线,2已下线,3已删除,4待审核,5审核未通过
 * @property int $for_sale 可购买标示1可购买2不可购买
 * @property int $seal_status 盖章状态0没有章,1质量低,2违规,4抄袭
 * @property int $mp_points 作者发文时积分
 * @property int|null $has_back 0待返款，1返款中，2已返款
 * @property int|null $need_back 1不中返款，0不中不返
 * @property int|null $end_open 1完赛开放，0完赛不开放
 * @property int $isWin 中奖状态0等待开奖 1中奖 2未中奖 3走盘
 * @property string|null $source 来源
 * @property int $free 免费资讯1是,2否
 * @property int $fee 付费金额：分
 * @property int|null $source_id 外部资讯来源ID
 * @property int $is_hot 热门标示
 * @property int $is_recommend 推荐标示
 * @property int $is_video 视频标示
 * @property string|null $plates 资讯板块
 * @property string|null $top_plates 资讯置顶板块
 * @property string|null $tags 资讯标签
 * @property int $jc 竞彩标示
 * @property string $jcId 竞彩ID
 * @property string $picture_1 资讯配图
 * @property string $picture_2 资讯配图
 * @property string $picture_3 资讯配图
 * @property int $insidecomments 内部评论数
 * @property int $insideclicks 内部点赞数
 * @property int $insiderewords 内部打赏数
 * @property int $usercomments 评论数
 * @property int $userclicks 点赞数
 * @property int $userrewords 打赏数
 * @property int $editorid 编辑ID
 * @property int $authorid 发布人ID
 * @property string $createtime 添加时间
 * @property int $updateperson 修改人ID
 * @property string|null $updatetime 修改时间
 * @property int $reviewperson 审核人ID
 * @property string|null $reviewtime 审核时间
 * @property string|null $freetime 免费时间
 * @property string|null $firsttime 首次上线时间
 * @property int $browsenum 浏览量
 * @property int $topflag 置顶显示
 * @property string|null $toptime 置顶时间
 * @property int $usertype 用户类型1正常，2虚拟
 * @property string|null $betCode 推荐投注信息
 * @property string $match 关联赛事
 * @property string $result 推荐赛果
 * @property string $betKind 推荐投注玩法：
 * @property int $matchid 关联赛事ID
 * @property int|null $is_opened 0未开赛1已开赛
 * @property string|null $match_time 赛事时间
 * @property string|null $odds 投注时盘口
 * @property string|null $betOdds 投注时赔率
 * @property string|null $winOdds 中奖赔率
 * @property string|null $betTopOdds 投注时最高赔率
 * @property int|null $betFee 投注额度：分
 * @property int|null $expFee 预计中奖额度：分
 * @property int|null $winFee 中奖额度：分
 * @property int|null $reason_type 下线原因:1质量低2违规3其他
 * @property string $reason 驳回或下线原因
 * @property mixed $seal_reason 盖章原因
 * @property int $reviewstatus 审核状态0待审核,1通过,2驳回
 * @property string|null $arraign_time 提交审核时间
 * @property int|null $support 支持数
 * @property int|null $against 反对数
 * @property int $browse 真实浏览量
 * @property int|null $first_day
 * @property int|null $companyId 赔率公司
 * @property int $game_id 电竞游戏ID
 * @property int|null $hd_number 公司赔率编号
 * @property string|null $update_time
 * @property string|null $riskLevel 处置建议可能返回值： PASS：正常，建议直接 放行REVIEW：可疑，建议人 工审核 REJECT：违规，建议直 接拦截
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereAgainst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereArraignTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereAuthorid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBetCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBetFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBetKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBetOdds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBetTopOdds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBrief($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBrowse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereBrowsenum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereCreatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereEditorid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereEndOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereExpFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereFirstDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereFirsttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereForSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereFreetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereHasBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereHdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereInsideclicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereInsidecomments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereInsiderewords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereIsOpened($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereIsVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereIsWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereJc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereJcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereMatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereMatchTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereMatchid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereMpPoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereNeedBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereOdds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePicture1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePicture2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePicture3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePlates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereReasonType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereReviewperson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereReviewstatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereReviewtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereRiskLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSealReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSealStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSupport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereTopPlates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereTopflag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereToptime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUpdateperson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUpdatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUserclicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUsercomments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUserrewords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereUsertype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereWinFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereWinOdds($value)
 * @mixin \Eloquent
 * @property int|null $sport_id 0 全部 1足球 2篮球
 * @property int|null $league_id 联赛ID
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereLeagueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel whereSportId($value)
 * @property int $predict_status 预测状态 0 初始 1已处理
 * @property string|null $predict_time 预测结果时间
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePredictStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NewsModel wherePredictTime($value)
 */
class NewsModel extends BaseModel
{

    protected $connection = 'cms';
    protected $table      = 'news';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
