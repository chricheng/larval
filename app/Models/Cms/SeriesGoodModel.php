<?php


namespace App\Models\Cms;


use App\Models\BaseModel;

/**
 * App\Models\Cms\SeriesGoodModel
 *
 * @property int $uid 作者id
 * @property int $leagueID 联赛id
 * @property string $leagueNameJ 联赛名称
 * @property int $num 发文总数
 * @property int $win 命中数
 * @property int $zou 走盘数
 * @property int $miss 黑单数
 * @property float $return_rate 回报率
 * @property float $hit_rate 命中率
 * @property string|null $update_time
 * @property string|null $max_time 最后发文时间
 * @property int $type 1 足球 2 篮球
 * @property float $last_x_y_win_rate 从当前场次前推50场取xy最高命中率，百分百命中清0
 * @property int $last_x 从当前场次前推50场计算的总场次，百分百命中清0
 * @property int $last_y 从当前场次前推50场命中的场次，百分百命中清0
 * @property int $hit 近期连红
 * @property int $hit_max 最大连红
 * @property int $several_win_count 近几中几统计场次
 * @property int $several_win_count_20 近几中几统计场次
 * @property int $several_win_count_50 近几中几统计场次
 * @property float $last_ten_return_rate 近10场回报率
 * @property int $several_win 近几中几
 * @property int $several_win_20 近几中几
 * @property int $several_win_50 近几中几
 * @property string $last_seven_day_return_rate 7天的 回报率
 * @property float $severval_win_rate 战绩10场回报率
 * @property float|null $severval_win_20_rate 战绩20场回报率
 * @property float|null $severval_win_50_rate 战绩50场回报率
 * @property int|null $hit_with_count
 * @property float $last_x_y_new_win_rate 3910 联赛总的命中率
 * @property int $last_x_new 3910 联赛 x 场次
 * @property int $last_y_new 3910 联赛 y 场次
 * @property string $recent_trend 近期走势
 * @property int|null $serveral_win_x 近几中几 真实的 Y 做 10场显示判断
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereHitMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereHitRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereHitWithCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastSevenDayReturnRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastTenReturnRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastXNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastXYNewWinRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastXYWinRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastY($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLastYNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLeagueID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereLeagueNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereMaxTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereMiss($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereRecentTrend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereReturnRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereServeralWinX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWin20($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWin50($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWinCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWinCount20($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSeveralWinCount50($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSevervalWin20Rate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSevervalWin50Rate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereSevervalWinRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeriesGoodModel whereZou($value)
 * @mixin \Eloquent
 */
class SeriesGoodModel extends BaseModel
{
    protected $connection = 'cms';
    protected $table      = 'series_good';
    protected $guarded    = [];
}
