<?php


namespace App\Models\Cms\Expert;


use App\Models\BaseModel;


/**
 * App\Models\Cms\Expert\ExpertTagModel
 *
 * @property int $id
 * @property int $uid UID
 * @property int $sport_id 0全部 1足球 2篮球
 * @property int $hit_rate 当前命中率
 * @property string $hit_rate_desc 当前命中率描述:近M中N
 * @property int $hit_high_num 当前连红数
 * @property int $max_high_num 最大连红
 * @property int $admin_rank 权重
 * @property string $recommend_desc 推荐描述
 * @property int $can_buy 0 没有可购买方案 1 有可购买方案
 * @property int $buy_count 可购买方案数量
 * @property string|null $last_article_time 最后发文时间
 * @property int $return_rate_7_days 7天回报率
 * @property int $return_rate_10_sessions 近10场回报率
 * @property int $article_num_7_days 7天推荐场次
 * @property int $article_num_10_sessions 10场推荐场次
 * @property int $order_num_3_days 近3天销量
 * @property int $order_num_7_days 近7天销量
 * @property int $order_num_30_days 近30天销量
 * @property int $profit_num_3_days 近3天盈利天数
 * @property int $profit_num_7_days 近7天盈利天数
 * @property int $profit_num_20_days 近20天盈利天数
 * @property int $hit_num_10_sessions 近10场命中
 * @property int $hit_num_20_sessions 近20场命中
 * @property int $hit_num_50_sessions 近50场命中
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereAdminRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereArticleNum10Sessions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereArticleNum7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereBuyCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereCanBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitNum10Sessions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitNum20Sessions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitNum50Sessions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitRateDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereLastArticleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereMaxHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereOrderNum30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereOrderNum3Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereOrderNum7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereProfitNum20Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereProfitNum3Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereProfitNum7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereRecommendDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereReturnRate10Sessions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereReturnRate7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $hit_rate_x 当前M
 * @property int $hit_rate_y 当前N
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitRateX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitRateY($value)
 * @property array $hit_last_10
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHitLast10($value)
 * @property int $hong_7_days 7天红
 * @property int $zou_7_days 7天走
 * @property int $hei_7_days 7天黑
 * @property int $hong_30_days 30天红
 * @property int $zou_30_days 30天走
 * @property int $hei_30_days 30天黑
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHei30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHei7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHong30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereHong7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereZou30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereZou7Days($value)
 * @property int $total_order_num 总销量
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertTagModel whereTotalOrderNum($value)
 * @property-read \App\Models\Cms\Expert\ExpertModel $expert
 */
class ExpertTagModel extends BaseModel
{

    protected $connection = 'cms';
    protected $table = 'expert_tags';
    protected $guarded = [];

    /**
     * 这个属性应该被转换为原生类型.
     *
     * @var array
     */
    protected $casts = [
        'hit_last_10' => 'array',
    ];

    public function expert()
    {
        return $this->belongsTo(ExpertModel::class, 'uid', 'uid');
    }
}
