<?php


namespace App\Models\Cms\Expert;


use App\Models\BaseModel;

/**
 * Class ExpertLeagueTagModel
 *
 * @package App\Models\Cms\Expert
 * @property int $id
 * @property int $uid UID
 * @property int $sport_id 0全部 1足球 2篮球
 * @property int $league_id 联赛ID
 * @property int $hit_rate 当前命中率
 * @property int $hit_rate_x 当前M
 * @property int $hit_rate_y 当前N
 * @property string $hit_rate_desc 当前命中率描述:近M中N
 * @property int $hit_high_num 当前连红数
 * @property int $max_high_num 最大连红
 * @property string|null $last_article_time 最后发文时间
 * @property int $total_order_num 总带红人数
 * @property int $total_hit_rate 总命中率
 * @property string $total_hit_rate_desc 总命中率描述
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitRateDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitRateX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitRateY($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereLastArticleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereLeagueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereMaxHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHitRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHitRateDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalOrderNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $hit_last_10 近10场命中情况
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereHitLast10($value)
 * @property int $total_hong_num 总红
 * @property int $total_hei_num 总黑
 * @property int $total_zou_num 总走
 * @property int $total_return_rate 总回报率
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHeiNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHongNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalReturnRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalZouNum($value)
 * @property string $league_name 联赛名称
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereLeagueName($value)
 * @property int $can_buy 0 没有可购买方案 1 有可购买方案
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereCanBuy($value)
 * @property int $total_hit_rate_x
 * @property int $total_hit_rate_y
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHitRateX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertLeagueTagModel whereTotalHitRateY($value)
 */
class ExpertLeagueTagModel extends BaseModel
{

    protected $connection = 'cms';
    protected $table      = 'expert_league_tags';
    protected $guarded    = [];

    /**
     * 这个属性应该被转换为原生类型.
     *
     * @var array
     */
    protected $casts = [
        'hit_last_10' => 'array',
    ];
}
