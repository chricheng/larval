<?php


namespace App\Models\Cms\Expert;


use App\Models\BaseModel;

/**
 * App\Models\Cms\Expert\ExpertJcKindTagModel
 *
 * @property int $id
 * @property int $uid UID
 * @property int $sport_id 0全部 1足球 2篮球
 * @property string $kind 推荐投注玩法：
 * @property int $hit_rate 当前命中率
 * @property int $hit_rate_x 当前M
 * @property int $hit_rate_y 当前N
 * @property string $hit_rate_desc 当前命中率描述:近M中N
 * @property int $hit_high_num 当前连红数
 * @property int $max_high_num 最大连红
 * @property string|null $last_article_time 最后发文时间
 * @property int $total_order_num 总带红人数
 * @property array $hit_last_10 近10场命中情况
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitLast10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitRateDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitRateX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHitRateY($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereLastArticleTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereMaxHighNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereSportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereTotalOrderNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $hong_7_days 7天红
 * @property int $zou_7_days 7天走
 * @property int $hei_7_days 7天黑
 * @property int $hong_30_days 30天红
 * @property int $zou_30_days 30天走
 * @property int $hei_30_days 30天黑
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHei30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHei7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHong30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereHong7Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereZou30Days($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertJcKindTagModel whereZou7Days($value)
 */
class ExpertJcKindTagModel extends BaseModel
{
    protected $connection = 'cms';
    protected $table      = 'expert_jc_kind_tags';
    protected $guarded    = [];

    /**
     * 这个属性应该被转换为原生类型.
     *
     * @var array
     */
    protected $casts = [
        'hit_last_10' => 'array',
    ];
}
