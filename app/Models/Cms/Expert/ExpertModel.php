<?php


namespace App\Models\Cms\Expert;


use App\Models\BaseModel;


/**
 * App\Models\Cms\Expert\ExpertModel
 *
 * @property int $id
 * @property int $uid
 * @property string $nickname
 * @property string $avatar 头像
 * @property int $buy_news_num
 * @property string $intro 个人简介
 * @property int $user_type 用户类型1正常，2虚拟，3小黑侠
 * @property int $is_active 是否激活
 * @property float $exp 经验值
 * @property string|null $reg_time 注册时间
 * @property int $points
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereBuyNewsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereRegTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereUserType($value)
 * @mixin \Eloquent
 * @property int $anchor_status 审核状态-1 未申请 0待审核,1通过,2驳回,3取消
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereAnchorStatus($value)
 * @property int $audit_flag 资料审核状态0待审核,1通过,2驳回,3取消
 * @property int $pass_flag 专家审核状态0待审核,1通过,2驳回,3取消
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereAuditFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel wherePassFlag($value)
 * @property int $status 账户状态 1正常 其他非正常
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereStatus($value)
 * @property int $level 0:普通用户,1:大V
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereLevel($value)
 * @property int $follower_num 粉丝数
 * @property int $followed_num 关注数
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereFollowedNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereFollowerNum($value)
 * @property int $close_type 积分过低 0正常，1封停，2审核中
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereCloseType($value)
 * @property string|null $final_news_time 最后发文时间
 * @method static \Illuminate\Database\Eloquent\Builder|ExpertModel whereFinalNewsTime($value)
 */
class ExpertModel extends BaseModel
{
    protected $connection = 'cms';
    protected $table      = 'experts';
    protected $guarded    = [];
}
