<?php


namespace App\Models\Cms;


use App\Models\BaseModel;

/**
 * App\Models\Cms\UserMatchUnblockLogModel
 *
 * @property int $id
 * @property int $uid 用户id
 * @property int $sport_type 类型 1 足球 2 篮球
 * @property int $match_id 解锁赛事id
 * @property string|null $create_time 创建时间
 * @property string|null $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereMatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereSportType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereUpdateTime($value)
 * @mixin \Eloquent
 * @property int $sport_id 类型 1 足球 2 篮球
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel uidWhere($uid)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel uniqueWhere($data)
 * @method static \Illuminate\Database\Eloquent\Builder|UserMatchUnblockLogModel whereSportId($value)
 */
class UserMatchUnblockLogModel extends BaseModel
{
    //use ExtendModel;

    protected $connection = 'cms';
    protected $table      = 'user_match_unblock_log';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

}
