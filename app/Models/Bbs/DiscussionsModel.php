<?php


namespace App\Models\Bbs;


use App\Models\BaseModel;

/**
 * App\Models\Bbs\DiscussionsModel
 *
 * @property int $id
 * @property string $title 标题
 * @property int $posts_num 评论[回复]总数
 * @property int $post_num_index 评论楼层索引
 * @property string $created_at 申请时间
 * @property string|null $c_time 发布时间
 * @property int $author_uid 发布用户id
 * @property int|null $first_post_id 首次评论id
 * @property int|null $first_posted_uid 首次评论用户id
 * @property string|null $first_posted_at 首次评论时间
 * @property int|null $last_post_id 最后评论id
 * @property int|null $last_posted_uid 最后评论用户id
 * @property string|null $last_posted_at 最后评论时间
 * @property string|null $oper_time 操作时间
 * @property int|null $oper_uid 操作用户
 * @property int $is_private 其它用户是否可见：0 不可见  1 可见
 * @property int $is_approved 审核状态： 0 待审核  1通过审核  2拒绝审核
 * @property int $is_locked 是否锁定： 0未锁定   1锁定
 * @property int $is_top 是否置顶:   0 否  1 是
 * @property int $status 是否上线 ： 0未上线  1上线  2已下线
 * @property string|null $content 内容
 * @property string|null $update_time
 * @property string|null $ip_address ip
 * @property int|null $clicks 点击数
 * @property int|null $heats 热度值
 * @property string|null $src 来源
 * @property int|null $shares 分享数
 * @property int|null $likes 点赞数
 * @property int|null $collects 收藏数
 * @property int|null $is_hot 是否热门  1 否 2是
 * @property string|null $hots_title 热门标题
 * @property string|null $hot_p1 热门图片1
 * @property string|null $hot_p2 热门图片2
 * @property string|null $hot_p3 热门图片3
 * @property string|null $edit_reason 编辑理由
 * @property string|null $edit_time 编辑时间
 * @property int|null $edit_uid 编辑用户
 * @property int|null $best_posts_id 最佳评论id
 * @property int|null $best_posts_uid 最佳评论uid
 * @property int|null $admin_uid 内部用户发布帖子uid
 * @property string|null $reject_reason 驳回理由
 * @property string|null $remove_time 手动移出热门时间
 * @property string|null $offine_reason 下线原因
 * @property int|null $reject_count 驳回次数
 * @property int|null $set_hot_uid 设置热门用户uid
 * @property int|null $hot_time 进入热门的时间戳
 * @property int|null $is_commit 是否重新提交  1 是 0 否
 * @property int|null $rand_likes 后台随机新增点赞
 * @property int|null $rand_shares 后台随机新增分享
 * @property int|null $rand_collects 后台随机新增收藏
 * @property int|null $plate_id 圈子id
 * @property int|null $type 是否为公告  1 否  2 是
 * @property string|null $longitude 经度
 * @property string|null $latitude 维度
 * @property string|null $address 地理位置
 * @property string|null $last_reply_at 最后回复时间
 * @property int|null $sort 公告排序
 * @property int $vote_id 投票ID
 * @property string|null $at @关系
 * @property string|null $source 媒体资讯来源
 * @property int|null $topic_id 话题ID
 * @property int|null $auto_audit_status 0 默认值 1 自动过审核，2 需要复审
 * @property int|null $topic_source 1系统操作
 * @property int|null $is_hot_tag 是否在热榜tag展示 1展示 0不展示
 * @property int $rand_posts_num 后台评论[回复]总数
 * @property int $rand_clicks 后台点击
 * @property string|null $page_scope  默认为 0 单个不限制 批量 1 欧洲杯 2 推荐，3快讯，4讨论，9996圈子，9997足球赛事详情，9998篮球赛事详情，9999球队详情动态，10000球员详情动态
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereAdminUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereAuthorUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereAutoAuditStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereBestPostsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereBestPostsUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereCTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereClicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereCollects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereEditReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereEditTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereEditUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereFirstPostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereFirstPostedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereFirstPostedUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHeats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHotP1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHotP2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHotP3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHotTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereHotsTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsCommit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsHotTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsPrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLastPostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLastPostedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLastPostedUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLastReplyAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereOffineReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereOperTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereOperUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel wherePageScope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel wherePlateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel wherePostNumIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel wherePostsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRandClicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRandCollects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRandLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRandPostsNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRandShares($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRejectCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRejectReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereRemoveTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereSetHotUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereShares($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereTopicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereTopicSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsModel whereVoteId($value)
 * @mixin \Eloquent
 */
class DiscussionsModel extends BaseModel
{

    protected $connection = 'bbs';
    protected $table      = 'discussions';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
