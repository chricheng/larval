<?php


namespace App\Models\Bbs;


use App\Models\BaseModel;

/**
 * App\Models\Bbs\DiscussionsClicksModel
 *
 * @property int $discussions_id
 * @property int|null $expect_num 最终固定值
 * @property int|null $increase_num 每5分钟 增加得值
 * @property int|null $actual_num 实际增加值
 * @property int|null $plan_num 实际增加值
 * @property string|null $first_time 首次时间
 * @property string|null $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereActualNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereDiscussionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereExpectNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereFirstTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereIncreaseNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel whereUpdateTime($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionsClicksModel wherePlanNum($value)
 */
class DiscussionsClicksModel extends BaseModel
{
    protected $connection = 'bbs';
    protected $table      = 'discussions_clicks';
    protected $guarded    = [];

    protected $primaryKey = 'discussions_id';
    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
