<?php

namespace App\Models;


use App\Models\Passport\Expert\AuthorListModel;
use App\Models\Passport\Expert\AuthorNewsModel;
use App\Models\Passport\User\AccountModel;
use App\Models\Passport\User\BindUserModel;
use App\Models\Passport\User\RealPersonCertificationModel;

/**
 * App\Models\User
 *
 * @property int $uid 用户ID
 * @property string $mobile 绑定手机号
 * @property string|null $nickname
 * @property string|null $loginemail
 * @property int|null $usertype 用户类型1正常，2虚拟，3小黑侠
 * @property int $status 账户状态 1正常 其他非正常
 * @property string $avatar 头像地址
 * @property int $sex 性别1男2女0未知
 * @property string $password 密码串
 * @property string $solt 密码种子
 * @property string $regtime 注册时间
 * @property string $lastlogintime 最后登录时间
 * @property string $lastmodtime 最后修改时间
 * @property string $regip 注册ip
 * @property string $lastloginip 最后登录ip
 * @property string $src 注册来源
 * @property string|null $device_token 设备信息
 * @property string $addr 位置
 * @property string|null $area 区号
 * @property string $ext 扩展预留
 * @property string|null $tnickname 临时昵称
 * @property string|null $intro 个人简介
 * @property int $level 0:普通用户,1:大V
 * @property int|null $avatar_flag 头像修改标示
 * @property int|null $sign_days 连续签到天数
 * @property string|null $lastsigntime 最后签到时间
 * @property float|null $exp 经验值
 * @property int|null $label_id 用户标签ID
 * @property string|null $update_time
 * @property int|null $has_mod 修改个人信息标示0未修改，1已修改
 * @property-read AuthorNewsModel|null $apply
 * @property-read AuthorListModel|null $mp
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatarFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeviceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHasMod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLabelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastloginip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastlogintime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastmodtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastsigntime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLoginemail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSignDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSolt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTnickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsertype($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|BindUserModel[] $sns
 * @property-read int|null $sns_count
 * @property-read AccountModel|null $account
 * @property-read RealPersonCertificationModel|null $real
 * @property int|null $user_vip 用户会员标识 0 未开通 1小会员 2 大会员
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserVip($value)
 */
class User extends BaseModel
{
    protected $connection = 'passport';
    protected $table      = 'user';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    public function mp()
    {
        return $this->hasOne(AuthorListModel::class, 'uid', 'uid');
    }

    public function apply()
    {
        return $this->hasOne(AuthorNewsModel::class, 'uid', 'uid');
    }

    public function sns()
    {
        return $this->hasMany(BindUserModel::class, 'uid', 'uid');
    }

    public function account()
    {
        return $this->hasOne(AccountModel::class, 'uid', 'uid');
    }

    public function real()
    {
        return $this->hasOne(RealPersonCertificationModel::class, 'uid', 'uid');
    }
}
