<?php


namespace App\Models\QtData;


use App\Models\BaseModel;

/**
 * App\Models\QtData\OddsRunningModel
 *
 * @property int $id
 * @property int|null $schedule_id 赛事ID
 * @property string|null $happen_time 发生时间
 * @property int|null $home_score 主队得分
 * @property int|null $away_score 客队得分
 * @property int|null $home_red 主队红牌
 * @property int|null $away_red 客队红牌
 * @property int|null $type 类型
 * @property int|null $company_id 公司ID
 * @property float|null $odds1 赔率1
 * @property float|null $odds2 赔率2
 * @property float|null $odds3 赔率3
 * @property string|null $odds_time 变盘时间
 * @property string|null $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereAwayRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereAwayScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereHappenTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereHomeRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereHomeScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereOdds1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereOdds2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereOdds3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereOddsTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class OddsRunningModel extends BaseModel
{
    protected $connection = 'qt_data';
    protected $table      = 'odds_running';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

}
