<?php


namespace App\Models\QtData;


use App\Models\BaseModel;

/**
 * App\Models\QtData\FootballLeagueModel
 *
 * @property int $id
 * @property int|null $sclass_id 联赛ID
 * @property string|null $color 联赛颜色值
 * @property string|null $name_j 联赛名称简体
 * @property string|null $name_f 联赛名称繁体
 * @property string|null $name_e 联赛名称英文
 * @property string|null $name_js 联赛名称简体简称
 * @property string|null $name_fs 联赛名称繁体简称
 * @property string|null $name_es 联赛名称英文简称
 * @property string|null $name_s 联赛名称简称
 * @property int|null $kind 联赛类型1联赛，2杯赛
 * @property int|null $mode 比赛方式1分轮2分组
 * @property int|null $count_round 赛季轮数：仅对分轮的赛程有效
 * @property int|null $curr_round 正在进行的轮次
 * @property string|null $curr_match_season 当前赛季
 * @property string|null $sclass_pic 联赛的标志
 * @property int|null $if_stop 表示正在进行还是已经休赛
 * @property int|null $sclass_order 联赛排名，按重要性手工排名
 * @property int|null $sclass_type 联赛类型
 * @property int|null $count_group 组数
 * @property string|null $sclass_rule 赛制，所订的规则
 * @property string|null $sclass_rule_e 赛制，所订的规则英文
 * @property int|null $Bf_simply_disp
 * @property int|null $info_id 与SclassInfo的ID对应
 * @property string|null $info_url
 * @property string $update_time 最后更新时间
 * @property string|null $begin_season 开始赛季
 * @property int|null $sub_sclass_id
 * @property int|null $if_have_sub
 * @property string|null $subSclass 子联赛名
 * @property int|null $areaID 区域ID
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereAreaID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereBeginSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereBfSimplyDisp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereCountGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereCountRound($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereCurrMatchSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereCurrRound($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereIfHaveSub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereIfStop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereInfoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameE($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameFs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameJ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameJs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereNameS($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassPic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassRuleE($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSclassType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSubSclass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereSubSclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballLeagueModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class FootballLeagueModel extends BaseModel
{
    protected $connection = 'qt_data';
    protected $table      = 'sclass';
    protected $guarded    = [];

    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
