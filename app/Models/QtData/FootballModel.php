<?php


namespace App\Models\QtData;


use App\Models\BaseModel;

/**
 * App\Models\QtData\FootballModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel query()
 * @mixin \Eloquent
 * @property int $schedule_id 赛事id
 * @property int|null $sclass_id 联赛ID
 * @property string|null $match_season 赛季
 * @property string|null $round 分组轮次
 * @property string|null $grouping 分组的组号
 * @property int|null $home_team_id 主队编号
 * @property int|null $guest_team_id 客队编号
 * @property string|null $home_team 主队名称
 * @property string|null $guest_team 客队名称
 * @property int|null $neutrality 是否是中立场
 * @property string|null $match_time 比赛的时间
 * @property string|null $match_time2 下半场的开赛时间
 * @property string|null $location 比赛地点
 * @property string|null $home_order 主队排名（澳门）
 * @property string|null $guest_order 客队排名
 * @property int|null $match_state 比赛状态
 * @property int|null $weather_icon 赛场天气的图标编号
 * @property string|null $weather 赛场天气情况
 * @property string|null $temperature 赛场温度
 * @property string|null $tv 直播电视台
 * @property int|null $visitor 参观人数
 * @property int|null $home_score 主队入球数
 * @property int|null $guest_score 客队入球数
 * @property int|null $home_half_score 主队上半场入球数
 * @property int|null $guest_half_score 客队上班场入球数
 * @property string|null $explain 比分说明
 * @property int|null $home_red 主队红牌
 * @property int|null $guest_red 客队红牌
 * @property int|null $home_yellow 主队黄牌
 * @property int|null $guest_yellow 客队黄牌
 * @property int|null $home_corner 主队角球
 * @property int|null $guest_corner 客队角球
 * @property string|null $bf_changetime 比分的变化时间，刷新控制用
 * @property int|null $shangpan 1主队是上盘，2客队是上盘
 * @property int|null $isanaly 是否有分析
 * @property string|null $grouping2 杯赛组号
 * @property int|null $sub_sclass_id 联赛子ID
 * @property string|null $sub_sclass_name 子联赛名称
 * @property string|null $explain2 加时赛果说明
 * @property int|null $kind 赛事类型联赛，杯赛
 * @property int|null $battlearray
 * @property string|null $color 色值
 * @property int|null $level 赛事级别
 * @property int|null $is_cp 是否是彩票赛事
 * @property string|null $update_time 最后更新时间
 * @property string|null $hidden True为该场比赛从[当天即时比分接口]隐藏
 * @property string|null $time_compute 新的时间计算方式
 * @property int|null $group_id 杯赛分组ID
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereBattlearray($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereBfChangetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereExplain2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGrouping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGrouping2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestCorner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestHalfScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereGuestYellow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeCorner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeHalfScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereHomeYellow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereIsCp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereIsanaly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereKind($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereMatchSeason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereMatchState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereMatchTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereMatchTime2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereNeutrality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereRound($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereSclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereShangpan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereSubSclassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereSubSclassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereTimeCompute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereTv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereVisitor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereWeather($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereWeatherIcon($value)
 * @property string|null $create_time 创建时间
 * @property int|null $update_timestamp 数据最后更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FootballModel whereUpdateTimestamp($value)
 */
class FootballModel extends BaseModel
{
    protected $connection = 'qt_data';
    protected $table      = 'schedule';
    protected $guarded    = [];

    /**
     * 与表关联的主键。
     *
     * @var string
     */
    protected $primaryKey = 'schedule_id';

    /**
     * 指明模型的 ID 不是自增。
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;
}
