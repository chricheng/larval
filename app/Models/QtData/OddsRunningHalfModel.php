<?php


namespace App\Models\QtData;


use App\Models\BaseModel;

/**
 * App\Models\QtData\OddsRunningHalfModel
 *
 * @property int $id
 * @property int|null $schedule_id 赛事ID
 * @property string|null $happen_time 发生时间
 * @property int|null $home_score 主队得分
 * @property int|null $away_score 客队得分
 * @property int|null $home_red 主队红牌
 * @property int|null $away_red 客队红牌
 * @property int|null $type 类型
 * @property int|null $company_id 公司ID
 * @property float|null $odds1 赔率1
 * @property float|null $odds2 赔率2
 * @property float|null $odds3 赔率3
 * @property string|null $odds_time 变盘时间
 * @property string|null $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereAwayRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereAwayScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereHappenTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereHomeRed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereHomeScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereOdds1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereOdds2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereOdds3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereOddsTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OddsRunningHalfModel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class OddsRunningHalfModel extends BaseModel
{
    protected $connection = 'qt_data';
    protected $table      = 'odds_running_half';
    protected $guarded    = [];


    /**
     * 是否主动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

}
