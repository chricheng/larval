<?php

namespace App\Http\Requests\News;

use App\Http\Requests\BaseRequest;

class MatchBlockRequest extends BaseRequest
{
    public function rules()
    {
        return [
            "match_id"  => "required|numeric|min:1",
            "sport_id" => "required|numeric|min:1",
        ];
    }
}