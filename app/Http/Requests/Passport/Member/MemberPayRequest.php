<?php

namespace App\Http\Requests\Passport\Member;

use App\Http\Requests\BaseRequest;

class MemberPayRequest extends BaseRequest
{
    public function rules()
    {
        return [
            "data_type"  => "required|integer|in:31,32",
            "experience" => "required|in:0,1",
        ];
    }
}
