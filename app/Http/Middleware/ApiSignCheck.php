<?php

namespace App\Http\Middleware;

use App\Exceptions\ForbiddenException;
use Closure;
use Illuminate\Http\Request;

class ApiSignCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!app()->environment('test', 'local') && $request->header('JH-Debug', 0) != 1) {
            if (!$this->checkSign($request->all())) {
                throw new ForbiddenException();
            }
        }

        return $next($request);
    }

    private function checkSign($params)
    {
        if (data_get($params, 'hy_debug', 0) == 1) {
            return true;
        }

        $key = 'f31s5884553FSBN%@&DFQ255a12';

        if (!isset($params['client_sign'])) {
            return false;
        }

        if (!isset($params['client_time']) || (time() - $params['client_time']) > 20) {
            return false;
        }

        $client_sign = substr(md5('client_time='.$params['client_time'].'&key='.$key), 0, 10);
        if ($params['client_sign'] != $client_sign) {
            return false;
        }

        return true;
    }
}
