<?php

namespace App\Http\Middleware;

use App\Services\Tools\Login\LoginCookieService;
use Closure;
use Illuminate\Http\Request;

class ApiLoginCheck
{
    /**
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cookie = $this->getLoginCookie($request);
        if (empty($cookie)) {
            return failJsonResponse(-1, '未登录');
        }

        $uid = app(LoginCookieService::class)
            ->decrypt($cookie);

        if (empty($uid)) {
            return failJsonResponse(-1, '未登录');
        }

        return $next($request);
    }

    private function getLoginCookie(Request $request)
    {
        $cookie = $request->header('JH');
        if (empty($cookie)) {
            $cookie = $request->cookie('JH');
            if (empty($cookie)) {
                //ios游客
                $cookie = $request->cookie('JHIOSGUEST');
                if (empty($cookie)) {
                    return 0;
                }
            }
        }

        return $cookie;
    }
}
