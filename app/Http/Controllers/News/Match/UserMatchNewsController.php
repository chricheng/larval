<?php


namespace App\Http\Controllers\News\Match;


use App\Http\Controllers\Controller;
use App\Http\Requests\News\MatchBlockRequest;
use App\Services\News\Match\MatchUnblockLogService;
use App\Services\Tools\Common\ErrorSvc;

class UserMatchNewsController extends Controller
{
    #点击弹窗
    public function index()
    {
        $uid  =  $this->getLoginUid();
       
        $setUnBlockObj = new class {};
        $setUnBlockObj->uid     =   $uid;
       
        $result = app(MatchUnblockLogService::class)
             ->setArgObj($setUnBlockObj)
             ->main();
         
        return ajaxReturn(ErrorSvc::ERR_SUC,$result);
    }

    #点击解锁
    public function click(MatchBlockRequest $request)
    {
        $sportId  = $request->post('sport_id', 0);
        $matchId = $request->post('match_id', 0);
        $uid  =  $this->getLoginUid();
       
        $setUnBlockObj = new class {};
        $setUnBlockObj->sportId =   $sportId;
        $setUnBlockObj->uid     =   $uid;
        $setUnBlockObj->matchId =   $matchId;
       
        $result = app(MatchUnblockLogService::class)
             ->setArgObj($setUnBlockObj)
             ->doBlock();
         
        
        if(!$result) {
            return ajaxReturn(ErrorSvc::ERR_UNLOCK_COUNT);
        }    
        return ajaxReturn(ErrorSvc::ERR_SUC);
    } 

}
