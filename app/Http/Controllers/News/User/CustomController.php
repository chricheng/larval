<?php

namespace App\Http\Controllers\News\User;

use App\Exceptions\ParamsException;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Services\News\Banner\BannerService;


class CustomController extends Controller
{
    // 各用户自定义独有信息获取
    public function index(BaseRequest $request)
    {
        $this->valid([
            'src'  => 'required|string',
            'channel' => 'required|string',
            'v' => 'required|integer'
        ]);

        $uid = $this->getLoginUid();

        $device  = $request->get('src');
        $version = $request->get('v');
        $channel = $request->get('channel');
        // 针对安卓参数跟ios不一致处理
        if($device != "ios"){
            $device = "android";
        }

        $data = app(BannerService::class)
            ->setUid(intval($uid))
            ->setDevice($device)
            ->setVersion($version)
            ->setChannel($channel)
            ->main();

        return successJsonResponse($data);
    }
}

