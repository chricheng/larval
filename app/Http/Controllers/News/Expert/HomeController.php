<?php


namespace App\Http\Controllers\News\Expert;


use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Services\News\Expert\ExpertHomeStatisticsService;


class HomeController extends Controller
{
    public function statistics(BaseRequest $request)
    {
        $resInfo = app(ExpertHomeStatisticsService::class)
            ->setUid($request->get('uid', 0))
            ->setSportId($request->get('sport_id', 0))
            ->main();

        return myJsonResCache($resInfo, 30);
    }
}
