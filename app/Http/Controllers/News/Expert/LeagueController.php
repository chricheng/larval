<?php


namespace App\Http\Controllers\News\Expert;


use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Models\Cms\Expert\ExpertLeagueTagModel;
use App\Models\Cms\Expert\ExpertModel;
use App\Models\Cms\NewsModel;
use App\Services\News\Common\ExpertBaseInfoService;
use App\Services\News\Expert\ExpertLeagueGoodAtService;
use Illuminate\Database\Eloquent\Builder;

class LeagueController extends Controller
{
    public function userInfo(BaseRequest $request)
    {
        $expertId = $request->get('uid', 0);
        $sportId  = $request->get('sport_id', 0);
        $leagueId = $request->get('league_id', 0);


        $redisKey = "expert_league_info_v2:{$expertId}:{$sportId}:{$leagueId}";

        $resInfo = \Cache::remember($redisKey, now()->addMinutes(2), function () use ($expertId, $sportId, $leagueId) {

            $expertModel = ExpertModel::query()->where('uid', $expertId)->first();

            $resInfo = app(ExpertBaseInfoService::class)
                ->setExpertModel($expertModel)
                ->main();

            $tagModel = ExpertLeagueTagModel::query()
                ->where('uid', $expertId)
                ->where('sport_id', $sportId)
                ->where('league_id', $leagueId)
                ->first();

            if (empty($tagModel)) {
                return $resInfo;
            }

            $resInfo['league_tag'] = [
                'league_id'           => $tagModel->league_id,
                'league_name'         => $tagModel->league_name,
                'hit_rate_desc'       => $tagModel->hit_rate_desc,
                'hit_rate_y'          => $tagModel->hit_rate_y,
                'hit_rate_x'          => $tagModel->hit_rate_x,
                'hit_high_num'        => $tagModel->hit_high_num,
                'max_high_num'        => $tagModel->max_high_num,
                'total_order_num'     => $tagModel->total_order_num,
                'total_hit_rate'      => $tagModel->total_hit_rate,
                'total_hit_rate_desc' => $tagModel->total_hit_rate_desc,
            ];

            //近10场
            $resInfo['hit_last_10'] = NewsModel::query()
                ->where('authorid', $expertId)
                ->where('predict_status', 1)
                ->where('sport_id', $sportId)
                ->where('league_id', $leagueId)
                ->orderByDesc('firsttime')
                ->limit(10)
                ->pluck('isWin')
                ->toArray();

            return $resInfo;
        });

        return myJsonResCache($resInfo);
    }

    public function goodAt(BaseRequest $request)
    {
        $resInfo = app(ExpertLeagueGoodAtService::class)
            ->setExpertId($request->get('uid', 0))
            ->main();

        return myJsonResCache($resInfo, 60);
    }
}
