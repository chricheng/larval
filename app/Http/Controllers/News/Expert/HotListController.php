<?php


namespace App\Http\Controllers\News\Expert;


use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Services\News\Expert\ExpertHotListService;
use App\Services\News\Expert\ExpertRankService;

class HotListController extends Controller
{
    public function index(BaseRequest $request)
    {
        $resInfo = app(ExpertHotListService::class)
            ->setSportId($request->get('sport_id', 0))
            ->main();

        return myJsonResCache($resInfo, 60);
    }

    public function rank(BaseRequest $request)
    {
        $resInfo = app(ExpertRankService::class)
            ->setSportId($request->get('sport_id', 0))
            ->setType($request->get('type', ''))
            ->setSubtype($request->get('subtype', ''))
            ->setLoginUid($this->getLoginUid())
            ->main();

        return myJsonResCache($resInfo, 60);
    }
}
