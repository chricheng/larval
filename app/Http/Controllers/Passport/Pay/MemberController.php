<?php


namespace App\Http\Controllers\Passport\Pay;


use App\Http\Controllers\Controller;
use App\Http\Requests\Passport\Member\MemberPayRequest;
use App\Services\Passport\Member\MemberHomeService;
use App\Services\Passport\Member\MemberPayService;
use App\Services\Passport\Member\MemberRightsService;

class MemberController extends Controller
{
    public function home()
    {
        $resInfo = app(MemberHomeService::class)
            ->setLoginUid($this->getLoginUid())
            ->main();

        return successJsonResponse($resInfo);
    }

    public function rights(MemberPayRequest $request)
    {
        $resInfo = app(MemberRightsService::class)
            ->setLoginUid($this->getLoginUid())
            ->setDataType($request->get('data_type', 0))
            ->setExperience($request->get('experience', 1))
            ->main();

        return myJsonResCache($resInfo);
    }

    public function buy(MemberPayRequest $request)
    {
        $resInfo = app(MemberPayService::class)
            ->setLoginUid($this->getLoginUid())
            ->setDataType($request->get('data_type', 0))
            ->setExperience($request->get('experience', 1))
            ->main();

        return myJsonResCache($resInfo);
    }
}
