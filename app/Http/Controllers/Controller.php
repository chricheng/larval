<?php

namespace App\Http\Controllers;

use App\Exceptions\ParamsException;
use App\Models\User;
use App\Services\Tools\Login\LoginCookieService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getLoginUser()
    {
        $uid = $this->getLoginUid();

        if (empty($uid)) {
            return null;
        }

        return User::findOrFail($uid);
    }

    protected function getLoginUid()
    {
        //后续使用此token
        $cookie = \Request::header('JH');
        if (empty($cookie) || $cookie == 'undefined') {
            $cookie = \Request::cookie('JH');
            if (empty($cookie) || $cookie == 'undefined') {
                //ios游客
                $cookie = \Request::cookie('JHIOSGUEST');
                if (empty($cookie) || $cookie == 'undefined') {
                    return 0;
                }
            }
        }

        return app(LoginCookieService::class)
            ->decrypt($cookie);
    }

    protected function valid($rules)
    {
        $validate = \Validator::make(\Request::all(), $rules);
        if ($validate->fails()) {
            throw new ParamsException();
        }
    }
}
