<?php


namespace App\Services\Push;


use App\Models\PushData\AndDeviceUserModel;
use App\Models\PushData\AndroidUserDeviceFollowMatch;
use App\Models\PushData\AndroidUserDeviceFollowMatchHot;
use App\Services\Tools\Common\CommonSetterService;


/**
 * 清除安卓设备信息
 * Class AndDeviceUserService
 * @package App\Services\push\
 * @method $this setUidArr($uidArr)
 */
class AndDeviceUserService
{
    use CommonSetterService;

    /**
     * @var array
     */
    protected $uidArr = [];

    //弹窗显示
    public function main()
    {
        return $this->delUserDevicePush();

    }

    /**
     * 清除推送设备 已经 token 和推送赛事信息
     * @return bool
     */
    private  function delUserDevicePush()
    {
        $deviceNo  =   $this->getUserDeviceNo();
        $this->delUserDeviceId();
        if(empty($deviceNo)){
            $this->delUserToken($deviceNo); //清除token
            $this->delUserDeviceFollowMatch($deviceNo);  //清除关注赛事
            $this->delUserDeviceFollowMatchHot($deviceNo);  //清除热门赛事
        }

        return  true;
    }

    /**
     * 清除用户设备关系
     * @return bool
     */
    private  function delUserDeviceId()
    {
        if(empty($this->uidArr)){
            return  false;
        }

        AndDeviceUserModel::query()
            ->whereIn('uid', $this->uidArr)
            ->delete();
         return  true;
    }
    /**
     * 清除用户推送信息
     * @return bool
     */
    private  function delUserToken($deviceNo = [])
    {
        if(empty($deviceNo)){
            return  false;
        }

        AndDeviceUserModel::query()
            ->whereIn('DeviceNo', $deviceNo)
            ->delete();
        return  true;
    }

    /**
     * 清除用户推送信息
     * @return bool
     */
    private  function delUserDeviceFollowMatch($deviceNo = [])
    {
        if(empty($deviceNo)){
           return  false;
        }
        AndroidUserDeviceFollowMatch::query()
            ->whereIn('DeviceNo', $deviceNo)
            ->delete();
        return  true;
    }

    /**
     * 清除用户关注赛事
     * @return bool
     */
    private  function delUserDeviceFollowMatchHot($deviceNo = [])
    {
        if(empty($deviceNo)){
           return  false;
        }

        AndroidUserDeviceFollowMatchHot::query()
            ->whereIn('DeviceNo', $deviceNo)
            ->delete();
        return  true;
    }

    /**
     * 获取设备号
     * @return array
     */
    private  function getUserDeviceNo(){
        if(empty($this->uidArr)){
            return  [];
        }
        $deviceNos =  AndDeviceUserModel::query()
            ->whereIn('uid', $this->uidArr)
            ->pluck("DeviceNo")
            ->toArray();

        return  $deviceNos;
    }
}
