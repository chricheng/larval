<?php


namespace App\Services\Passport\User;

use App\Jobs\push\DeleteAndroidUser;
use App\Models\User;
use App\Services\Tools\Common\CommonSetterService;

/**
 * 获取用户不活跃信息
 * Class UserUnActiveService
 */
class UserUnActiveService
{
    use CommonSetterService;


    public function main(){
        $this->handleUser();
    }

    /**
     * 处理任务分发
     * @return int;
     */
    public  function handleUser(){
        User::query()
            ->select(['uid'])
            ->where('lastlogintime', '<', now()->subDays(35))
            ->orderByDesc('lastlogintime')
            ->chunk(1000,function($itemsTmp){
                $items = $itemsTmp->toArray();
                $uidArr  =  array_column($items,'uid');
                if(!empty($uidArr)) {
                    DeleteAndroidUser::dispatch($uidArr)->onQueue("user-no-active-info");
                }
            });
        return 0;
    }
}
