<?php


namespace App\Services\Passport\User;

use App\Models\Passport\User\UserFriendModel;
use App\Models\User;
use App\Services\Tools\Common\CommonSetterService;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * 获取用户信息
 * 注意：当uid时一个时返回一维数组
 * Class UserBaseInfoService
 * @package App\Services\Passport\User
 * @method $this setUidArr($uidArr)
 */
class UserFullInfoService
{
    use CommonSetterService;

    /**
     * @var array
     */
    protected $uidArr = [];

    protected $relations = [
        'mp',
        'apply',
        'real',
        'account',
        'sns'
    ];

    protected $keyPrefix = 'user_full_info:uid:';

    final public function main()
    {
        if (!is_array($this->uidArr)) {
            $this->uidArr = [$this->uidArr];
        }
        $this->uidArr = array_filter($this->uidArr);
        if (empty($this->uidArr)) {
            return [];
        }

        $keys = [];

        foreach ($this->uidArr as $uid) {
            $keys[] = "{$this->keyPrefix}{$uid}";
        }

        //缓存里面所有数据
        $tmpArr = $this->getMultiple();
        //命中的数据
        $yesArr = array_filter($tmpArr);

        //未命中的数据
        $noArr = array_diff(array_keys($tmpArr), array_keys($yesArr));

        if (!empty($noArr)) {
            $totalArr = array_merge($yesArr, $this->setMultiple($noArr));
        } else {
            $totalArr = $yesArr;
        }

        $resInfo = [];

        foreach ($totalArr as $key => $item) {
            $resInfo[str_replace($this->keyPrefix, '', $key)] = $item;
        }

        if (empty($resInfo)) {
            return [];
        }

        return count($resInfo) > 1 ? $resInfo : \Arr::first($resInfo);
    }

    private function setMultiple($noArr)
    {
        $noArr = array_map(function ($item) {
            return str_replace($this->keyPrefix, '', $item);
        }, $noArr);

        $data = $this->handleNotCacheUser($noArr);

        try {
            \Cache::setMultiple($data, now()->addMinutes(5));
        } catch (InvalidArgumentException $e) {

        }

        return $data;
    }


    private function getMultiple()
    {
        $keys = [];

        foreach ($this->uidArr as $uid) {
            $keys[] = "{$this->keyPrefix}{$uid}";
        }

        try {
            $tmpArr = \Cache::getMultiple($keys);
        } catch (InvalidArgumentException $e) {
            $tmpArr = [];
        }

        return $tmpArr;

    }

    private function handleNotCacheUser($notUidArr)
    {
        $userModels = User::query()
            ->with($this->relations)
            ->whereIn('uid', $notUidArr)
            ->get();

        $resInfo = [];

        foreach ($userModels as $model) {
            $resInfo[$this->keyPrefix.$model->uid] = $this->handleEachModel($model);
        }

        return $resInfo;
    }

    protected function handleEachModel(User $user)
    {
        $this->handleBaseInfo($user, $resInfo);
        $this->handleNickName($user, $resInfo);
        $this->handleNewsStatus($user, $resInfo);
        $this->handleMpInfo($user, $resInfo);
        $this->handlePointRank($user, $resInfo);
        $this->handleSNSInfo($user, $resInfo);
        $this->handleAccountInfo($user, $resInfo);
        $this->handleRealPersonCrtStatus($user, $resInfo);
        $this->handleFollowInfo($user, $resInfo);

        return $resInfo;
    }

    protected function handleFollowInfo(User $user, &$resInfo)
    {
        $resInfo['followerNum'] = UserFriendModel::query()
            ->where('uid', $user->uid)
            ->where('status', 1)
            ->count();

        $resInfo['followedNum'] = UserFriendModel::query()
            ->where('followuid', $user->uid)
            ->where('status', 1)
            ->count();
    }

    protected function handleBaseInfo(User $user, &$resInfo)
    {
        $resInfo = [
            'uid'      => $user->uid,
            'status'   => $user->status,
            'nickname' => $user->nickname,
            'avatar'   => formatAvatarUrl($user->avatar),
            'sex'      => $user->sex,
            'mobile'   => $user->mobile,
            'intro'    => $user->intro,
            'level'    => $user->level,

            'has_mod'       => $user->has_mod,
            'lastmodtime'   => $user->lastmodtime,
            'regtime'       => $user->regtime,
            'regip'         => $user->regip,
            'lastlogintime' => $user->lastlogintime,
            'lastloginip'   => $user->lastloginip,
            'src'           => $user->src,
            'exp_level'     => $user->status == -99 ? 0 : $this->getExpLevel($user->exp),
            'exp'           => $user->exp,
            'label_id'      => $user->label_id,
            'usertype'      => $user->usertype,
            'set_nickname'  => $user->nickname == $user->uid ? '2' : '1',
            'ext'           => $user->ext
        ];
    }

    /**
     * 获取用户认证状态
     * @param  User  $user
     * @param $resInfo
     */
    protected function handleRealPersonCrtStatus(User $user, &$resInfo)
    {
        $resInfo['real_person_status'] = data_get($user->real, 'status', -1);
    }

    /**
     * 处理账号信息
     * @param  User  $user
     * @param $resInfo
     */
    protected function handleAccountInfo(User $user, &$resInfo)
    {
        $resInfo['pay_level'] = data_get($user->account, 'pay_money', 0) > 0 ? 1 : 0;
    }

    /**
     * 处理第三方绑定表
     * @param  User  $user
     * @param $resInfo
     */
    protected function handleSNSInfo(User $user, &$resInfo)
    {
        foreach ($user->sns as $model) {

            $resInfo[$model->bindappsrc] = [
                'bindtime' => $model->bindtime,
                'nickname' => $model->nickname,
                'avatar'   => $model->avatar,
            ];
        }
    }

    protected function handlePointRank(User $user, &$resInfo)
    {
        if ($resInfo['mp'] == -1) {
            $resInfo['mp_rank']   = '';
            $resInfo['mp_points'] = 0;
            return;
        }

        if ($resInfo['mp'] != 1) {
            return;
        }

        $points = data_get($user->mp, 'points', 0);

        if ($points >= 200) {

            $fansNum = UserFriendModel::query()
                ->where('uid', $user->uid)
                ->where('status', 1)
                ->count();

            $points += $fansNum;
        }

        if ($points < 0) {
            $rank = '-1';
        } elseif ($points < 200) {
            $rank = '1';
        } elseif ($points < 1000) {
            $rank = '2';
        } else {
            $rank = '3';
        }

        $resInfo['mp_rank']   = $rank;
        $resInfo['mp_points'] = $points;
    }

    protected function handleMpInfo(User $user, &$resInfo)
    {
        if (empty($user->mp)) {
            $resInfo['mp']            = -1;
            $resInfo['anchor_status'] = -1;
            $resInfo['points']        = 0;
            $resInfo['mp_memo']       = '';
            $resInfo['author_source'] = '';

            return;
        }

        $resInfo['author_source'] = $user->mp->author_source;

        if ($user->mp->pass_flag == 0 && $user->mp->check_phone == 0) {
            $resInfo['mp'] = -1;
        } else {
            $resInfo['mp'] = $user->mp->pass_flag;
        }

        if ($user->mp->anchor_status == 0 && $user->mp->check_phone == 0) {
            $resInfo['anchor_status'] = -1;
        } else {
            $resInfo['anchor_status'] = $user->mp->anchor_status;
        }
        $resInfo['mp_memo'] = config("mp.status_memo.{$resInfo['mp']}", '');

    }

    protected function handleNewsStatus(User $user, &$resInfo)
    {
        $newsStatus = data_get($user->apply, 'status', -1);

        if ($newsStatus == -1) {
            $tmpTime    = data_get($user->mp, 'create_time', now()->toDateTimeString());
            $newsStatus = $tmpTime <= '2019-03-29 00:00:00' ? 3 : -1;
        }

        $resInfo['news_status'] = $newsStatus;
    }


    protected function getExpLevel($exp)
    {
        if ($exp >= 0 && $exp <= 9) {
            $exp_level = 0;
        } elseif ($exp >= 10 && $exp < 50) {
            $exp_level = 1;
        } elseif ($exp >= 50 && $exp < 100) {
            $exp_level = 2;
        } elseif ($exp >= 100 && $exp < 300) {
            $exp_level = 3;
        } elseif ($exp >= 300 && $exp < 500) {
            $exp_level = 4;
        } elseif ($exp >= 500 && $exp < 1000) {
            $exp_level = 5;
        } elseif ($exp >= 1000 && $exp < 2000) {
            $exp_level = 6;
        } elseif ($exp >= 2000 && $exp < 5000) {
            $exp_level = 7;
        } elseif ($exp >= 5000 && $exp < 10000) {
            $exp_level = 8;
        } elseif ($exp >= 10000 && $exp < 20000) {
            $exp_level = 9;
        } elseif ($exp >= 20000 && $exp < 50000) {
            $exp_level = 10;
        } elseif ($exp >= 50000 && $exp < 100000) {
            $exp_level = 11;
        } elseif ($exp >= 100000 && $exp < 200000) {
            $exp_level = 12;
        } elseif ($exp >= 200000 && $exp < 500000) {
            $exp_level = 13;
        } elseif ($exp >= 500000 && $exp < 1000000) {
            $exp_level = 14;
        } elseif ($exp >= 1000000 && $exp < 2000000) {
            $exp_level = 15;
        } elseif ($exp >= 2000000 && $exp < 5000000) {
            $exp_level = 16;
        } elseif ($exp >= 5000000 && $exp < 10000000) {
            $exp_level = 17;
        } elseif ($exp >= 10000000 && $exp < 20000000) {
            $exp_level = 18;
        } elseif ($exp >= 20000000) {
            $exp_level = 19;
        } else {
            $exp_level = 0;
        }

        return $exp_level;
    }

    protected function handleNickName(User $user, &$resInfo)
    {
        if (is_numeric($user->nickname)) {
            $resInfo['nickname'] = str_replace(['官方', 'jihaibifen', 'jihai8', 'jihai8.com', '嗨球', '秒嗨', '嗨娱'], '*',
                $user->tnickname);
            return;
        }

        if ($user->usertype == 3) {
            if ($user->ext == 'lock_intro_only') {
                $resInfo['nickname'] = '此用户违反相关规定，已被关进小黑屋！';

            } else {

                $resInfo['nickname'] = formatHiddenName($resInfo['nickname'], 1);
                $resInfo['avatar']   = 'https://img1.jihai8.com/default1.png';
                $resInfo['intro']    = '此用户违反相关规定，已被关进小黑屋！';
            }
        }
        if ($user->status == -99) {
            $resInfo['nickname'] = '已注销';
            $resInfo['avatar']   = 'https://img1.jihai8.com/default1.png';
            $resInfo['intro']    = '账号已注销，如有疑问可联系APP内在线客服';
        }
    }

}
