<?php


namespace App\Services\Passport\User;

use App\Models\User;

/**
 * 获取用户基本信息
 * Class UserBaseInfoService
 * @package App\Services\Passport\User
 */
class UserBaseInfoService extends UserFullInfoService
{
    /**
     * @var array
     */
    protected $relations = [
        'mp'
    ];

    protected $keyPrefix = 'user_base_info_v3:uid:';

    protected function handleEachModel(User $user)
    {
        $resInfo = [];

        $this->handleBaseInfo($user, $resInfo);
        $this->handleNickName($user, $resInfo);
        $this->handleMpInfo($user, $resInfo);
        $this->handlePointRank($user, $resInfo);
        $this->handleFollowInfo($user, $resInfo);

        return $resInfo;
    }

    protected function handleBaseInfo(User $user, &$resInfo)
    {
        $resInfo = [
            'uid'          => $user->uid,
            'status'       => $user->status,
            'nickname'     => $user->nickname,
            'avatar'       => formatAvatarUrl($user->avatar),
            'intro'        => $user->intro,
            'level'        => $user->level,
            'exp_level'    => $user->status == -99 ? 0 : $this->getExpLevel($user->exp),
            'exp'          => $user->exp,
            'label_id'     => $user->label_id,
            'usertype'     => $user->usertype,
            'set_nickname' => $user->nickname == $user->uid ? '2' : '1',
            'regtime'      => $user->regtime,
        ];
    }
}
