<?php


namespace App\Services\Passport\User;

use App\Models\Passport\User\UserOrdersRecord;
use App\Models\User;
use App\Services\Tools\Common\CommonSetterService;

/**
 * 获取用户统计数据
 * Class UserOrderStaticService
 * @method $this setRecodeArr($arr)
 */
class UserOrderStaticService
{
    use CommonSetterService;

    protected   $recodeArr;

    public function  main(){
        $this->addUserOrderRecord();
    }

    #处理用户记录同步信息
    public function addUserOrderRecord()
    {
        if(empty($this->recodeArr)){
            return false;
        }

        $userOrdersRecordModel = UserOrdersRecord::query()
            ->whereUid($this->recodeArr['uid'])
            ->first();

        if (empty($userOrdersRecordModel)) {

            $userOrdersRecordModel = new UserOrdersRecord();

            $userOrdersRecordModel->uid = $this->recodeArr['uid'];
            $userOrdersRecordModel->reg_time = $this->recodeArr['regtime'];
            $userOrdersRecordModel->frist_order_time = $this->recodeArr['first_pay_time'];
        }

        $userOrdersRecordModel->last_order_time = $this->recodeArr['last_pay_time'];
        $userOrdersRecordModel->save();
    }

}
