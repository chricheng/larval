<?php


namespace App\Services\Passport\Member;


use App\Exceptions\ApiException;
use App\Exceptions\ForbiddenException;
use App\Models\Passport\Pay\PayDataModel;
use App\Services\Passport\User\UserBaseInfoService;

class MemberPayService extends MemberBaseService
{
    public function main()
    {
        $this->initParams();
        $this->handleExperienceData();
    }

    private function handleExperienceData()
    {
        PayDataModel::query()
            ->create([
                'uid'           => $this->loginUid,
                'data_type'     => self::SMALL_DATA_TYPE,
                'pay_time'      => now(),
                'trade_no'      => $this->getTradeNo(),
                'status'        => 3,
                'src'           => \Request::get('src', ''),
                'start_time'    => now(),
                'end_time'      => now()->addDays(3),
                'is_experience' => 1,
                'expect_num'    => 1,
                'ext'           => '新用户专享',
            ]);
    }

    private function initParams()
    {
        //目前仅支持体验会员购买
        if (!$this->experience) {
            throw new ApiException('系统异常');
        }

        $userInfo = app(UserBaseInfoService::class)
            ->setUidArr($this->loginUid)
            ->main();

        $regTime = data_get($userInfo, 'regtime', '2020-01-01 00:00:00');

        if (!$this->checkExperience($regTime)) {
            throw new ForbiddenException('不满足新用户专享条件');
        }
    }
}

