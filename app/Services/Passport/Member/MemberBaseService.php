<?php


namespace App\Services\Passport\Member;


use App\Models\Passport\Pay\PayDataModel;
use App\Services\Tools\Common\CommonSetterService;


/**
 * Class MemberBaseService
 * @package App\Services\Passport\Member
 * @method $this setLoginUid($loginUid)
 * @method $this setExperience($experience)
 * @method $this setDataType($dataType)
 * @method $this setId($id)
 */
class MemberBaseService
{

    use CommonSetterService;

    /**
     * @var int
     */
    protected $loginUid = 0;

    const BIG_DATA_TYPE   = 31;
    const SMALL_DATA_TYPE = 32;

    /**
     *注册起始日期
     */
    const REG_START_TIME = '2022-01-01 00:00:00';

    const EXPERIENCE_INFO = [
        'cooperate_type' => 1,
        'data_name'      => '体验会员',
        'data_type'      => self::SMALL_DATA_TYPE,
        'id'             => 0,
        'money'          => 0,
        'number'         => 3,
        'old_money'      => 66,
        'type'           => 'day',
        'type_name'      => '天',
        'experience'     => 1,
    ];

    /**
     * 是否为体验会员
     * @var int
     */
    protected $experience = 1;

    /**
     * @var int
     */
    protected $dataType = 0;

    protected $id = 0;

    /**
     * 判断是否有体验资格
     * @param $regTime
     * @return bool
     */
    protected function checkExperience($regTime)
    {
        if (strtotime($regTime) < strtotime(self::REG_START_TIME)) {
            return false;
        }

        $num = PayDataModel::query()
            ->where('uid', $this->loginUid)
            ->whereIn('data_type', [self::SMALL_DATA_TYPE, self::BIG_DATA_TYPE])
            ->where('status', 3)
            ->count();

        return $num == 0;
    }


    /**
     * 获取大小会员有效期
     * @return array
     */
    protected function getMemberEndTime()
    {
        return PayDataModel::query()
            ->where('uid', $this->loginUid)
            ->whereIn('data_type', [self::SMALL_DATA_TYPE, self::BIG_DATA_TYPE])
            ->where('status', 3)
            ->where('end_time', '>', now())
            ->orderByDesc('id')
            ->selectRaw('data_type,max(end_time) end_time')
            ->groupBy('data_type')
            ->pluck('end_time', 'data_type')
            ->toArray();
    }

    protected function getTradeNo()
    {
        return 'D' . date('YmdHis') . rand(100000, 999999);
    }
}
