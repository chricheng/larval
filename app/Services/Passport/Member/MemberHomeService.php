<?php


namespace App\Services\Passport\Member;

use App\Models\Passport\Pay\PayCategoryModel;
use App\Models\Passport\User\AccountModel;
use App\Services\Passport\User\UserBaseInfoService;
use Carbon\Carbon;

/**
 * Class MemberHomeService
 * @package App\Services\Passport\Member
 */
class MemberHomeService extends MemberBaseService
{
    public function main()
    {
        $isAppStore = isIosDevice();
        $redisKey   = "member_home_v1:{$isAppStore}";

        $resInfo = \Cache::remember($redisKey, now()->addMinutes(5), function () use ($isAppStore) {
            return $this->handleCacheData($isAppStore);
        });

        if (!empty($this->loginUid)) {
            //用户信息
            $resInfo['user_info'] = app(UserBaseInfoService::class)
                ->setUidArr($this->loginUid)
                ->main();
        }

        $this->handleExperiencePrice($resInfo);
        $this->handleMemberInfo($resInfo);
        $this->handleWalletInfo($resInfo);

        //会员信息
        return $resInfo;
    }


    #处理用户购买
    public function getUserBuyStatus($uid)
    {
        if (empty($uid)) {
            return [];
        }
        $resInfo['can_experience'] = 1; #有资格
        //用户信息
        $resInfo['user_info'] = app(UserBaseInfoService::class)
            ->setUidArr($uid)
            ->main();

        $regTime = data_get($resInfo, 'user_info.regtime', self::REG_START_TIME);

        if (!$this->checkExperience($regTime)) {
            $resInfo['can_experience'] = 0;
        }

        return $resInfo;
    }

    private function handleWalletInfo(&$resInfo)
    {
        $accountModel = AccountModel::query()
            ->where('uid', $this->loginUid)
            ->first();

        if (empty($accountModel)) {
            $resInfo['wallet_info'] = [
                'uid'     => $this->loginUid,
                'balance' => 0,
            ];
        } else {
            $resInfo['wallet_info'] = [
                'uid'     => $accountModel->uid,
                'balance' => $accountModel->balance,
            ];
        }
    }

    /**
     * 会员信息
     * @param $resInfo
     */
    private function handleMemberInfo(&$resInfo)
    {
        $memberInfo['can_experience'] = 0;
        $memberInfo['big_active']     = 0;
        $memberInfo['big_end_time']   = '';
        $memberInfo['big_text']       = '';
        $memberInfo['small_active']   = 0;
        $memberInfo['small_end_time'] = '';
        $memberInfo['small_text']     = '';

        if (empty($this->loginUid)) {
            $memberInfo['can_experience'] = 1;

            $resInfo['member_info'] = $memberInfo;
            return;
        }

        $regTime = data_get($resInfo, 'user_info.regtime', '2022-02-01 00:00:00');

        if ($this->checkExperience($regTime)) {
            $memberInfo['can_experience'] = 1;

            $resInfo['member_info'] = $memberInfo;
            return;
        }

        $endTimeArr = $this->getMemberEndTime();

        foreach ($endTimeArr as $key => $endTime) {

            if ($key == self::BIG_DATA_TYPE) {
                //
                $memberInfo['big_active']   = 1;
                $memberInfo['big_end_time'] = $endTime;
                $memberInfo['big_text']     = "大会员".$this->getMemberTimeInfo($endTime);
            } else {
                $memberInfo['small_active']   = 1;
                $memberInfo['small_end_time'] = $endTime;
                $memberInfo['small_text']     = "小会员".$this->getMemberTimeInfo($endTime);
            }
        }

        $resInfo['member_info'] = $memberInfo;
    }

    private function getMemberTimeInfo($endTime)
    {
        $days = Carbon::parse($endTime)->diffInDays(now());
        //大会员12-12-12（剩余7天）到期，续费后有效期顺延
        $text = Carbon::parse($endTime)->toDateString();
        if ($days <= 7) {
            $text .= "（剩余{$days}天）";
        }

        $text .= '到期，续费后有效期顺延';

        return $text;
    }

    /**
     * 添加体验信息
     * @param $resInfo
     */
    private function handleExperiencePrice(&$resInfo)
    {
        $regTime = data_get($resInfo, 'user_info.regtime', '2020-01-01 00:00:00');

        if (!empty($this->loginUid) && !$this->checkExperience($regTime)) {
            return;
        }

        array_unshift($resInfo['small_info'], self::EXPERIENCE_INFO);
    }

    private function handleCacheData($isAppStore)
    {
        $resInfo['big_info']  = $this->handlePriceList(self::BIG_DATA_TYPE, $isAppStore);
        $resInfo['big_cheap'] = '974';

        $resInfo['small_info']  = $this->handlePriceList(self::SMALL_DATA_TYPE, $isAppStore);
        $resInfo['small_cheap'] = '476';

        return $resInfo;
    }

    /**
     * 处理价格列表
     * @param $dateType
     * @param $isAppStore
     * @return array
     */
    private function handlePriceList($dateType, $isAppStore)
    {
        $categoryModels = PayCategoryModel::query()
            ->where('data_type', $dateType)
            ->where('cooperate_type', 1)
            ->where('is_auto', 0)
            ->where('status', $isAppStore)
            ->orderBy('sort')
            ->get();

        $resInfo = [];

        foreach ($categoryModels as $model) {

            $resInfo[] = [
                'cooperate_type' => $model->cooperate_type,
                'data_type'      => $model->data_type,
                'id'             => $model->id,
                'money'          => $model->money,
                'number'         => $model->number,
                'old_money'      => $model->old_money,
                'type'           => $model->type,
                'type_name'      => $this->getTypeName($model->type),
                'experience'     => 0,
            ];
        }

        return $resInfo;

    }

    private function getTypeName($type)
    {
        switch ($type) {
            case 'day'  :
                $title = '天';
                break;
            case 'month':
                $title = '个月';
                break;
            case 'year' :
                $title = '年';
                break;
            default:
                $title = '';
                break;
        }
        return $title;
    }
}
