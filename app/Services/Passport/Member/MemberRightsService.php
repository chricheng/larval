<?php


namespace App\Services\Passport\Member;

/**
 * Class MemberRightsService
 * @package App\Services\Passport\Member
 */
class MemberRightsService extends MemberBaseService
{
    /**
     * 是否为体验会员
     * @var int
     */
    protected $experience = 1;

    /**
     * @var int
     */
    protected $dataType = 0;

    protected $id = 0;


    public function main()
    {
        $this->initParams();

        $resInfo = [
            'title' => '会员权益',
        ];

        $list = [];

        $this->handleForecastStatistics($list);
        $this->handleFreeForecast($list);
        $this->handleRedPacket($list);
        $this->handleBigDataReport($list);
        $this->handleDataRadar($list);
        $this->handleRenamingCard($list);

        $resInfo['list'] = $list;

        return $resInfo;
    }

    /**
     * 免费查看赛事预测分布
     * @param
     *
     */
    private function handleForecastStatistics(&$resInfo)
    {
        $resInfo[] = [
            'title'     => '免费查看赛事预测分布',
            'desc'      => '专家预测分布清晰明了，方便直观了解专家推荐走向',
            'img'       => getImgOssUrl('/op/member/forecast_statistics.png'),
            'button'    => '去查看',
            'price'     => '',
            'jump_info' => appJumpInfo(531),
        ];
    }

    /**
     * 限免方案
     * @param $resInfo
     */
    private function handleFreeForecast(&$resInfo)
    {
        $resInfo[] = [
            'title'     => '限免方案无限看',
            'desc'      => '每月上千场热门赛事的指定方案，会员可免费查看',
            'img'       => getImgOssUrl('/op/member/free_forecast.png'),
            'button'    => '去查看',
            'price'     => '',
            'jump_info' => appJumpInfo(501, [
                'newsPlateType' => 18
            ]),
        ];
    }

    /**
     * 红包
     * @param $resInfo
     */
    private function handleRedPacket(&$resInfo)
    {
        if ($this->dataType == self::BIG_DATA_TYPE) {

            $resInfo[] = [
                'title'     => '288嗨币红包',
                'desc'      => '开通会员立得288嗨币红包，订阅文章、模型更优惠！',
                'img'       => getImgOssUrl('/op/member/red_packet.png'),
                'button'    => '去查看',
                'price'     => '',
                'jump_info' => h5JumpInfo('/page/new_my', [], 'step14', env('OLD_MOBILE_DOMAIN')),
            ];

        } else {
            $resInfo[] = [
                'title'     => '88嗨币红包',
                'desc'      => '开通会员立得88嗨币红包，订阅文章、模型更优惠！',
                'img'       => getImgOssUrl('/op/member/red_packet.png'),
                'button'    => '去查看',
                'price'     => '',
                'jump_info' => h5JumpInfo('/page/new_my', [], 'step14', env('OLD_MOBILE_DOMAIN')),
            ];
        }
    }

    /**
     * 大数据报告模型
     * @param $resInfo
     */
    private function handleBigDataReport(&$resInfo)
    {
        if ($this->dataType != self::BIG_DATA_TYPE) {
            return;
        }

        $resInfo[] = [
            'title'     => '大数据报告模型',
            'desc'      => '根据实力、状态、建站、战役、指数等数据进行多维分析',
            'img'       => getImgOssUrl('/op/member/big_data_report.png'),
            'button'    => '去查看',
            'price'     => '298元/月',
            'jump_info' => h5JumpInfo('/model/bigData', [
                'type' => 15
            ]),
        ];
    }

    /**
     * 数据雷达模型
     * @param $resInfo
     */
    private function handleDataRadar(&$resInfo)
    {
        $resInfo [] = [
            'title'     => '数据雷达模型',
            'desc'      => '精选全球比赛影响巨大的各项数据，进行多维数据统计',
            'img'       => getImgOssUrl('/op/member/data_radar.png'),
            'button'    => '去查看',
            'price'     => '298元/月',
            'jump_info' => h5JumpInfo('/model/dataRadar', [
                'type' => 20
            ]),
        ];
    }

    /**
     * 改名卡
     * @param $resInfo
     */
    private function handleRenamingCard(&$resInfo)
    {
        if ($this->experience) {
            return;
        }

        $resInfo[] = [
            'title'     => '改名卡',
            'desc'      => '会员每月可免费更改3次昵称、简介',
            'img'       => getImgOssUrl('/op/member/renaming_card.png'),
            'button'    => '去查看',
            'price'     => '90元/月',
            'jump_info' => appJumpInfo(203),
        ];
    }

    private function initParams()
    {
    }
}
