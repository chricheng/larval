<?php


namespace App\Services\News\Match;

use App\Services\Tools\Common\CommonSetterService;
use App\Services\Passport\Member\MemberHomeService;
use App\Models\Cms\UserMatchUnblockLogModel;
/**
 * 资讯弹窗信息
 * Class MatchUnblockLogService
 * @package App\Services\News\Match
 * @method $this setArgObj($obj)
 */
class MatchUnblockLogService
{
    use CommonSetterService;

    /**赛事类型
     * @var object()
     */
    protected $argObj;

    const UNBLOCK_COUNT   =  3;

    //弹窗显示
    public function main()
    {
        return $this->handlePopMessage();

    }

    #点击解锁
    public function doBlock()
    {
        return $this->handleDoBlock();

    }


    private function handlePopMessage()
    {
         $leftBlockCount = $this->userUnlockLeftCount();

         $resInfo = app(MemberHomeService::class)->getUserBuyStatus($this->argObj->uid);

         $rightButton   =  "新用户限免";
         if(isset($resInfo['can_experience']) && $resInfo['can_experience'] == 0){  //新注册且未开通会员的用户
             $rightButton   =  "开通会员";
         }

         //返回数据 []
        return [
            'blockCount' => $leftBlockCount,//剩余次数
            'text' => '每日可免费解锁'.self::UNBLOCK_COUNT.'场限免方案',
            'downTextLeft' => '剩余场次:',
            'downTextRight' => $leftBlockCount .'场',
            'leftButton' => '解锁本场',
            'rightButton' => $rightButton,
            'rightButton2' => '可查看所有赛事',
            'vip_url' => getNewMobileUrl('/vip')
        ];
    }


    private function handleDoBlock()
    {
        $data   =  [
          "uid"           => $this->argObj->uid,
          "sport_id"      => $this->argObj->sportId,
          "match_id"      => $this->argObj->matchId,
        ];

        $leftBlockCount = $this->userUnlockLeftCount();

        if($leftBlockCount == 0){  #无解锁次数
            return false;
        }

        $userModel = UserMatchUnblockLogModel::query()
            ->where('uid', $this->argObj->uid)
            ->where('sport_id', $this->argObj->sportId)
            ->where('match_id', $this->argObj->matchId)
            ->first();

        if (empty($userModel)) {
             UserMatchUnblockLogModel::query()->create($data);
        }

        return true;
    }

    private function  userUnlockLeftCount()
    {
        $userCount = UserMatchUnblockLogModel::whereUid($this->argObj->uid)
            ->whereBetween('create_time',[
                now()->startOfDay(),
                now()->endOfDay()
            ])
            ->count();

        $blockCount = self::UNBLOCK_COUNT - $userCount;
        $leftBlockCount = ($blockCount >= 0) ? $blockCount : 0;
        return  $leftBlockCount;
    }

}
