<?php


namespace App\Services\News\Common;


use App\Models\Cms\Expert\ExpertModel;
use App\Services\Tools\Common\CommonSetterService;

/**
 * Class ExpertBaseInfoService
 * @package App\Services\News\Common
 * @method $this setExpertModel($expertModel)
 */
class ExpertBaseInfoService
{
    use CommonSetterService;

    /**
     * @var ExpertModel
     */
    protected $expertModel;

    public function main()
    {

        $resInfo = [
            'uid'          => $this->expertModel->uid,
            'status'       => $this->expertModel->status,
            'nickname'     => $this->expertModel->nickname,
            'avatar'       => formatAvatarUrl($this->expertModel->avatar),
            'intro'        => $this->expertModel->intro,
            'level'        => $this->expertModel->level,
            'exp_level'    => $this->expertModel->status == -99 ? 0 : $this->getExpLevel($this->expertModel->exp),
            'exp'          => $this->expertModel->exp,
            'usertype'     => $this->expertModel->user_type,
            'set_nickname' => $this->expertModel->nickname == $this->expertModel->uid ? '2' : '1',
            'regtime'      => $this->expertModel->reg_time,
            'followerNum'  => $this->expertModel->follower_num,
            'followedNum'  => $this->expertModel->followed_num,
            'isFollowed'   => 0,
        ];

        $this->handleMpInfo($resInfo);
        $this->handlePointRank($resInfo);

        return $resInfo;
    }


    /**
     * 专家信息
     * @param $resInfo
     */
    private function handleMpInfo(&$resInfo)
    {
        $resInfo['mp']      = $this->expertModel->pass_flag;
        $resInfo['mp_memo'] = config("mp.status_memo.{$this->expertModel->pass_flag}", '');;
        $resInfo['anchor_status'] = $this->expertModel->anchor_status;
    }

    /**
     * 积分，等级
     * @param $resInfo
     */
    private function handlePointRank(&$resInfo)
    {
        $points = $this->expertModel->points;

        if ($points >= 200) {
            $points += $this->expertModel->follower_num;
        }

        if ($points < 0) {
            $rank = '-1';
        } elseif ($points < 200) {
            $rank = '1';
        } elseif ($points < 1000) {
            $rank = '2';
        } else {
            $rank = '3';
        }

        $resInfo['mp_rank']   = $rank;
        $resInfo['mp_points'] = $points;
    }

    /**
     * LV等级
     * @param $exp
     * @return int
     */
    protected function getExpLevel($exp)
    {
        if ($exp >= 0 && $exp <= 9) {
            $exp_level = 0;
        } elseif ($exp >= 10 && $exp < 50) {
            $exp_level = 1;
        } elseif ($exp >= 50 && $exp < 100) {
            $exp_level = 2;
        } elseif ($exp >= 100 && $exp < 300) {
            $exp_level = 3;
        } elseif ($exp >= 300 && $exp < 500) {
            $exp_level = 4;
        } elseif ($exp >= 500 && $exp < 1000) {
            $exp_level = 5;
        } elseif ($exp >= 1000 && $exp < 2000) {
            $exp_level = 6;
        } elseif ($exp >= 2000 && $exp < 5000) {
            $exp_level = 7;
        } elseif ($exp >= 5000 && $exp < 10000) {
            $exp_level = 8;
        } elseif ($exp >= 10000 && $exp < 20000) {
            $exp_level = 9;
        } elseif ($exp >= 20000 && $exp < 50000) {
            $exp_level = 10;
        } elseif ($exp >= 50000 && $exp < 100000) {
            $exp_level = 11;
        } elseif ($exp >= 100000 && $exp < 200000) {
            $exp_level = 12;
        } elseif ($exp >= 200000 && $exp < 500000) {
            $exp_level = 13;
        } elseif ($exp >= 500000 && $exp < 1000000) {
            $exp_level = 14;
        } elseif ($exp >= 1000000 && $exp < 2000000) {
            $exp_level = 15;
        } elseif ($exp >= 2000000 && $exp < 5000000) {
            $exp_level = 16;
        } elseif ($exp >= 5000000 && $exp < 10000000) {
            $exp_level = 17;
        } elseif ($exp >= 10000000 && $exp < 20000000) {
            $exp_level = 18;
        } elseif ($exp >= 20000000) {
            $exp_level = 19;
        } else {
            $exp_level = 0;
        }

        return $exp_level;
    }
}
