<?php

namespace App\Services\News\Banner;

use App\Models\Cms\BannerModel;
use App\Services\Tools\Common\CommonSetterService;

/**
 * 专家-热榜
 * Class BannerService
 * @package App\Http\Controllers\News\User
 * @method $this setUid($Uid)
 * @method $this setDevice($device)
 * @method $this setVersion($version)
 * @method $this setChannel($channel)
 */
class BannerService
{
    use CommonSetterService;

    /**we
     * $uid int 用户ID ; 0 未登录
     */
    protected $uid = 0;
    protected $device = '';
    protected $version = 0;
    protected $channel = '';

    public function main()
    {
        $resInfo = [
            "custom" => null,     // 获取 首页自定义弹窗
        ];

        $custom = $this->handleCustom();
        if (!empty($custom)) {
            $resInfo["custom"] = $custom;
        }

        return $resInfo;
    }

    private function handleCustom()
    {
        $pasInfo = [];

        $info = $this->getAllCustom();
        if(empty($info)){
            return $pasInfo;
        }

        $this->checkVDC($info);
        $pasInfo = $this->checkUids($info);
        return $pasInfo;
    }

    private function checkUids(&$info)
    {
        $pasInfo = [];
        foreach ($info as $row){
            $uids = $row["uids"];
            $uids = array_filter($uids);
            // 为空则匹配所有用户
            if(empty($uids)){
                // 判断是否登录校验
                $sign_in = $row["sign_in"];
                if(!$sign_in){
                    $pasInfo = $row;
                }elseif ($sign_in and $this->uid){
                    $pasInfo = $row;
                }
            }else{
                if(in_array($this->uid,$uids)){
                    $pasInfo = $row;
                }
            }
            if(!empty($pasInfo)){
                break;
            }
        }
        if(!empty($pasInfo)){
            unset($pasInfo["uids"]);
        }
        return $pasInfo;
    }

    private function checkVDC(&$info)
    {
        /**
         * device 2 ios ;3 android
         *
         */
        $idevice = 0;
        if(strtolower($this->device) == "ios"){
            $idevice = 2;
        }elseif (strtolower($this->device) == "android"){
            $idevice = 3;
        }
        $info = collect($info)
            ->where("device",$idevice)
            ->where("version",intval($this->version))
            ->whereIn("channel",["all",strval($this->channel)])
            ->toArray();
    }

    private function getAllCustom()
    {
        $keyname = "cms_banner_customer_index";
        $info = \Cache::get($keyname);
        if(empty($info)){
            // 获取 首页自定义弹窗
            $now = date('Y-m-d H:i:s');
            $info = BannerModel::select(["id","device","version","channel","uids","imgurl","mtitle",
                "stitle","jumptype","url","params","sign_in","code","newsid"])
                ->where('type',34)
                ->where('status',1)
                ->where('endtime',">",$now)
                ->orderBy('createtime')
                ->get()
                ->toArray();
            \Cache::set($keyname,json_encode($info),now()->addMinute());
        }else{
            $info = json_decode($info,true);
        }
        return $info;
    }

}
