<?php


namespace App\Services\News\Expert;

use App\Models\Cms\NewsModel;
use App\Services\Tools\Common\CommonSetterService;

/**
 * Class ExpertTagBaseService
 * @package App\Services\News\Expert
 * @method $this setSportId($sportId)
 * @method $this setIsFirst($isFirst)
 * @method $this setIsJc($isJc)
 * @method $this setUid($uid)
 * @method $this setLeagueId($leagueId)
 * @method $this setKindArr($kind)
 */
class ExpertTagBaseService
{
    use CommonSetterService;

    /**
     * 赛事类型 1足球 2篮球
     * @var int
     */
    protected $sportId = 0;

    /**
     * 是否首次执行
     * @var bool
     */
    protected $isFirst = false;

    /**
     * 是否竞彩赛事
     * @var int
     */
    protected $isJc = 0;

    /**
     * 当前连红最大值
     * @var int
     */
    protected $limit = 50;

    /**
     * @var int
     */
    protected $uid = 0;

    protected $leagueId = 0;

    protected $kindArr = [];

    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    /**
     * 计算最大连红
     * @param $arr
     * @return int
     */
    protected function reckonMaxHighNum($arr)
    {
        $winNum     = 0;
        $maxHighNum = 0;

        foreach ($arr as $item) {

            if ($item == 2) {

                if ($winNum > $maxHighNum) {
                    $maxHighNum = $winNum;
                }

                $winNum = 0;
                continue;
            }

            $winNum++;
        }

        return $maxHighNum;
    }

}
