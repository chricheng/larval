<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertModel;

trait ExpertCommonTrait
{

    /**
     * 判断专家文章状态
     * @param $uid
     * @return bool
     */
    protected function checkCmsActive($uid)
    {
        $model = ExpertModel::query()
            ->where('uid', $this->uid)
            ->first();

        if (empty($model)) {
            return false;
        }

        return !!$model->is_active;
    }
}
