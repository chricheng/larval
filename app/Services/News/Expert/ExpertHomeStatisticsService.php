<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertJcKindTagModel;
use App\Models\Cms\Expert\ExpertKindTagModel;
use App\Models\Cms\Expert\ExpertLeagueTagModel;
use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Cms\NewsModel;
use App\Services\Tools\Common\CommonSetterService;

/**
 * 专家主页统计信息
 * Class ExpertHomeStatisticsService
 * @package App\Services\News\Expert
 * @method $this setUid($uid)
 * @method $this setSportId($sportId)
 */
class ExpertHomeStatisticsService
{
    use CommonSetterService, ExpertCommonTrait;

    protected $uid;

    /**
     * @var int
     */
    protected $sportId = 0;

    const JC_BET_KIND = [
        'JCSPF',
        'JCRQSF',
        'JCLQSF',
        'JCLQRFSF',
        'JCLQDXF'
    ];

    const KIND_ALL = 'ALL';

    public function main()
    {
        $redisKey = "expert_home_statistics_v2:{$this->uid}:{$this->sportId}";

        return \Cache::remember($redisKey, now()->addMinutes(5), function () {

            $resInfo = [];

            //判断专家方案是否可以显示
            if (!$this->checkCmsActive($this->uid)) {
                return $resInfo;
            }

            if ($this->sportId == 1) {
                $resInfo['jc']     = $this->handleKindInfo(1);
                $resInfo['normal'] = $this->handleKindInfo();
            } elseif ($this->sportId == 2) {
                $resInfo['jc']     = $this->handleKindInfo(1);
                $resInfo['normal'] = $this->handleKindInfo();
            } else {
                $resInfo['all'] = $this->getTogetherInfo();
            }

            $this->handleReturnRate($resInfo);
            $this->handleHitRate($resInfo);

            return $resInfo;
        });
    }


    private function handleKindInfo($jc = 0)
    {
        if ($jc) {
            $kindTagModels = ExpertJcKindTagModel::query()
                ->where('uid', $this->uid)
                ->where('sport_id', $this->sportId)
                ->get();

        } else {
            $kindTagModels = ExpertKindTagModel::query()
                ->where('uid', $this->uid)
                ->where('sport_id', $this->sportId)
                ->get();
        }


        $resInfo = [];

        foreach ($kindTagModels as $tagModel) {

            $tmp = [];

            $tmp['hit_rate_desc']    = $this->getHitRateText($tagModel->hit_rate_desc);
            $tmp['red_num_desc']     = $this->getRedNumText($tagModel->hit_high_num);
            $tmp['max_red_num_desc'] = $this->getRedNumText($tagModel->max_high_num);
            $tmp['order_num_desc']   = $tagModel->total_order_num.'次';
            $tmp['seven_rate']       = $this->getKindRateByDays($tagModel, 7);
            $tmp['thirty_rate']      = $this->getKindRateByDays($tagModel, 30);

            $tmp['ten'] = $tagModel->hit_last_10;

            $resInfo[strtolower($tagModel->kind)] = $tmp;
        }

        return $resInfo;
    }

    /**
     * 联赛命中率前3
     * @param $resInfo
     */
    private function handleHitRate(&$resInfo)
    {
        $builder = ExpertLeagueTagModel::query()
            ->where('uid', $this->uid)
            ->whereRaw('total_hong_num+total_hei_num+total_zou_num >=5');

        if (!empty($this->sportId)) {
            $builder->where('sport_id', $this->sportId);
        }

        $tagModels = $builder
            ->orderByDesc('total_hit_rate')
            ->limit(3)
            ->get();

        $arr = [];
        foreach ($tagModels as $model) {

            $arr[] = [
                'league_id'   => $model->league_id,
                'league_name' => $model->league_name,
                'win'         => $model->total_hong_num,
                'miss'        => $model->total_hei_num,
                'zou'         => $model->total_zou_num,
                'rate'        => $model->total_hit_rate,
            ];
        }

        if (!empty($arr)) {
            $resInfo['hit_rate'] = $arr;
        }
    }

    /**
     * 联赛回报率前3
     * @param $resInfo
     */
    private function handleReturnRate(&$resInfo)
    {
        $builder = ExpertLeagueTagModel::query()
            ->where('uid', $this->uid)
            ->whereRaw('total_hong_num+total_hei_num+total_zou_num >=5');

        if (!empty($this->sportId)) {
            $builder->where('sport_id', $this->sportId);
        }

        $tagModels = $builder
            ->orderByDesc('total_return_rate')
            ->limit(3)
            ->get();

        $arr = [];
        foreach ($tagModels as $model) {

            $arr[] = [
                'league_id'   => $model->league_id,
                'league_name' => $model->league_name,
                'win'         => $model->total_hong_num,
                'miss'        => $model->total_hei_num,
                'zou'         => $model->total_zou_num,
                'rate'        => $model->total_return_rate,
            ];
        }

        if (!empty($arr)) {
            $resInfo['return_rate'] = $arr;
        }
    }

    private function getTogetherInfo()
    {
        $tagModel = ExpertTagModel::query()
            ->where('uid', $this->uid)
            ->where('sport_id', $this->sportId)
            ->first();

        if (empty($tagModel)) {
            return [];
        }

        $resInfo['hit_rate_desc']    = $this->getHitRateText($tagModel->hit_rate_desc);
        $resInfo['red_num_desc']     = $this->getRedNumText($tagModel->hit_high_num);
        $resInfo['max_red_num_desc'] = $this->getRedNumText($tagModel->max_high_num);
        $resInfo['order_num_desc']   = $tagModel->total_order_num.'次';
        $resInfo['seven_rate']       = $this->getRateByDays($tagModel, 7);
        $resInfo['thirty_rate']      = $this->getRateByDays($tagModel, 30);
        $resInfo['ten']              = $tagModel->hit_last_10;

        return $resInfo;
    }


    /**
     * @param  ExpertKindTagModel|ExpertJcKindTagModel  $tagModel
     * @param  int  $days
     * @param  int  $jc
     * @return array|int[]
     */
    private function getKindRateByDays($tagModel, $days = 7, $jc = 0)
    {
        $builder = NewsModel::query()
            ->where('authorid', $tagModel->uid)
            ->where('firsttime', '>', now()->subDays($days)->startOfDay());

        if (!empty($tagModel->sport_id)) {
            $builder->where('sport_id', $tagModel->sport_id);
        }

        if ($tagModel->kind == self::KIND_ALL) {
            if ($jc) {
                $builder->whereIn('betKind', self::JC_BET_KIND);
            } else {
                $builder->whereNotIn('betKind', self::JC_BET_KIND);
            }
        } else {
            $builder->where('betKind', $tagModel->kind);
        }

        $arr = $builder
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1)
            ->selectRaw('isWin,count(1) num')
            ->groupBy('isWin')
            ->pluck('num', 'isWin')
            ->toArray();

        if (empty($arr)) {
            return [
                'win'  => 0,
                'zou'  => 0,
                'miss' => 0,
            ];
        }

        $totalNum = array_sum($arr);

        $win  = intval(round(data_get($arr, 1, 0) / $totalNum, 2) * 100);
        $zou  = intval(round(data_get($arr, 3, 0) / $totalNum, 2) * 100);
        $miss = max(100 - $win - $zou, 0);

        return [
            'win'  => $win,
            'zou'  => $zou,
            'miss' => $miss,
        ];
    }

    /**
     * 获取近N天红走黑比例
     * @param  ExpertTagModel  $tagModel
     * @param  int  $days
     * @return array|int[]
     */
    private function getRateByDays($tagModel, $days = 7)
    {
        $builder = NewsModel::query()
            ->where('authorid', $tagModel->uid)
            ->where('firsttime', '>', now()->subDays($days)->startOfDay());

        if (!empty($tagModel->sport_id)) {
            $builder->where('sport_id', $tagModel->sport_id);
        }


        $arr = $builder
            ->whereIn('isWin', [1, 2, 3])
            ->where('status', 1)
            ->where('predict_status', 1)
            ->selectRaw('isWin,count(1) num')
            ->groupBy('isWin')
            ->pluck('num', 'isWin')
            ->toArray();

        if (empty($arr)) {
            return [
                'win'  => 0,
                'zou'  => 0,
                'miss' => 0,
            ];
        }


        $totalNum = array_sum($arr);

        $win  = intval(round(data_get($arr, 1, 0) / $totalNum, 2) * 100);
        $zou  = intval(round(data_get($arr, 3, 0) / $totalNum, 2) * 100);
        $miss = max(100 - $win - $zou, 0);

        return [
            'win'  => $win,
            'zou'  => $zou,
            'miss' => $miss,
        ];
    }

    private function getHitRateText($desc)
    {
        if (empty($desc)) {
            return '近-中-';
        }

        return $desc;
    }

    private function getRedNumText($num, $limit = 3)
    {
        if ($num >= $limit) {
            return $num.'连红';
        }

        return '-';
    }
}
