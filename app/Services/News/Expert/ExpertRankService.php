<?php


namespace App\Services\News\Expert;


use App\Exceptions\ParamsException;
use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Passport\User\UserFriendModel;
use App\Services\News\Common\ExpertBaseInfoService;
use App\Services\Tools\Common\CommonSetterService;

/**
 * Class ExpertRankService
 * @package App\Services\News\Expert
 * @method $this setSportId($sportId)
 * @method $this setType($type)
 * @method $this setSubtype($subtype)
 * @method $this setLoginUid($loginUid)
 */
class ExpertRankService
{
    use CommonSetterService;

    /**
     * @var string 主类型
     */
    protected $type = '';

    /**
     * @var string 子类型
     */
    protected $subtype = '';


    protected $loginUid = 0;

    /**
     * @var int 0全部 1足球  2篮球
     */
    protected $sportId = 0;

    private $limit = 20;

    private $articleDays = 30;

    const TYPE_ARR = [
        //连红
        'high_num'    => [
            'hit_high_num' => [
                'after' => '连红'
            ],
            'max_high_num' => [
                'after' => '连红'
            ],
        ],
        //销量榜
        'order_num'   => [
            'order_num_3_days'  => [
                'after' => '人次'
            ],
            'order_num_7_days'  => [
                'after' => '人次'
            ],
            'order_num_30_days' => [
                'after' => '人次'
            ],
        ],
        //高回报
        'return_rate' => [
            'return_rate_10_sessions' => [
                'after' => '%'
            ],
            'return_rate_7_days'      => [
                'after' => '%'
            ],
        ],
        //盈利
        'profit'      => [
            'profit_num_20_days' => [
                'before' => '近20盈'
            ],
            'profit_num_3_days'  => [
                'before' => '近3盈'
            ],
            'profit_num_7_days'  => [
                'before' => '近7盈'
            ],
        ],
        //战绩
        'several'     => [
            'hit_num_20_sessions' => [
                'before' => '近20中'
            ],
            'hit_num_10_sessions' => [
                'before' => '近10中'
            ],
            'hit_num_50_sessions' => [
                'before' => '近50中'
            ],
        ],
    ];

    public function main()
    {
        $this->initParams();

        $redisKey = "expert_rank_hit_v1:{$this->sportId}{$this->type}:{$this->subtype}";

        $resInfo = \Cache::remember($redisKey, now()->addMinutes(10), function () {

            $orderColumn = "tag.{$this->subtype}";

            $tagModels = ExpertTagModel::query()
                ->with(['expert'])
                ->from('expert_tags as tag')
                ->leftJoin('experts as expert', 'tag.uid', '=', 'expert.uid')
                ->where('tag.last_article_time', '>', now()->subDays($this->articleDays))
                ->where('tag.sport_id', $this->sportId)
                ->where('expert.is_active', 1)
                ->orderByDesc($orderColumn)
                ->select(['tag.*'])
                ->limit($this->limit)
                ->get();


            $resInfo = [];

            foreach ($tagModels as $model) {

                $resInfo[] = [
                    'uid'          => $model->uid,
                    'sport_id'     => $model->sport_id,
                    'buy_count'    => $model->buy_count,
                    'max_high_num' => $model->max_high_num,
                    'text'         => $this->getTextInfo(data_get($model, $this->subtype)),
                    'user_info'    => app(ExpertBaseInfoService::class)
                        ->setExpertModel($model->expert)
                        ->main(),
                    'login_uid'    => $this->loginUid,
                ];
            }

            return $resInfo;
        });

        $this->handleFollowedInfo($resInfo);

        return $resInfo;
    }

    private function getTextInfo($value)
    {
        $before = data_get(self::TYPE_ARR, "{$this->type}.{$this->subtype}.before", '');
        $after  = data_get(self::TYPE_ARR, "{$this->type}.{$this->subtype}.after", '');

//        if ($this->type == 'return_rate') {
//            $value = sprintf("%.2f", $value);
//        }

        return $before.$value.$after;
    }

    /**
     * 获取关注信息
     * @param $resInfo
     */
    private function handleFollowedInfo(&$resInfo)
    {
        if (empty($this->loginUid) || empty($resInfo)) {
            return;
        }

        $uidArr = array_column($resInfo, 'uid');

        $followedArr = UserFriendModel::query()
            ->where('followuid', $this->loginUid)
            ->whereIn('uid', $uidArr)
            ->pluck('uid')
            ->toArray();

        foreach ($resInfo as &$item) {

            if (in_array($item['uid'], $followedArr)) {
                $item['user_info']['isFollowed'] = 1;
            } else {
                $item['user_info']['isFollowed'] = 0;
            }
        }
    }


    private function initParams()
    {
        if (!isset(self::TYPE_ARR[$this->type][$this->subtype])) {
            throw new ParamsException();
        }

    }
}
