<?php


namespace App\Services\News\Expert;

use App\Models\Cms\Expert\ExpertTagModel;
use App\Models\Cms\NewsModel;

/**
 * Class ExpertTagInitService
 * @package App\Services\News\Expert
 * @method $this setSportId($sportId)
 * @method $this setIsFirst($isFirst)
 */
class ExpertTagInitService extends ExpertTagBaseService
{
    public function main()
    {
        $this->handleEachUid($this->uid);
    }

    private function handleEachUid($uid)
    {
        $resInfo = [
            'uid'      => $uid,
            'sport_id' => $this->sportId,
        ];

        $tagModel = ExpertTagModel::query()
            ->where('uid', $uid)
            ->where('sport_id', $this->sportId)
            ->first();

        //初始化历史最高连红
        if (!empty($tagModel)) {
            $resInfo['max_high_num'] = $tagModel->max_high_num;
        } else {
            $resInfo['max_high_num'] = 0;
        }

        if ($this->isFirst) {
            $this->handleMaxHighNum($uid, $resInfo);
        }

        $this->handleRateAndDesc($uid, $resInfo);
        $this->handleHitHighNum($uid, $resInfo);
        $this->handleBuyCountInfo($uid, $resInfo);
        $this->handleLastArticleTime($uid, $resInfo);

        $this->handleReturnRate($uid, $resInfo);
        $this->handleTotalOrderNum($uid, $resInfo);
        $this->handleLastHitInfo($uid, $resInfo);
        $this->handleHitNumBySessions($uid, $resInfo);

        if (!empty($tagModel)) {
            $tagModel->update($resInfo);
        } else {
            ExpertTagModel::query()->create($resInfo);
        }
    }

    private function handleHitNumBySessions($uid, &$resInfo)
    {
        $resInfo['hit_num_10_sessions'] = $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->limit(10)
            ->select(['isWin'])
            ->get()
            ->where('isWin', 1)
            ->count();

        $resInfo['hit_num_20_sessions'] = $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->limit(20)
            ->select(['isWin'])
            ->get()
            ->where('isWin', 1)
            ->count();

        $resInfo['hit_num_50_sessions'] = $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->limit(50)
            ->select(['isWin'])
            ->get()
            ->where('isWin', 1)
            ->count();
    }


    /**
     * 近期情况
     * @param $uid
     * @param $resInfo
     */
    private function handleLastHitInfo($uid, &$resInfo)
    {
        //近10场
        $resInfo['hit_last_10'] = $this->initBuilder($uid, [1, 2, 3])
            ->orderByDesc('firsttime')
            ->limit(10)
            ->pluck('isWin')
            ->toArray();
    }


    /**
     * 处理带红人数
     * @param $uid
     * @param $resInfo
     */
    private function handleTotalOrderNum($uid, &$resInfo)
    {
        $resInfo['total_order_num'] = $this->initBuilder($uid, [1])
            ->sum('sales');
    }

    /**
     * 回报率
     * @param $uid
     * @param $resInfo
     */
    private function handleReturnRate($uid, &$resInfo)
    {
        //近7天回报率
        $days = 7;

        $obj = $this->initBuilder($uid)
            ->where('firsttime', '>=', now()->subDays($days))
            ->selectRaw('FORMAT(SUM(winFee)/SUM(betFee)*100,2) as res')
            ->first();

        $resInfo['return_rate_7_days'] = (string) data_get($obj, 'res', 0);

        //近10场回报率
        $limit = 10;

        $tmp = $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->limit($limit)
            ->pluck('winFee')
            ->toArray();

        $tmpNum = count($tmp);

        if ($tmpNum == 0) {
            $resInfo['return_rate_10_sessions'] = 0;
        } else {
            $resInfo['return_rate_10_sessions'] = (string) round(collect($tmp)->sum() / ($tmpNum * 100));
        }
    }


    /**
     * 最后发文时间
     * @param $uid
     * @param $resInfo
     */
    private function handleLastArticleTime($uid, &$resInfo)
    {
        $model = $this->initBuilder($uid, [0, 1, 2, 3])
            ->orderByDesc('firsttime')
            ->first();

        if (!empty($model)) {
            $resInfo['last_article_time'] = $model->firsttime;
        }
    }

    /**
     * 处理可够信息及最后范文时间
     * @param $uid
     * @param $resInfo
     */
    private function handleBuyCountInfo($uid, &$resInfo)
    {
        $num = $this->initBuilder($uid, [0])
            ->where('for_sale', 1)
            ->orderByDesc('firsttime')
            ->count();

        $resInfo['can_buy']   = $num > 0 ? 1 : 0;
        $resInfo['buy_count'] = $num;
    }

    /**
     * 计算连红
     * @param $uid
     * @param $resInfo
     */
    private function handleHitHighNum($uid, &$resInfo)
    {
        $arr = $this->getLastNumNews($uid, 50);

        $num = 0;

        foreach ($arr as $item) {

            if ($item == 2) {
                break;
            }

            $num++;
        }

        $resInfo['hit_high_num'] = $num;

        if ($num > $resInfo['max_high_num']) {
            $resInfo['max_high_num'] = $num;
        }
    }

    /**
     * 处理近期命中率和描述
     * @param $uid
     * @param $resInfo
     */
    private function handleRateAndDesc($uid, &$resInfo)
    {
        $arr = $this->getLastNumNews($uid, 50);

        $tmpArr   = [];
        $totalNum = $winNum = 0;

        foreach ($arr as $item) {

            $totalNum += 1;

            if ($item == 1) {
                $winNum += 1;
            }

            if ($totalNum >= 4) {

                if ($totalNum == $winNum) {
                    continue;
                }

                $tmpArr[$totalNum] = [
                    'hit_rate'      => intval(round($winNum / $totalNum, 2) * 100),
                    'hit_rate_desc' => "近{$totalNum}中{$winNum}",
                    'hit_rate_x'    => $winNum,
                    'hit_rate_y'    => $totalNum,
                ];
            }
        }


        $firstArr = [];
        foreach ($tmpArr as $item) {

            if (empty($firstArr)) {
                $firstArr = $item;
                continue;
            }

            if ($item['hit_rate'] >= $firstArr['hit_rate']) {
                if ($item['hit_rate_y'] > $firstArr['hit_rate_y']) {
                    $firstArr = $item;
                }
            }
        }

        if (empty($firstArr)) {
            $firstArr = [
                'hit_rate'      => 0,
                'hit_rate_desc' => '',
                'hit_rate_x'    => 0,
                'hit_rate_y'    => 0,
            ];
        }

        $resInfo = array_merge($resInfo, $firstArr);
    }

    private function handleMaxHighNum($uid, &$resInfo)
    {
        $arr = $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->pluck('isWin')
            ->toArray();

        $winNum     = 0;
        $maxHighNum = 0;

        foreach ($arr as $key => $item) {
            if ($item == 2) {
                if ($winNum > $maxHighNum) {
                    $maxHighNum = $winNum;
                }

                $winNum = 0;
                continue;
            }

            $winNum++;
        }

        $resInfo['max_high_num'] = $maxHighNum;
    }


    private function getLastNumNews($uid, $num)
    {
        return $this->initBuilder($uid)
            ->orderByDesc('firsttime')
            ->limit($num)
            ->pluck('isWin')
            ->toArray();
    }


    /**
     * 处理赛事类型查询
     * @param $uid
     * @param  int[]  $isWin
     * @return NewsModel
     */
    private function initBuilder($uid, $isWin = [1, 2])
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid);

        if (!empty($isWin)) {
            $builder->whereIn('isWin', $isWin);
        }

        if (empty($this->sportId)) {
            $builder->whereIn('sport_id', [1, 2]);
        } else {
            $builder->where('sport_id', $this->sportId);
        }

        $builder->where('status', 1)
            ->where('predict_status', 1);

        return $builder;
    }
}
