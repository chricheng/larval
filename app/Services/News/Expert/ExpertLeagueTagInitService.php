<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertLeagueTagModel;
use App\Models\Cms\NewsModel;
use App\Models\QtBasketball\BasketballLeagueModel;
use App\Models\QtData\FootballLeagueModel;

/**
 * Class ExpertLeagueTagInitService
 * @package App\Services\News\Expert
 */
class ExpertLeagueTagInitService extends ExpertTagBaseService
{
    public function main()
    {
        if ($this->sportId == 1) {

            $leagueModel = FootballLeagueModel::query()
                ->where('sclass_id', $this->leagueId)
                ->first();

            if (empty($leagueModel)) {
                return;
            }

            $leagueName = $leagueModel->name_js;


        } elseif ($this->sportId == 2) {

            $leagueModel = BasketballLeagueModel::query()
                ->where('league_id', $this->leagueId)
                ->first();

            if (empty($leagueModel)) {
                return;
            }

            $leagueName = $leagueModel->name_s;

        } else {
            return;
        }


        $this->handleEachLeague($this->uid, $this->leagueId, $leagueName);

    }


    private function handleEachLeague($uid, $leagueId, $leagueName)
    {
        $resInfo = [
            'uid'         => $uid,
            'sport_id'    => $this->sportId,
            'league_id'   => $leagueId,
            'league_name' => is_null($leagueName) ? '' : $leagueName,
        ];

        $tagModel = ExpertLeagueTagModel::query()
            ->where('uid', $uid)
            ->where('sport_id', $this->sportId)
            ->where('league_id', $leagueId)
            ->first();


        //初始化历史最高连红
        if (!empty($tagModel)) {
            $resInfo['max_high_num'] = $tagModel->max_high_num;
        } else {
            $resInfo['max_high_num'] = 0;
        }

        if ($this->isFirst) {
            $this->handleMaxHighNum($uid, $leagueId, $resInfo);
        }

        $this->handleRateAndDesc($uid, $leagueId, $resInfo);
        $this->handleHitHighNum($uid, $leagueId, $resInfo);
//        $this->handleLastArticleTime($uid, $leagueId, $resInfo);
        $this->handleTotalOrderNum($uid, $leagueId, $resInfo);
        $this->handleTotalInfo($uid, $leagueId, $resInfo);
        $this->handleTotalReturnRate($uid, $leagueId, $resInfo);
        $this->handleLastHitInfo($uid, $leagueId, $resInfo);
//        $this->handleBuyCountInfo($uid, $leagueId, $resInfo);

        if (!empty($tagModel)) {
            $tagModel->update($resInfo);
        } else {
            ExpertLeagueTagModel::query()->create($resInfo);
        }
    }

    /**
     * 总回报率
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleTotalReturnRate($uid, $leagueId, &$resInfo)
    {
        $obj = $this->initBuilder($uid, $leagueId)
            ->orderByDesc('firsttime')
            ->selectRaw('FORMAT(SUM(winFee)/SUM(betFee)*100,2) as res')
            ->first();

        $resInfo['total_return_rate'] = (string) data_get($obj, 'res', 0);
    }

    /**
     * 近10场命中情况
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleLastHitInfo($uid, $leagueId, &$resInfo)
    {
        $resInfo['hit_last_10'] = $this->initBuilder($uid, $leagueId, [1, 2, 3])
            ->orderByDesc('firsttime')
            ->limit(10)
            ->pluck('isWin')
            ->toArray();
    }

    /**
     * 处理总命中等信息
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleTotalInfo($uid, $leagueId, &$resInfo)
    {
        $countArr = $this->initBuilder($uid, $leagueId, [1, 2, 3])
            ->selectRaw('isWin,count(1) as num')
            ->groupBy('isWin')
            ->pluck('num', 'isWin')
            ->toArray();

        if (empty($countArr)) {
            return;
        }

        $resInfo['total_hong_num'] = data_get($countArr, 1, 0);
        $resInfo['total_hei_num']  = data_get($countArr, 2, 0);
        $resInfo['total_zou_num']  = data_get($countArr, 3, 0);

        $total = $resInfo['total_hong_num'] + $resInfo['total_hei_num'];

        if ($total <= 0) {
            return;
        }

        $resInfo['total_hit_rate']      = intval(round($resInfo['total_hong_num'] / $total, 2) * 100);
        $resInfo['total_hit_rate_desc'] = "{$total}中{$resInfo['total_hong_num']}";
        $resInfo['total_hit_rate_x']    = $resInfo['total_hong_num'];
        $resInfo['total_hit_rate_y']    = $total;

    }

    /**
     * 处理带红人数
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleTotalOrderNum($uid, $leagueId, &$resInfo)
    {
        $resInfo['total_order_num'] = $this->initBuilder($uid, $leagueId, [1])
            ->sum('sales');
    }

    /**
     * 计算连红
     * @param $uid
     * @param  $leagueId
     * @param $resInfo
     */
    private function handleHitHighNum($uid, $leagueId, &$resInfo)
    {
        $arr = $this->getLastNumNews($uid, $leagueId, 50);

        $num = 0;

        foreach ($arr as $item) {

            if ($item == 2) {
                break;
            }

            $num++;
        }

        $resInfo['hit_high_num'] = $num;

        if ($num > $resInfo['max_high_num']) {
            $resInfo['max_high_num'] = $num;
        }
    }

    /**
     * 处理近期命中率和描述
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleRateAndDesc($uid, $leagueId, &$resInfo)
    {
        $arr = $this->getLastNumNews($uid, $leagueId, 50);

        $tmpArr   = [];
        $totalNum = $winNum = 0;

        foreach ($arr as $item) {

            $totalNum += 1;

            if ($item == 1) {
                $winNum += 1;
            }

            if ($totalNum >= 4) {

                if ($totalNum == $winNum) {
                    continue;
                }

                $tmpArr[$totalNum] = [
                    'hit_rate'      => intval(round($winNum / $totalNum, 2) * 100),
                    'hit_rate_desc' => "近{$totalNum}中{$winNum}",
                    'hit_rate_x'    => $winNum,
                    'hit_rate_y'    => $totalNum,
                ];
            }
        }


        $firstArr = [];
        foreach ($tmpArr as $item) {

            if (empty($firstArr)) {
                $firstArr = $item;
                continue;
            }

            if ($item['hit_rate'] >= $firstArr['hit_rate']) {
                if ($item['hit_rate_y'] > $firstArr['hit_rate_y']) {
                    $firstArr = $item;
                }
            }
        }

        if (empty($firstArr)) {
            $firstArr = [
                'hit_rate'      => 0,
                'hit_rate_desc' => '',
                'hit_rate_x'    => 0,
                'hit_rate_y'    => 0,
            ];
        }

        $resInfo = array_merge($resInfo, $firstArr);
    }

    /**
     * 联赛最高连红
     * @param $uid
     * @param $leagueId
     * @param $resInfo
     */
    private function handleMaxHighNum($uid, $leagueId, &$resInfo)
    {
        $builder = $this->initBuilder($uid, $leagueId);
        $arr     = $builder->orderByDesc('firsttime')
            ->pluck('isWin')
            ->toArray();

        $winNum     = 0;
        $maxHighNum = 0;

        foreach ($arr as $item) {

            if ($item == 2) {

                if ($winNum > $maxHighNum) {
                    $maxHighNum = $winNum;
                }

                $winNum = 0;
                continue;
            }

            $winNum++;
        }

        $resInfo['max_high_num'] = $maxHighNum;
    }


    /**
     * 近50场
     * @param $uid
     * @param $leagueId
     * @param $num
     * @return array
     */
    private function getLastNumNews($uid, $leagueId, $num)
    {
        $builder = $this->initBuilder($uid, $leagueId);

        return $builder->orderByDesc('firsttime')
            ->limit($num)
            ->pluck('isWin')
            ->toArray();
    }


    /**
     * 初始化查询
     * @param $uid
     * @param  int  $leagueId
     * @param  int[]  $isWin
     * @return NewsModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    private function initBuilder($uid, $leagueId = 0, $isWin = [1, 2])
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid)
            ->where('sport_id', $this->sportId);

        if (!empty($leagueId)) {
            $builder->where('league_id', $leagueId);
        }

        return $builder->whereIn('isWin', $isWin)
            ->where('status', 1)
            ->where('predict_status', 1);
    }
}
