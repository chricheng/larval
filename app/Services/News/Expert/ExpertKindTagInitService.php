<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertJcKindTagModel;
use App\Models\Cms\Expert\ExpertKindTagModel;
use App\Models\Cms\NewsModel;

/**
 * Class ExpertKindTagInitService
 * @package App\Services\News\Expert
 */
class ExpertKindTagInitService extends ExpertTagBaseService
{
    const KIND_ALL = 'ALL';

    public function main()
    {
        array_unshift($this->kindArr, self::KIND_ALL);

        foreach ($this->kindArr as $kind) {
            $this->handleEachKind($this->uid, $kind);
        }
    }


    /**
     * 处理每一个玩法
     * @param $uid
     * @param $kind
     */
    private function handleEachKind($uid, $kind)
    {
        $resInfo = [
            'uid'      => $uid,
            'sport_id' => $this->sportId,
            'kind'     => $kind,
        ];

        if ($this->isJc == 1) {
            $tagModel = ExpertJcKindTagModel::query()
                ->where('uid', $uid)
                ->where('sport_id', $this->sportId)
                ->where('kind', $kind)
                ->first();
        } else {
            $tagModel = ExpertKindTagModel::query()
                ->where('uid', $uid)
                ->where('sport_id', $this->sportId)
                ->where('kind', $kind)
                ->first();
        }

        //初始化历史最高连红
        if (!empty($tagModel)) {
            $resInfo['max_high_num'] = $tagModel->max_high_num;
        } else {
            $resInfo['max_high_num'] = 0;
        }

        if ($this->isFirst) {
            $this->handleMaxHighNum($uid, $kind, $resInfo);
        }

        $this->handleRateAndDesc($uid, $kind, $resInfo);
        $this->handleHitHighNum($uid, $kind, $resInfo);
        //$this->handleLastArticleTime($uid, $kind, $resInfo);
        $this->handleTotalOrderNum($uid, $kind, $resInfo);
        $this->handleLastHitInfo($uid, $kind, $resInfo);

        if (!empty($tagModel)) {
            $tagModel->update($resInfo);
        } else {
            if ($this->isJc == 1) {
                ExpertJcKindTagModel::query()->create($resInfo);
            } else {
                ExpertKindTagModel::query()->create($resInfo);
            }
        }
    }

    /**
     * 近10场命中情况
     * @param $uid
     * @param $kind
     * @param $resInfo
     */
    private function handleLastHitInfo($uid, $kind, &$resInfo)
    {
        $resInfo['hit_last_10'] = $this->initBuilder($uid, $kind, [1, 2, 3])
            ->orderByDesc('firsttime')
            ->limit(10)
            ->pluck('isWin')
            ->toArray();
    }

    /**
     * 处理带红人数
     * @param $uid
     * @param $kind
     * @param $resInfo
     */
    private function handleTotalOrderNum($uid, $kind, &$resInfo)
    {
        $resInfo['total_order_num'] = $this->initBuilder($uid, $kind, [1])
            ->sum('sales');
    }

    /**
     * 最后发文时间
     * @param $uid
     * @param $kind
     * @param $resInfo
     */
    private function handleLastArticleTime($uid, $kind, &$resInfo)
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid)
            ->where('status', 1)
            ->where('for_sale', 1)
            ->where('sport_id', $this->sportId);

        if ($this->isJc == 1) {
            $builder->where('jc', 1);
        }

        if ($kind != self::KIND_ALL) {
            $builder->where('betKind', $kind);
        }

        $model = $builder
            ->orderByDesc('firsttime')
            ->first();

        if (!empty($model)) {
            $resInfo['last_article_time'] = $model->firsttime;
        }
    }

    /**
     * 计算连红
     * @param $uid
     * @param  $leagueId
     * @param $resInfo
     */
    private function handleHitHighNum($uid, $leagueId, &$resInfo)
    {
        $arr = $this->initBuilder($uid, $leagueId)
            ->orderByDesc('firsttime')
            ->limit($this->limit)
            ->pluck('isWin')
            ->toArray();

        $num = 0;

        foreach ($arr as $item) {

            if ($item == 2) {
                break;
            }

            $num++;
        }

        $resInfo['hit_high_num'] = $num;

        if ($num > $resInfo['max_high_num']) {
            $resInfo['max_high_num'] = $num;
        }
    }

    /**
     * 处理近期命中率和描述
     * @param $uid
     * @param $kind
     * @param $resInfo
     */
    private function handleRateAndDesc($uid, $kind, &$resInfo)
    {
        $arr = $this->initBuilder($uid, $kind)
            ->orderByDesc('firsttime')
            ->limit($this->limit)
            ->pluck('isWin')
            ->toArray();

        $tmpArr   = [];
        $totalNum = $winNum = 0;

        foreach ($arr as $item) {

            $totalNum += 1;

            if ($item == 1) {
                $winNum += 1;
            }

            if ($totalNum >= 4) {

                if ($totalNum == $winNum) {
                    continue;
                }

                $tmpArr[$totalNum] = [
                    'hit_rate'      => intval(round($winNum / $totalNum, 2) * 100),
                    'hit_rate_desc' => "近{$totalNum}中{$winNum}",
                    'hit_rate_x'    => $winNum,
                    'hit_rate_y'    => $totalNum,
                ];
            }
        }


        $firstArr = [];
        foreach ($tmpArr as $item) {

            if (empty($firstArr)) {
                $firstArr = $item;
                continue;
            }

            if ($item['hit_rate'] >= $firstArr['hit_rate']) {
                if ($item['hit_rate_y'] > $firstArr['hit_rate_y']) {
                    $firstArr = $item;
                }
            }
        }

        if (empty($firstArr)) {
            $firstArr = [
                'hit_rate'      => 0,
                'hit_rate_desc' => '',
                'hit_rate_x'    => 0,
                'hit_rate_y'    => 0,
            ];
        }

        $resInfo = array_merge($resInfo, $firstArr);
    }

    /**
     * 最高连红
     * @param $uid
     * @param $kind
     * @param $resInfo
     */
    private function handleMaxHighNum($uid, $kind, &$resInfo)
    {
        $arr = $this->initBuilder($uid, $kind)
            ->orderByDesc('firsttime')
            ->pluck('isWin')
            ->toArray();

        $resInfo['max_high_num'] = $this->reckonMaxHighNum($arr);
    }

    /**
     * 初始化查询
     * @param $uid
     * @param  string  $kind
     * @param  int[]  $isWin
     * @return NewsModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    private function initBuilder($uid, $kind = 'ALL', $isWin = [1, 2])
    {
        $builder = NewsModel::query()
            ->where('authorid', $uid)
            ->where('sport_id', $this->sportId);

        if ($kind != self::KIND_ALL) {
            $builder->where('betKind', $kind);
        }

        if ($this->isJc) {
            $builder->whereIn('betKind', self::JC_BET_KIND);
        } else {
            $builder->whereNotIn('betKind', self::JC_BET_KIND);
        }

        if (!empty($isWin)) {
            $builder->whereIn('isWin', $isWin);
        }

        return $builder
            ->where('status', 1)
            ->where('predict_status', 1);
    }
}
