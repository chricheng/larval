<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertLeagueTagModel;
use App\Models\Cms\NewsModel;
use App\Services\Tools\Common\CommonSetterService;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExpertLeagueGoodAtService
 * @package App\Services\News\Expert
 * @method $this setExpertId($expertId)
 */
class ExpertLeagueGoodAtService
{
    use CommonSetterService;

    protected $expertId;

    public function main()
    {
        $redisKey = "expert_league_good_at_v3:{$this->expertId}";

        return \Cache::remember($redisKey, now()->addMinutes(5), function () {

            return $this->handleCacheData();
        });
    }

    private function handleCacheData()
    {
        //30天发文的联赛ID
        $totalIds = $this->getLeagueIdsByDays();

        //可购买的联赛ID
        $canBuyIds = $this->getCanBuyLeagueIds($totalIds);

        $tagModels = ExpertLeagueTagModel::query()
            ->where('uid', $this->expertId)
            ->whereIn('league_id', $totalIds)
            ->where(function (Builder $query) {
                $query->where('hit_high_num', '>=', 3)
                    ->orWhere('hit_rate', '>=', 60);
            })
            ->orderByDesc('hit_high_num')
            ->orderByDesc('hit_rate')
            ->get();

        $resInfo = [];

        foreach ($tagModels as $model) {

            $resInfo[] = [
                'name'    => $this->getShowName($model),
                'can_buy' => in_array($model->league_id, $canBuyIds) ? 1 : 0,
                'url'     => getNewMobileUrl('/matchRecord', [
                    'league_id' => $model->league_id,
                    'sclass_id' => $model->league_id,
                    'sport_id'  => $model->sport_id,
                    'uid'       => $this->expertId,
                    'followUid' => $this->expertId,
                ])
            ];
        }

        return collect($resInfo)->sortByDesc('can_buy')->values()->toArray();
    }

    private function getShowName(ExpertLeagueTagModel $model)
    {
        if ($model->hit_high_num >= 3) {
            return $model->league_name.$model->hit_high_num.'连红';
        }

        return $model->league_name.$model->hit_rate_desc;
    }

    private function getCanBuyLeagueIds($leagueIds)
    {
        return NewsModel::query()
            ->where('authorid', $this->expertId)
            ->where('firsttime', '>', now()->subDays(30))
            ->whereIn('league_id', $leagueIds)
            ->where('for_sale', 1)
            ->distinct('league_id')
            ->pluck('league_id')
            ->toArray();
    }

    private function getLeagueIdsByDays()
    {
        return NewsModel::query()
            ->where('authorid', $this->expertId)
            ->where('firsttime', '>', now()->subDays(30))
            ->where('status', 1)
            ->distinct('league_id')
            ->pluck('league_id')
            ->toArray();
    }
}
