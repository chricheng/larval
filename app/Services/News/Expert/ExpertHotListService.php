<?php


namespace App\Services\News\Expert;


use App\Models\Cms\Expert\ExpertTagModel;
use App\Services\News\Common\ExpertBaseInfoService;
use App\Services\Tools\Common\CommonSetterService;

/**
 * 专家-热榜
 * Class ExpertHotListService
 * @package App\Services\News\Expert
 * @method $this setSportId($sportId)
 */
class ExpertHotListService
{
    use CommonSetterService;

    /**we
     * @var int 1 足球 2 篮球 0 全部
     */
    protected $sportId = 0;

    /**
     * 限制
     * @var int
     */
    private $limit = 8;

    private $articleDays = 30;

    public function main()
    {
        //本地和测试环境调整到90天
        if (app()->environment('local', 'test')) {
            $this->articleDays = 90;
        }

        $redisKey = "expert_hot_list_v1:sport_id:{$this->sportId}";

        $list = \Cache::remember($redisKey, now()->addMinutes(5), function () {
            return $this->handleCacheData();
        });

        if (empty($list)) {
            return [];
        }

        return [
            'list'      => $list,
            'jump_url'  => getNewMobileUrl('/talent'),
            'jump_name' => '热推榜',
        ];
    }

    private function handleCacheData()
    {
        $resInfo = [];

        $tmpArr = $this->getRecommendInfo();
        if (!empty($tmpArr)) {
            $resInfo[] = [
                'tab'       => 'recommend',
                'is_active' => 0,
                'name'      => '推荐',
                'data'      => $tmpArr
            ];
        }

        $tmpArr = $this->getReturnRateInfo();
        if (!empty($tmpArr)) {
            $resInfo[] = [
                'tab'       => 'return_rate',
                'is_active' => 0,
                'name'      => '高回报',
                'data'      => $tmpArr
            ];
        }

        $tmpArr = $this->getRedInfo();
        if (!empty($tmpArr)) {
            $resInfo[] = [
                'tab'       => 'red',
                'is_active' => 1,
                'name'      => '连红',
                'data'      => $tmpArr
            ];
        }

//        $tmpArr = $this->getOrderNumInfo();
//        if (!empty($tmpArr)) {
//            $resInfo[] = [
//                'tab'  => 'order_num',
//                'name' => '销量',
//                'data' => $tmpArr
//            ];
//        }

        return $resInfo;
    }

    /**
     * 销量
     * @return array
     */
    private function getOrderNumInfo()
    {
        $resInfo = [];

        //筛选：近30天发过文，可够买
        //排序：近七天销量>命中率
        $tagModels = $this->getBuilder()
            ->orderByDesc('tag.order_num_7_days')
            ->orderByDesc('tag.hit_rate')
            ->limit($this->limit)
            ->get();

        foreach ($tagModels as $model) {

            /**@var $model ExpertTagModel* */

            $resInfo[] = [
                'uid'       => $model->uid,
                'sport_id'  => $model->sport_id,
                'buy_count' => $model->buy_count,
                'text'      => $model->order_num_7_days.'人购买',
                'user_info' => app(ExpertBaseInfoService::class)
                    ->setExpertModel($model->expert)
                    ->main()
            ];
        }

        return $resInfo;
    }

    /**
     * 连红
     * @return array
     */
    private function getRedInfo()
    {
        $resInfo = [];

        //分组一
        //筛选：近30天发过文，可够买，当前连红>=3
        //排序：当前连红>命中率
        $tagModels = $this->getBuilder()
            ->where('hit_high_num', '>=', 3)
            ->orderByDesc('hit_high_num')
            ->orderByDesc('hit_rate')
            ->limit($this->limit)
            ->get();

        foreach ($tagModels as $model) {

            /**@var $model ExpertTagModel* */

            $resInfo[] = [
                'uid'       => $model->uid,
                'sport_id'  => $model->sport_id,
                'buy_count' => $model->buy_count,
                'text'      => $model->hit_high_num.'连红',
                'user_info' => app(ExpertBaseInfoService::class)
                    ->setExpertModel($model->expert)
                    ->main()
            ];
        }

        return $resInfo;
    }

    /**
     * 回报率
     * @return array
     */
    private function getReturnRateInfo()
    {
        $resInfo = [];

        //分组一
        //筛选：近30天发过文，可够买，近10场回报率>120
        //排序：近10场回报率>命中率
        $tagModels = $this->getBuilder()
            ->where('return_rate_10_sessions', '>=', 120)
            ->orderByDesc('return_rate_10_sessions')
            ->orderByDesc('hit_rate')
            ->limit($this->limit)
            ->get();

        foreach ($tagModels as $model) {

            /**@var $model ExpertTagModel* */

            $resInfo[] = [
                'uid'       => $model->uid,
                'sport_id'  => $model->sport_id,
                'buy_count' => $model->buy_count,
                'text'      => $model->return_rate_10_sessions.'%',
                'user_info' => app(ExpertBaseInfoService::class)
                    ->setExpertModel($model->expert)
                    ->main()
            ];
        }

        return $resInfo;
    }

    /**
     * 推荐标签数据
     * @return array
     */
    private function getRecommendInfo()
    {
        $resInfo = [];

        //分组一
        //筛选：近30天发过文，当前有可购买的。
        //排序：权重(目前全部为0)>命中率>分母大的
        $tagModels = $this->getBuilder()
            ->orderByDesc('admin_rank')
            ->orderByDesc('hit_rate')
            ->orderByDesc('hit_rate_y')
            ->limit($this->limit)
            ->get();

        foreach ($tagModels as $model) {

            /**@var $model ExpertTagModel* */

            $resInfo[] = [
                'uid'       => $model->uid,
                'sport_id'  => $model->sport_id,
                'buy_count' => $model->buy_count,
                'text'      => !empty($model->recommend_desc) ? $model->recommend_desc : $model->hit_rate_desc,
                'user_info' => app(ExpertBaseInfoService::class)
                    ->setExpertModel($model->expert)
                    ->main()
            ];
        }

        return $resInfo;
    }


    /**
     * 通用查询
     * @return ExpertTagModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    private function getBuilder()
    {
        $builder = ExpertTagModel::query()
            ->with(['expert'])
            ->from('expert_tags as tag')
            ->leftJoin('experts as expert', 'tag.uid', '=', 'expert.uid')
            ->where('tag.last_article_time', '>=', now()->subDays($this->articleDays))
            ->where('tag.sport_id', $this->sportId)
            ->where('expert.is_active', 1);

        return $builder->select(['tag.*'])
            ->orderByDesc('tag.can_buy');
    }

}
