<?php


namespace App\Services\Tools\Common;


trait CommonSetterService
{
    public function __call($name, $arguments)
    {
        return $this->handleCall($name, $arguments);
    }

    /**
     * 处理类的属性，改属性不能是private，目前支持setter
     * @param $name
     * @param $arguments
     * @return $this
     */
    private function handleCall($name, $arguments)
    {
        $type = substr($name, 0, 3);
        $info = lcfirst(substr($name, 3));

        if (empty($arguments)) {
            return $this;
        }

        //setter
        if ($type === 'set') {
            $this->$info = $arguments[0];
        }
        return $this;
    }
}
