<?php

namespace App\Services\Tools\Common;

class ErrorSvc
{
	//系统级提示信息
	const ERR_NEED_LOGIN         = -1;
	const ERR_SUC                = 0;
	const ERR_SEND_SUC           = 1;
	const ERR_UNBIND_SUC         = 2;
	const ERR_OP_SUC             = 3;
	const ERR_FIND_PWD_SUC       = 5;
	const ERR_USE_MOBILE_SUC     = 6;
	const ERR_USE_USERNAME_SUC   = 7;

	const ERR_FAILED           = 99;
	const ERR_SYS              = 100;
	const ERR_PARAMS           = 101;

	const ERR_UNLOCK_COUNT           = 1000;


	public static function getMsg($code)
	{
		$ret = '系统繁忙，请稍后再试！';
		$arr = [
			self::ERR_NEED_LOGIN                => '账号未登录,请登录',
			self::ERR_SUC                       => 'OK',
			self::ERR_SEND_SUC                  => '发送成功',
			self::ERR_UNBIND_SUC                => '已解除绑定',
			self::ERR_OP_SUC                    => '操作成功',
			self::ERR_FIND_PWD_SUC              => '密码修改成功',
			self::ERR_USE_MOBILE_SUC            => '手机号可以使用',
			self::ERR_USE_USERNAME_SUC          => '用户名可以使用',
			self::ERR_FAILED                    => '操作失败',
			self::ERR_SYS                       => '系统繁忙，请稍候再试',
			self::ERR_PARAMS                    => '缺少参数',
			self::ERR_UNLOCK_COUNT                    => '当日解锁次数已用完',
        ];
        if ($code !== null && isset($arr[$code])) {
			$ret = $arr[$code];
		}
        return $ret;
	}

	public static function getError($code, $data = null, $msg = null)
	{
		$ret = ['errno' => $code, 'errmsg' => self::getMsg($code)];
		if ($code < self::ERR_FAILED && $code > self::ERR_SUC) {
			$ret['errno'] = self::ERR_SUC;
		}
		if ($data !== null) {
			$ret['data'] = $data;
		}
		if ($msg) {
			$ret['errmsg'] = $msg;
		}
		return $ret;
	}

	/**
	 * 记录或读取错误信息
	 *
	 *
	 * @param int $code
	 *
	 * @return array
	 */
	public static function error($code = null)
	{
		static $error_code;
		if ($code !== null) {
			if ($code < self::ERR_FAILED && $code > self::ERR_SUC) {
				$error_code = self::ERR_SUC;
			} else {
				$error_code = $code;
			}
		} else {
			return $error_code;
		}
	}
}
