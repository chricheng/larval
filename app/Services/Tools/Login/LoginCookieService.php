<?php


namespace App\Services\Tools\Login;

use App\Services\Tools\Common\CommonSetterService;

/**
 * 处理登录信息
 * Class LoginCookieService
 * @package App\Serrvices\Common
 * @method $this setUid($uid);
 * @method $this setIsLive($isLive);
 */
class LoginCookieService
{
    use CommonSetterService;

    protected $uid;

    protected $isLive = false;

    /**
     * @var string 加密KEY
     */
    private $desKey = '8Ab(a5c*_test';

    /**
     * @var string 签名KEY
     */
    private $signKey = '8a0dda6ac_test';

    function decrypt($cookie)
    {
        $this->initParams();

        $data = base64_decode(substr($this->baseDecode($cookie), 128));

        if ($iv_size = openssl_cipher_iv_length('des-cbc')) {
            $iv   = substr($data, 0, $iv_size);
            $data = substr($data, $iv_size);
        } else {
            $iv = null;
        }

        $key = hash_hkdf('sha512', $this->desKey, strlen($this->desKey), 'encryption');

        $originalStr = openssl_decrypt($data, 'des-cbc', $key, 1, $iv);

        if (empty($originalStr)) {
            return false;
        }

        [$uid, $loginTime, $sign] = explode('|', $originalStr);

        if (empty($uid) || empty($loginTime) || empty($sign)) {
            return false;
        }

        if ($sign != $this->getGenSign($uid, $loginTime)) {
            return false;
        }

        return $uid;
    }

    public function encrypt()
    {
        $this->initParams();

        $data = $this->getGenCookieStr($this->uid);

        $iv = ($iv_size = openssl_cipher_iv_length('des-cbc'))
            ? openssl_random_pseudo_bytes($iv_size)
            : null;

        $key     = hash_hkdf('sha512', $this->desKey, strlen($this->desKey), 'encryption');
        $hmacKey = hash_hkdf('sha512', $this->desKey, 0, 'authentication');

        $data = base64_encode($iv.openssl_encrypt($data, 'des-cbc', $key, 1, $iv));

        $data = hash_hmac('sha512', $data, $hmacKey, false).$data;

        return urlencode($this->baseEncode($data));
    }

    public function baseEncode($str)
    {
        $src  = ["/", "+", "="];
        $dist = ["_a", "_b", "_c"];
        $old  = base64_encode($str);
        return str_replace($src, $dist, $old);
    }

    public function baseDecode($str)
    {
        $src  = ["_a", "_b", "_c"];
        $dist = ["/", "+", "="];
        $old  = str_replace($src, $dist, $str);
        return base64_decode($old);
    }


    private function getGenCookieStr($uid)
    {
        $loginTime = time() + 60;//首次注册登录时间要大于最后修改密码时间，不然登录会失效
        $sign      = $this->getGenSign($uid, $loginTime);
        return implode("|", [$uid, $loginTime, $sign]);
    }

    /**
     * 参数签名
     * @param $uid
     * @param $loginTime
     * @return false|string
     */
    private function getGenSign($uid, $loginTime)
    {
        $sign = md5($uid.$loginTime.$this->signKey);
        $sign = substr($sign, 3, 14);
        return $sign;
    }

    private function initParams()
    {
        if (app()->environment('production', 'pre') || $this->isLive) {
            $this->signKey = '5mb8mjpn61a51';
            $this->desKey  = 'vbsti6jhhrgd9';
        }
    }

}
