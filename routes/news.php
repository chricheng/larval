<?php

Route::group([
    'namespace' => 'Expert',
    'prefix'    => 'expert',
], function () {
    Route::get('/hot-list/index', 'HotListController@index');
    Route::get('/hot-list/rank', 'HotListController@rank');

    Route::get('/league/user-info', 'LeagueController@userInfo');
    Route::get('/league/good-at', 'LeagueController@goodAt');


    Route::get('/home/statistics', 'HomeController@statistics');


});

Route::group([
    'namespace' => 'User',
    'prefix'    => 'user',
], function () {
    Route::get('/custom/index', 'CustomController@index');
});

Route::group([
    'namespace'  => 'Match',
    'prefix'     => 'match',
    'middleware' => 'api.auth',
], function () {

    Route::get('/unlock-news/pop', 'UserMatchNewsController@index'); #处理弹窗
    Route::post('/unlock-news/click', 'UserMatchNewsController@click'); #处理解锁

});
