<?php


Route::group([
    'namespace'  => 'Pay',
    'prefix'     => 'pay',
], function () {

    Route::get('/member/rights', 'MemberController@rights');
    Route::get('/member/home', 'MemberController@home');


    Route::group([
        'middleware' => 'api.auth'
    ], function () {
        Route::post('/member/buy', 'MemberController@buy');
    });
});
